#include <stdio.h>
void main(){

    // Create a name type
    typedef char name_field[256];

    // Declare variables with the new type
    name_field capt_name = {"Jean-Luc Picard"};
    name_field first_officer = {"William T. Riker"};

    printf("%s leads %s\n", capt_name, first_officer);   
}