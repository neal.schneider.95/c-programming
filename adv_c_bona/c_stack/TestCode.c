#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int push(struct numNode **top, int data)
{
    if (top == NULL)
    {
        return 1;
    }

    struct numNode *newNode = (struct numNode*) malloc(sizeof(struct numNode));
    newNode->num = data;

    newNode->next = (*top);

    (*top) = newNode;

    return 0;
}

int pop(struct numNode **top)
{
    if (top==NULL)
    {
        return 0;
    }
    if((*top)==NULL)
    {
        return 0;
    }
    struct numNode *temp = (*top);
    int num;
    (*top) = (*top)->next;
    num = temp->num;
    free(temp);
    return num;
}

void emptyStack(struct numNode **top)
{
    // if (top == NULL)
    // {
    //     return 0;
    // }
    while (pop(top) != 0)
        ;
}

struct numNode *createStack(int actions[], int numActions)
{
    struct numNode *head = (struct numNode*) malloc(sizeof(struct numNode));
    head = NULL;

    for (int x = 0; x < (numActions*2); x+=2)
    {
        // pop
        if (actions[x] == 1)
        {
            pop(&head);
        }
        // push
        else if (actions[x] == 2)
        {
            push(&head, actions[x+1]);
        }
        // empty stack
        else if (actions[x] == 3)
        {
            emptyStack(&head);
        }
        else
        {
            return NULL;
        }
    }
    return head;
}
