#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

// PROTOTYPES
int checkDuplicates(struct numNode *head, int value);

struct numNode *buildCList(int *nums, int size)
{
    if (sizeof(size) == 0 || nums == NULL)
    {
        return NULL;
    }

    // create head
    struct numNode *head = (struct numNode *)malloc(sizeof(struct numNode));
    head = NULL;

    // loop through items in list
    for (int i = 0; i < size; i++)
    {
        // create new node
        struct numNode *newNode = (struct numNode *)malloc(sizeof(struct numNode));
        // assign the looped number to the nodes value
        newNode->num = nums[i];

        // check if head is empty
        if (head == NULL)
        {
            // if empty assing new node as head
            head = newNode;
            // since its only one item, point heads->next to head
            head->next = head;
        }
        else
        {
            int bl = checkDuplicates(head, nums[i]);
            // check for duplicates
            if (bl == 2)
            {
                // append new item
                newNode->next = head;
                // traverse to end of list
                struct numNode *probe = (struct numNode *)malloc(sizeof(struct numNode));
                probe = head;
                while (probe->next != head)
                {
                    probe = probe->next;
                }
                probe->next = newNode;
                // update the head
                head = newNode;
            }
        }
    }
    return head;
}

int emptyList(struct numNode *head)
{
    if (head == NULL)
    {
        return 0;
    }
    int freedNodes = 0;

    if (head->next == head)
    {
        free(head);
        freedNodes++;
    }
    else
    {
        struct numNode *probe = (struct numNode *)malloc(sizeof(struct numNode));
        probe = head->next;
        while (probe->next != head)
        {
            struct numNode *temp = probe;
            probe = probe->next;
            free(temp);
            freedNodes++;
        }
        free(probe);
        free(head);
        freedNodes = +2;
    }
    return freedNodes;
}

int checkDuplicates(struct numNode *head, int value)
{
    struct numNode *probe = (struct numNode *)malloc(sizeof(struct numNode));
    probe = head;
    while (probe->next != head)
    {
        if (probe->num == value)
        {
            return 1;
        }
        probe = probe->next;
    }
    return 2;
    // return 1 if item already exists
    // return 2 if items doesn't exist
}