#include <stdio.h>

int main () {
    int var = 100;

    // pointer to it...
    int *var_ptr;

    var_ptr = &var;

    // double pointer to var..
    int **var_dptr = &var_ptr;

    printf("value of var = %d, \n", var);
    printf("value of using ptr = %d\n", *var_ptr );
    printf("value of using double ptr = %d\n", **var_dptr);
    }