#include <stdio.h>
void swap_nums(int *a, int *b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int main(){
    int x = 4; int y = 7;
    printf("x= %d, y= %d\n", x, y);
    swap_nums(&x, &y);
    // What are the values of x and y now?
    printf("x= %d, y= %d\n", x, y);
    return 0;
}
