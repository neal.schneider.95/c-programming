#include <stdio.h>

int main() {
    // Array of string pointers
    char *phrases[5];
    char original_strings[5][70] =
    {   "I'm not lazy, I'm on energy-saving mode.",
        "I'm not clumsy, I'm just adept at creating abstract art.",
        "I'm not forgetful, I'm just creating a mystery game for myself.",
        "I'm not procrastinating, I'm just allowing my ideas to marinate.",
        "I'm not indecisive, I'm just exploring all parallel universes." };

    // Populate the pointer array with strings
    for (int i = 0; i < 5; i++) {
        phrases[i] = original_strings[i];
    }

    // Display the strings
    for (int i = 0; i < 5; i++) {
        printf("%s\n", phrases[i]);
    }

    return 0;
}
