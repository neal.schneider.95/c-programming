#include <stdio.h>
void main() {
    int *a;
    int contents;
    // Print a valid address for the user to start
    printf("My memory starts at %p.\nWhat would you like to scan? ", a);
    // Use scanf to receive address
    scanf("%p", &a);
    printf("That address contains: ");
    // Dereference the pointer to read the value there. (Discussed next.)
    contents = *a;
    printf("%x\n", contents);
}