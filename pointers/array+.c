#include <stdio.h>
void main() {
    int int_array[6] = {0, 1, 2, 3, 4};
    *int_array = 42;
    for (int i = 0; i < 6; i++) {
        // Adds i to the array pointer (multiplies by sizeof(int))
        printf("Element [%d] is %d\n", i, *(int_array+i));
    }
}