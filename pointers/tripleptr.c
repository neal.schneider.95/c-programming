#include <stdio.h>
#include <stdlib.h>

int main () {
    int i = 42;
    int *p = &i;
    int **double_p = &p;
    int ***trip_p = &double_p;
    printf("   i = %d\n", i);
    printf("  &i = %p\n", &i);
    printf("  *p = %d,  p = %p\n", *p, p);
    printf(" *dp = %p, dp = %p\n", *double_p, double_p);
    printf("**dp = %d\n", **double_p);
    printf(" *tp = %p, tp = %p\n", *trip_p, trip_p);
    printf("***tp= %d\n", ***trip_p);
}