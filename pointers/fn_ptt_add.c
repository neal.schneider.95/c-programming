	// Complete Example 1: add function
#include <stdio.h>
 
int add (int a, int b);         // add function prototype
int (*add_fn_ptr) (int, int);   // Pointer declaration for add function
 
int main () {
    int sum;
    add_fn_ptr = &add;          // Explicit assignment of function pointer
    for (int i = 0; i<4; i++){
        sum = add_fn_ptr(i, i); // Call the function pointer
        printf("%d\n", sum);
    }
}
 
int add (int a, int b) {        // add function definition
    return a + b;
}