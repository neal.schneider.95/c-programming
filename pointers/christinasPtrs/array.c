
# include<stdio.h>
# include<stdbool.h>
//*******************************array pointer*******************
// int main()
// {

// int lst[] = {0xFEEDFACE, 0xC001C0DE, 0xCAFEF00D, 0xDECAFBAD};
// int indexNum = 0;   // Used to reference an index
// int *lst_ptr = NULL;
// bool TorF = false;
 
// lst[indexNum];      // 0xFEEDFACE
// lst_ptr = &lst[0];  // Assign the address of array index 0
// printf("This is the lst_ptr assigned to the first element of the lst array: %x\n", *lst_ptr);
// printf("Testing to see if the lst_ptr is the same as lst\n");
// TorF = (lst_ptr == lst);     // Array name == array memory address
// printf("%s\n", TorF ? "true" : "false");

// printf("Testing to see if the dereference value of the lst_ptr is the same as the first element of the lst array\n");
// TorF = *lst_ptr == lst[0]; // 0xFEEDFACE
// printf("%s\n", TorF ? "true" : "false");

// printf("Testing to see if the dereference lst is the same as the dereferenced list pointer\n");

// TorF = *lst_ptr == *lst;   // 0xFEEDFACE
// printf("%s\n", TorF ? "true" : "false");
// }
//************************************accessing array******************************************

// put a watch on in debug mode:
// - lst_ptr
// - *lst_ptr
// - &lst
// int main()
// {
// //Example code will be executed per line and displayed in memory
// // int lst[] = {0xFEEDFACE, 0xC001C0DE, 0xCAFEF00D, 0xDECAFBAD};
// int lst[] = {77, 88, 65, 4};
// // lst[0] = 0x8BADF00D          // Normal
// lst[0] = 5;
// int * lst_ptr = &lst[1]; // By pointer reference...
// // *lst_ptr = 0xC0DEDEAD;        // ...and then dereferencing it
// *lst_ptr = 106;
// // *(lst + 2) = 0x1BADD00D;      // Array name pointer math…
//  *(lst + 2) = 42;      
// lst_ptr += 2;                 // ...AKA Address arithmetic
// // *lst_ptr = 0xDEADBEEF;       // Dereference a pointer
// *lst_ptr = 23; 
// }

//************************array arithmetic**************

// put watch on:
// - &someNums
// - someNums_ptr
// - *someNums_ptr


int main()
{
    int someNums [5] = {0, 1, 2, 3, 4}; // Int array of length 5
    int * someNums_ptr = someNums;   // Points at &someNums[0]

    //ptr++ increments ptr to the next element
    someNums_ptr++; 
    //ptr += n moves the pointer n elements
    someNums_ptr += 2;
    //ptr-- decrements ptr to previous element
    someNums_ptr--; 
    //ptr -= n moves the pointer back n elements
    someNums_ptr -= 2;  

    return 0;
}