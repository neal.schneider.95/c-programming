#define NO_WINNER 0
#define PLAYER_ONE_WINS 1
#define PLAYER_TWO_WINS 2
#define INVALID_INPUT -1

#include <stdio.h>

int check_tic_tack_toe_board(int[3][3]);

int main()
{
    int output;

    printf("\nTEST 1 NULL board:\n");
    output = check_tic_tac_toe_board(NULL);
    if (output == INVALID_INPUT)
        printf("PASS\n");
    else
        printf("FAIL\n");

    printf("\nTEST 2 Malformed board:\n");
    int board1[3][3] = {{0, 0, 0}, {0, 0, 3}, {0, 0, 0}};
    output = check_tic_tac_toe_board(board1);
    if (output == INVALID_INPUT)
        printf("PASS\n");
    else
        printf("FAIL\n");

    printf("\nTEST 3 No winner board:\n");
    int board2[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    output = check_tic_tac_toe_board(board2);
    if (output == NO_WINNER)
        printf("PASS\n");
    else
        printf("FAIL\n");

    printf("\nTEST 4 P1 winner board:\n");
    int board3[3][3] = {{0, 1, 0}, {0, 1, 0}, {0, 1, 0}};
    output = check_tic_tac_toe_board(board3);
    if (output == PLAYER_ONE_WINS)
        printf("PASS\n");
    else
        printf("FAIL\n");

    printf("\nTEST 5 P2 winner board:\n");
    int board4[3][3] = {{2, 0, 0}, {0, 2, 0}, {0, 0, 2}};
    output = check_tic_tac_toe_board(board4);
    if (output == PLAYER_TWO_WINS)
        printf("PASS\n");
    else
        printf("FAIL\n");

    return 0;
}

int check_tic_tac_toe_board(int board_state[3][3])
{
    if (!board_state)
        return INVALID_INPUT;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (board_state[i][j] > 2 || board_state[i][j] < 0)
                return INVALID_INPUT;
        }
    }

    for (int i = 0; i < 3; i++)
    {
        if (board_state[i][0] == board_state[i][1] && board_state[i][2] == board_state[i][1])
        {
            if (board_state[i][1] == 1)
                return PLAYER_ONE_WINS;

            if (board_state[i][1] == 2)
                return PLAYER_TWO_WINS;
        }
        if (board_state[0][i] == board_state[1][i] && board_state[2][i] == board_state[1][i])
        {
            if (board_state[1][i] == 1)
                return PLAYER_ONE_WINS;
            if (board_state[1][i] == 2)
                return PLAYER_TWO_WINS;
        }
    }

    if ((board_state[0][0] == board_state[1][1] && board_state[1][1] == board_state[2][2]) ||
        (board_state[2][0] == board_state[1][1] && board_state[1][1] == board_state[0][2]))
    {
        if (board_state[1][1] == 1)
            return PLAYER_ONE_WINS;
        if (board_state[1][1] == 2)
            return PLAYER_TWO_WINS;
    }

    return NO_WINNER;
}