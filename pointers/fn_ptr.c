#include <stdio.h>

float add (float a, float b) {return a + b;}
float sub (float a, float b) {return a - b;}
float mul (float a, float b) {return a * b;}
float div (float a, float b) { if (b) {return a/b;} else return 0;}

float (*math_ptr) (float, float);
*math_ptr decision_array[] = {add, sub, mul, div};

int main () {
    for (int i = 0; i<4; i++){
        printf("%f\n", decision_array[i](7.0, 7.0));
    }
}
