#include <stdio.h>
void main() {
    int *a;
    unsigned char *block = (char*) a;
    int contents;
    // Print a valid address for the user to start
    printf("My memory starts at %16p\nWhat would you like to scan? ", a);
    // Use scanf to receive address
    scanf("%p", &a);
    printf("That address contains: ");
    // Dereference the pointer to read the value there. (Discussed next.)
    contents = *a;
    // printf("%x\n", contents);

    // Larger block:
    for (int row = 0; row < 16; row++) {
        printf("\n%p", block + (row * 16));
        for (int col = 0; col < 16; col++) {
            printf(" %02X", *(block + row * 16 + col));
        }
    }
    printf("\n");

    char *ints = {0, 10, 100, 150};
}