#include <stdio.h>

void swap_fail(int a, int b) {
    int tmp = a;
    a = b;
    b = tmp;                        // Values Swapped at this point
}                                   // But values lost on the return

void swap_nums(int *a, int *b) {    // Both a and b here are declared as pointers
    int tmp = *a;                   // Get the actual value by dereferencing it first 
    *a = *b;                        // Change a's value
    *b = tmp;                       // Change b's value
// The original variables changed, so changes preserved on the function's return
}

int main(){
    int x = 4; 
    int y = 7;
    printf("Original: x= %d, y= %d\n", x, y);

    swap_fail(x, y);                // Passing by value -- Doesn't work to mod variables
    printf("  Failed: x= %d, y= %d\n", x, y);

    swap_nums(&x, &y);              // Passing by reference -- This method works!
    // When &x and &y are dereferenced, the original variables are accessed

    printf("   Final: x= %d, y= %d\n", x, y);
    return 0;
}