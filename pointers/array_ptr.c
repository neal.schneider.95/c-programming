#include <stdio.h>
void main() {
    int int_array[8] = {0, 1, 2, 3, 4};
    *int_array = 42;
    int *int_ptr = int_array;
    for (int i = 0; i < 6; i++) {
        printf("Element [%d] is %d\n", i, int_array[i]);
    }
}