#include <stdio.h>
#include <stdlib.h>

int main () {
    int ****p = NULL;
    p = malloc(sizeof(p));
    *p = malloc(sizeof(p));
    **p = malloc(sizeof(p));
    printf("pointer size = %ld\n", sizeof(p));
    printf("   p = %p\n", p);
    printf("  *p = %p\n", *p);
    printf(" **p = %p\n", **p);
    printf("***p = %p\n", ***p);

}