#include <stdio.h>
#define ROWS 12
#define COLUMNS 24

int main (){
    char board[ROWS][COLUMNS] = { ' ' };
    int i = 0;	  // Iterate through rows
    int j = 0;	 // Iterate through columns

    
    for (i = 0; i < ROWS; i++)
    {
        for (j = 0; j < COLUMNS; j++)  
        {   
            if ((i+j) % 2 == 0 )
            {
                board[i][j] = '#';
            }
            else
            {
                board[i][j] = ' ';
            }
        }
    }

    for (i = 0; i < ROWS; i++)
    {
        for (j = 0; j < COLUMNS; j++)  
        {
            printf("%c", board[i][j]);
        }
        printf("\n");
    }
}