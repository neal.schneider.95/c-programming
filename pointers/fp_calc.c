#include <stdio.h>
// #include <math.h>            // Necessary for NAN definition (extra credit)

double add(double x, double y);
double sub(double x, double y);
double mul(double x, double y);
double div(double x, double y);

int main (void) 
{
    // Define function pointer matrix using above prototypes
    double(*mathFnPtr[4])(double, double) = {add, sub, mul, div};

    double num1 = 0;
	char mathOperator = 0; 
	double num2 = 0;
	double returnValue = 0;
    int fn_number = 0;

	printf("Enter numbers separated by a math operator (+-*/): \n");
	scanf("%lf %c %lf", &num1, &mathOperator, &num2);
	
    if (mathOperator == '+') {
        fn_number = 0;
    } 
    else if (mathOperator == '-') {
        fn_number = 1;
    } 
    else if (mathOperator == '*') {
        fn_number = 2;
    } 
    else if (mathOperator == '/') {
        fn_number = 3;
    } else {
        // Something other than the acceptable operators was received
        printf("Error: Invalid operator.\n");
        return -1;
    }
    if (mathFnPtr) {                    // Check for NULL pointer
        // Using function pointer array, select the operation and pass the variables.
        returnValue = mathFnPtr[fn_number](num1, num2);
        printf( "The result is: %f \n", returnValue);
    } else {
        // This should never happen
        printf("Error: NULL Math Pointer.");
        return -1;
    }
}
double add(double x, double y) { 
    return x+y;
}
double sub(double x, double y) { 
    return x-y;
}
double mul(double x, double y) { 
    return x*y;
}
double div(double x, double y) { 
    if (y) {                        // Avoid zero div error
        return x/y;
    }
    printf("Error: Division by zero.\n");
    return 0.0;
    // return NAN;          // Extra Credit - Need <math.h>
}