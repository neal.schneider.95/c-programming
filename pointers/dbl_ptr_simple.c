#include <stdio.h>
int main () {
    int a = 42;
    int *a_ptr = &a;
    int **double_ptr = &a_ptr;

    printf("value of a = %d\n", a);
    printf("value of using ptr = %d\n", *a_ptr);
    printf("value of using double ptr = %d\n\n", **double_ptr);

    printf("value of   a = %16d, at %p\n", a, &a);
    printf("value of ptr = %16p, at %p\n", a_ptr, &a_ptr);
    printf("value of dptr= %16p, at %p\n", double_ptr, &double_ptr);
    
    void *c = 0;
    printf("void, +1 = %p, %p", c, c+1);
}