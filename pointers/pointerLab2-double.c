// CCD Pointer Lab Solution
// Evaluates using double pointers

#include <stdio.h>

void print_dp(char *label, float** ptr) {
    printf("%s = %p -> %p -> %.3f\n", label, ptr, *ptr, **ptr);
}

void main() {
    float f0 = 3.14;
    float *f0_p = &f0;
    float **f0_dp = &f0_p;
    print_dp("f0_dp", f0_dp);
    
    float f2 = 99.999;
    float *f2_p = &f2;
    float **f2_dp = &f2_p;
    print_dp("f2_dp", f2_dp);
    printf("\n");

    *f0_dp = &f2;
    print_dp("f0_dp", f0_dp);
    print_dp("f2_dp", f2_dp);
    printf("\n");

    f0 = 88;
    **f2_dp = 777.7;
    print_dp("f0_dp", f0_dp);
    print_dp("f2_dp", f2_dp);
}