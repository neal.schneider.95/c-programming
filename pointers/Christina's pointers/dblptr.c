// #include <stdio.h>

// int main()
// {
//     // Initialize a Number
//     int number = 4;
//     // Initialize a Pointer to the Number
//     int *numPtr = &number;
//     // Initialize a Pointer to the Pointer, pointing to the Number
//     int ** ptrPtr = &numPtr;

//     // Print out Number
//     printf("number = %d\n", number);
//     printf("The address of number = %p\n", &number);
//     // Print out numPtr
//     printf("numPtr -> %p = %d\n", numPtr, *numPtr);
//     // Print out ptrPtr
//     printf("ptrPtr -> %p -> %p = %d\n", ptrPtr, *ptrPtr, **ptrPtr);

//     printf("Pointer 1 is %p with value %d\n", numPtr, *numPtr);
//     printf("Pointer 2 is %p with value %d\n", ptrPtr, **ptrPtr);

//     **ptrPtr = 7;

//     printf("Number is now %d\n", number);
// }


//****************byteclub pointerpointerexmple***************

# include <stdio.h>
int main()
{

    char rule0[] = {"Talk about Byte Club."};
    char rule1[] = {"Please talk about Byte Club."};
    char rule2[] = {"If a byte is 0x0 the string is over."};
    char * byteClubRules[] = {rule0, rule1, rule2};
    char ** rule_ptr = byteClubRules;
    /* byteClubRules is an array of character pointers */
    /* rule_ptr points to mem addresses of elements in byteClubRules */ 
    for (int i = 0; i < 3; i++)
    {
        printf("Rule #%d:\t%s\n" , i, *rule_ptr);
        rule_ptr++;
    }

    return 0;

}