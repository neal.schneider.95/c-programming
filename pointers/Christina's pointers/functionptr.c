// #include <stdio.h>

// int printInt(int a)
// {
//     if(a < 10 && a > 0)
//     {
//         printf("Basic Int\n");
//         return 0;
//     }
//     else if (a < 0)
//     {
//         printf("Negative Value\n");
//         return -1;
//     }
//     printf("Better Num\n");
//     return 1;
    
// }

// int main()
// {
//     // Implicit Function Pointer
//     int (*funPtr)(int) = printInt;
//     int var = funPtr(2);

//     // Explicit Funtion Pointer
//     int (*expFunPtr)(int) = &printInt;
    
//     if((*expFunPtr)(-2) > 0)
//     {
//         printf("good\n");
//     } 

//     printf("Function printInt addess is: %p\n", funPtr);
//     printf("Function printInt addess is: %p\n", expFunPtr);

// }

//*****************function ptr array calc******************

#include <stdio.h>

void add(int a, int b)
{
    printf("Addition is %d\n", a+b);
}
void sub(int a, int b)
{
    printf("Subtraction is %d\n", a-b);
}
void mult(int a, int b)
{
    printf("Multiplication is %d\n", a*b);
}
void div(int a, int b)
{
    if(b == 0)
    {
        printf("Cannot Divide by 0\n");
    }
    else
    {
        printf("Division is %d\n", a/b);
    }
}

int main()
{
    // Declare 3 Variables 
    int num1;
    int num2;
    int fun_input;

    // Single Function Pointer initialization
    //void (* div_ptr)(int,int) = &div;

    // Initialize an Array of function Pointers
    //returns void, a pointer array that takes two ints as arguments made of the functions ad, sub, mult and div
    void (* calc_ptr[])(int,int) = {add, sub, mult, div};

    printf("Enter two Intergers:\n");
    scanf("%d %d", &num1, &num2);
    printf("\nEnter Function Choice:\n0 for Addition\n1 for Subtraction\n2 for Multiplication\n3 for Division\n");
    scanf("%d", &fun_input);

    if (fun_input > 3)
    {
        printf("Please Enter a Valid Choice\n");
        return 0;
    }
    //used in place of a switch case
    (*calc_ptr[fun_input])(num1, num2);

    return 0;

}