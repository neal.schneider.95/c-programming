#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Comparison function for qsort
int compare_floats(const void* a, const void* b) {
    float float_a = *((const float*)a);
    float float_b = *((const float*)b);

    if (float_a < float_b)
        return -1;
    else if (float_a > float_b)
        return 1;
    else
        return 0;
}

int main() {
    // Seed the random number generator
    srand(time(0));

    float arr[10];

    printf("Randomly generated array:\n");
    for (int i = 0; i < 10; i++) {
        arr[i] = (float)rand() / RAND_MAX * 1000; // Generate random float between 0 and 1000
        printf("%.2f ", arr[i]);
    }

    // Using qsort to sort the array
    qsort(arr, 10, sizeof(float), compare_floats);

    printf("\n\nSorted array:\n");
    for (int i = 0; i < 10; i++) {
        printf("%.2f ", arr[i]);
    }
    printf("\n");

    return 0;
}



// #include <stdio.h>
// #include <stdlib.h>

// int compare(void *a, void *b) {
//     return *(int *)a < *(int *)b;
// }

// void main() {
//     float data[5] = {1000, 0, 3.14, 42, -33.333};
//     float sorted[5];
//     qsort(data, 5, sizeof(float),  compare, sorted);
//     for (int i = 0; i < 5; i++) {
//         printf("%d: %f\n", i, sorted[i]);
//     }
// }