#include <stdio.h>
void main() {
    int num_a = 9;       // Integer variable num_a defined as 9
    int num_b = 0;       // Integer variable num_b defined as 0
    int *int_ptr;        // Pointer variable for an integer
    printf("Uninitialized ptr = %p\n", int_ptr);

    int_ptr = &num_a;    // Assigned with address of num_a

    printf("  Initialized ptr = %p, points to %d\n", int_ptr, *int_ptr);
    num_b = *int_ptr;    // num_b assigned value at int_ptr address

    printf("num_b = %d\n", num_b);  // num_b, now 9, now equals num_a
    *int_ptr = 1;                   // Value at int_ptr address assigned 1
                                    // num_a is now equal to 1 
    printf("num_a = %d\n", num_a);  // num_b, now 9, now equals num_a

    num_b = 99;
    int_ptr = &num_b;  // Changes where int_ptr points to, now num_b
    printf("int_ptr = %p, points to %d\n", int_ptr, *int_ptr);
}