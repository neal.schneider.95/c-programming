
## 1.  Prints a date to represent it in a legal form 

Example execution of program:
```
Enter date (mm/dd/yy): 7/19/14
Dated this 19th day of July, 2014.
```

## 2.  Write a program that requests the hours worked in a week and then prints the gross pay, the taxes, and the net pay. Assume the following:  

a.  Basic pay rate = $10.00/hr  
b.  Overtime (in excess of 40 hours) = time and a half  
c.  Tax rate: #15% of the first $300, 20% of the next $150, 25% of the rest  

#define is a preprocessor directive in C
syntax ->    #define CNAME (expression)     // expression will be the value you assign to CNAME

Use #define constants, and don’t worry if the example does not conform to current tax law.


## 3.  Write a program that finds the largest and smallest of four integers entered by the user:

```sh
Enter four integers: 21 43 10 35
Largest: 43
Smallest: 10
```

Use as few ```if``` statements as possible. *Hint*: Four ```if``` statements are sufficient.


## 4. The following table shows the daily flights from one city to another:

|*Departure time*|*Arrival time*|
|:-:|:-:|
|8:00 a.m. | 10:16 a.m.|
|9:43 a.m. | 11:52 a.m.|
|11:19 a.m.| 1:31 p.m.|
|12:47 p.m.| 3:00 p.m.|
|2:00 p.m.|4:08 p.m.|
|3:45 p.m.|5:55 p.m.|
|7:00 p.m.|9:20 p.m.|
|9:45 p.m.|11:58 p.m.|

Write a program that asks users to enter a time (expressed in hours and minutes, using the 24-hour clock). The program then displays the departure and arrival times for the flight whose departure time is closest to that entered by the user:

```sh
Enter a 24-hour time: 13:15
Closest departure time is 12:47 p.m., arriving at 3:00 p.m.
```
*Hint*: Convert the input into a time expressed in minutes since midnight, and compare it to the departure times, also expressed in minutes since midnight. For example, 13:15 is 13 x 60 + 15 = 795 minutes since midnight, which is closer to 12:47 p.m. (767 minutes since midnight) thank to any of the other departure times.


## 5.  Write a program that prompts the user to enter two dates and then indicates which date comes earlier on the calendar:

```sh
Enter first date (mm/dd/yy): 3/6/08
Enter second date (mm/dd/yy): 5/17/07
5/17/07 is earlier than 3/6/08
```

## 6.  Using the ```switch``` statement, write a program that converts a numerical grade into a letter grade:

```sh
Enter numerical grade: 84
Letter grade: B
```

Use the following grading scale: A = 90-10, B = 80-89, C = 70-79, D = 60-69, F = 0-59. Print an error message if the grade is larger than 100 or less than 0. *Hint*: Break the grade into two digits, then use a ```switch``` statement to test the ten's digit.

## 7.  Write a program that asks the user for a two-digit number, then prints the English word for the number:

```sh
Enter a two-digit number: 45
You entered the number forty-five.
```

*Hint*: Break the number into two digits. Use one ```switch``` statement to print the word for the first digit ("twenty", "thirty", and so forth). Use a second ```switch``` statement to print the word for the second digit. Don't forget that the numbers between 11 and 19 require special treatment.


## 8.  Write a program to check whether a triangle is valid or not, when the three angles of the triangle are entered through the keyboard. A triangle is valid if the sum of all the three angles is equal to 180 degrees.


## 9.  Given the length and breadth of a rectangle, write a program to find whether the area of the rectangle is greater than its perimeter. For example, the area of the rectangle with length = 5 and breadth = 4 is greater than its perimeter.


## 10.   Here's a simplified version of the Beaufort scale, which is used to estimate wind force:

Speed(knots)    Description
Less than 1     Calm
1-3             Light Air
4-27            Breeze
28-47           Gale
48-63           Storm
Above 63        Hurricane

Write a program that asks the user to enter a wind speed, then display the corresponding description.


## 11.   Write a program to check for a vowel in a string( implement a switch statement)


## 12.   Write a program that will first allow a user to choose one of two options:

Convert a temperature from degrees Celsius to degrees Fahrenheit.
Convert a temperature from degrees Fahrenheit to degrees Celsius.

The program should then prompt for the temperature value to be entered and output the new value that results from the conversion. To convert from Celsius to Fahrenheit you can multiply the value by 1.8 and then add 32. To convert from Fahrenheit to Celsius, you can subtract 32 from the value, then multiply by 5, and divide the result by 9


## 13.   Write a program that will calculate the price for a quantity entered from the keyboard, given that the unit price is $5 and there is a discount of 10 percent for quantities over 30 and a 15 percent discount for quantities over 50.


## 14.   Write a C program to find out the student’s grades according to the following rules:

- If any of the marks is less than or equal to 40, grade is ‘F’.
- If each mark is less or equal to 60 and the total is greater than 120, then the grade is ‘O’.
- If each mark is less or equal to 80 and the total marks are not less than 180, then the grade is ‘S’.
- If total marks is larger than 240, then the grade is ‘E’.