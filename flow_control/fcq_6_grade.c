// 6.  Using the ```switch``` statement, write a program that converts a numerical grade into a letter grade:

// ```sh
// Enter numerical grade: 84
// Letter grade: B
// ```

// Use the following grading scale: A = 90-10, B = 80-89, C = 70-79, D = 60-69, F = 0-59. Print an error message if the grade is larger than 100 or less than 0. *Hint*: Break the grade into two digits, then use a ```switch``` statement to test the ten's digit.

#include <stdio.h>

void main ()  
{
    int i;
    printf("Decimal Grade?\n");
    scanf("%d", &i);
    i = i / 10;
    switch (i) {
        case 10:
            printf("A+\n");
            break;
        case 9:
            printf("A\n");
            break;
        case 8:
            printf("B\n");
            break;
        case 7:
            printf("C\n");
            break;
        case 6:
            printf("D\n");
            break;
        default:
            printf("F\n");
            break;
    }
}