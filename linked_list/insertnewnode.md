### Inserting a new node at the beginning of a doubly linked list
```
//Usually done with setting up the struct node

Step 1: IF AVAIL = NULL //check to see if Memory is available
    Write OVERFLOW
    Go to Step 9
    [END OF IF]
Step 2: SET NEW_NODE = AVAIL  // allocate memory if it is available 
Step 3: SET AVAIL = AVAIL NEXT // allocate memory for pointer

//Reassigning pointers and setting new start
Step 4: SET NEW_NODE -> DATA = VAL
Step 5: SET NEW_NODE -> PREV = NULL
Step 6: SET NEW_NODE -> NEXT = START
Step 7: SET START -> PREV = NEW_NODE
Step 8: SET START = NEW_NODE
Step 9: EXIT
```
 


### Algorithm to insert new node at the end
```
Step 1: IF AVAIL = NULL //check to see if Memory is available
    Write OVERFLOW
    Go to Step 11
    [END OF IF]
Step 2: SET NEW_NODE = AVAIL  // allocate memory if it is available 
Step 3: SET AVAIL = AVAIL NEXT // allocate memory for pointer

//Reassigning pointers and navigate to the end
Step 4: SET NEW_NODE -> DATA = VAL
Step 5: SET NEW_NODE -> PREV = NULL
Step 6: SET PTR = START
Step 7: Repeat Step 8 while PTR -> NEXT != NULL
Step 8:     SET PTR = PTR -> NEXT
[END OF LOOP]
Step 9: SET PTR -> NEXT = NEW_NODE
Step 10 : SET NEW_NODE -> PREV = PTR
Step 11: EXIT
```


### Algorithm to insert a new node after a given node

```
Step 1: IF AVAIL = NULL //check to see if Memory is available
    Write OVERFLOW
    Go to Step 12
    [END OF IF]
Step 2: SET NEW_NODE = AVAIL  // allocate memory if it is available 
Step 3: SET AVAIL = AVAIL NEXT // allocate memory for pointer

//Reassigning pointers and navigate to the end
Step 4: SET NEW_NODE -> DATA = VAL
Step 5: SET PTR = START
Step 6: Repeat Step 7 while PTR -> DATA != NUM
Step 7: SET PTR = PTR -> NEXT
[END OF LOOP]
Step 8: SET NEW_NODE -> NEXT = PTR -> NEXT
Step 9: SET NEW_NODE -> PREV - PTR
Step 10: SET PTR -> NEXT = NEW_NODE
Step 11: SET PTR -> NEXT-> PREV = NEW_NODE
Step 12: EXIT
```