/******************************************************************************
TSgt Clemente Fernandez
13 October 2021
Linked List Lab
    Create a linked list, the struct members should contain the following:
        Each students initals
        Each students favoriate musical artist
        Each students dream car
        Required data for linked list completion
        May be worth each student just posting their data into discord so everyone can grab off of it
    Create a menu allowing for the following:
        Print out of all students and all of their data
        Print out of all students and only one of their data members on request of user (ex. print out all students and their fav car)
        Print out of only a specific student (This one can be challanging)

******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct student
{
    // the data
    char *initials;
    char *favMusicalArtist;
    char *dreamCar;
    // address of another node
    struct student *next;
}Student;

struct student *head = NULL;
struct student *current = NULL;

//display the list
void printList() {

   struct student *ptr = head;

   //start from the beginning
   while(ptr != NULL) {        
      printf("Initials: %s \nFavorite Muscian: %s Dream Car: %s\n",ptr->initials, ptr->favMusicalArtist, ptr->dreamCar);
      printf("----------------------------------------------\n");
      ptr = ptr->next;
   }
}
// insert link at the first location
void insert(char *initials, char *favMusician, char *dreamCar) {
   // create a link
   struct student *link = (struct student*) malloc(sizeof(struct student));

   // link->key = key;
   link->initials = initials;
   link->favMusicalArtist = favMusician;
   link->dreamCar = dreamCar;

   //point it to old first node
   link->next = head;

   //point first to new first node
   head = link;
}
int main(void)
{
    insert("CF4", "Red Hot Chili Peppers", "1999 Nissan Skyline GT-R");
    insert("AAA", "The Spice Girls", "2000 Geo Metro");
    printList();
}