/******************************************************************************
TSgt Clemente Fernandez
14 October 2021
Lab 1: Creating a Linked List, Then Reversing Its Elements 
    Write a program that creates a linked list of 10 characters, then creates 
    a copy of the list in reverse order.
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct node
{
    // the data
    int data;
    char character;
    // address of next node
    struct node *next;
    // address of prev node
    struct node *prev;

} Node, *ptrNode;

// create the linked list
ptrNode add(ptrNode head, int data, char charData)
{
    // allocate memory for new node
    ptrNode newNode = (Node*)malloc(sizeof(Node));
    
    //strcpy(newNode->character, charData);
    newNode->character = charData;
    newNode->data = data;
    newNode->prev = NULL;
    newNode->next = NULL;
    // if head is empty start new list
    if (head == NULL)
    {
        return newNode;
    }

     // traverse until the end is met
    ptrNode ptr = head;
    while(ptr->next != NULL)
    {
        ptr = ptr->next;
    }
    // set next for last item to newStudent
    ptr->next = newNode;
    newNode->prev = ptr;
    return head;
}
//display the list
void display(ptrNode head) {
   ptrNode ptr = head;

   //start from the beginning
   while(ptr->next != NULL) {        
      printf("Node: %d Character: %c \n",ptr->data, ptr->character);
      ptr = ptr->next;
   }

   printf("Node: %d Character: %c \n",ptr->data, ptr->character);
}
//Prints all elements in linked list in reverse traversal order. 
ptrNode reverse(ptrNode head, ptrNode revHead) {
    // pointer to original linked list head
    ptrNode temp = head;
    
    if(temp == NULL) return revHead; // empty list, exit
    
    // Going to last Node
    while(temp->next != NULL) {
        temp = temp->next;
    }
    
    // Traversing backward using prev pointer
    while(temp != NULL) {

        // adds node using revHead
        revHead = add(revHead, temp->data, temp->character);
        temp = temp->prev;
    }

    return revHead;
}
int checkPalindrone(ptrNode head, ptrNode reverse)
{
    // pointer to original linked list head
    ptrNode looper = head;
    ptrNode revLooper = reverse;
    
    // traverses link list
    while (looper->next != NULL)
    {
        // if data is not equals
        if(looper->data != revLooper->data)
        {
            // returns -1
            return -1;
        }
        looper = looper->next;
        revLooper = revLooper->next;
    }
    return 0;
    
}
int main(void)
{
    // pointer for forward list
    ptrNode headF = NULL;

    // pointer for reverse list
    ptrNode headR = NULL;

    headF = add(headF, 1, 'a');
    headF = add(headF, 2, 'b');
    headF = add(headF, 3, 'c');
    headF = add(headF, 4, 'd');
    headF = add(headF, 5, 'e');
    headF = add(headF, 6, 'f'); 
    display(headF);
    // headR = reverse(headF, headR);
    // display(headR);

    // if(checkPalindrone(headF, headR) < 0)
    // {
    //     printf("Not a palindrone");
    // }
    // else
    // {
    //     printf("this is a palindrone!");
    // }
    return 0;
}