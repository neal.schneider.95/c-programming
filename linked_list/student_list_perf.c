// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Performance/LLLab.html
// Linked List Lab
// Instructions
// Create a linked list, the struct members should contain the following:
// Each students initals
// Each students favoriate musical artist
// Each students dream car
// Required data for linked list completion
// May be worth each student just posting their data into discord so everyone can grab off of it
// Create a menu allowing for the following:
// Print out of all students and all of their data
// Print out of all students and only one of their data members on request of user (ex. print out all students and their fav car)
// Print out of only a specific student (This one can be challanging)
// BONUS
// Add additonal students to the end
// Remove students at the end
// Remove a specific student (This one is challanging)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student {
    char initials[10];
    char artist[40];
    char car[20];
    struct student *next;
}Student, *StudentPtr;

int print_student(StudentPtr stu) {
    StudentPtr curr = stu;
    if (stu==NULL) {
        printf("Null (Empty) student!\n");
        return -1;
    } else {
        printf ("Initals: %s Fav Artist: %s Fav Car: %s\n", 
            stu->initials, stu->artist, stu->car);
        return 0;
    }
}

StudentPtr remove_student (StudentPtr stu, char * stuInitals) {
    StudentPtr curr = stu;
    StudentPtr prev = stu;
    int count = 0;
    if (curr == NULL) {
        return NULL;
    } else if (curr->next == NULL) {  // only one left
        if (strcmp(stuInitals, curr->initials) == 0) {  //if match del and return NULL
            free (curr);  
            return NULL;
        } else {
            return stu;         // if no match, don't change
        }
    }
    while (curr->next) {
        if (strcmp(stuInitals, curr->initials) == 0) {
            prev->next = curr->next;
            free (curr);
            return stu;
        } else {
            prev = curr;
            curr = curr->next;
        }
    }
    return stu;  // didn't find a match, return original
}

int print_students(StudentPtr stu) {
    int count = 0;
    StudentPtr curr = stu;
    while (curr) {
        print_student (curr);
        curr = curr->next;
        count++;
    }
    return count;
}

StudentPtr pop_student (StudentPtr stu){
    StudentPtr curr = stu, prev = NULL;
    if (stu == NULL){                   // Empty list
        return NULL;
    } else if (curr->next == NULL){     // only one item left in list
        free (curr);
        return NULL;
    } else {
        while (curr->next) {
            prev = curr;
            curr = curr->next;
        }
        prev->next = NULL;
        free (curr);
        return stu;
    }
}
//  Append student to end
StudentPtr append_student (StudentPtr head, Student stu) {
    StudentPtr curr = head;
    StudentPtr tmp_stu = (StudentPtr) malloc (sizeof (Student));  // Create new Student node
    strcpy (tmp_stu->initials, stu.initials);
    strcpy (tmp_stu->artist, stu.artist);
    strcpy (tmp_stu->car, stu.car);
    tmp_stu->next = NULL;                            // since it will be at the end, next should be NULL
    if (curr) {                     // if not empty...                     
        while (curr->next) {        // ...advance to end of list
            curr = curr->next;
        }
        curr->next = tmp_stu;       // insert tmp student at end
        return head;
    } else {                                        // if Empty, return new node
        return tmp_stu;
    }
}

void main () {
    StudentPtr head = NULL;
    head = append_student (head, (Student) {"AKA", "Picasa", "Ford"});
    head = append_student (head, (Student) {"YOLO", "NIN", "Tesla"});
    head = append_student (head, (Student) {"BOD", "Starset", "Chevy"});
    print_students (head);

    printf("popping 1\n");
    head = pop_student (head);
    print_students (head);
    printf("popping 2\n");
    head = pop_student (head);
    print_students (head);
    printf("popping 3\n");
    head = pop_student (head);
    print_students (head);

    head = append_student (head, (Student) {"AKA", "Picasa", "Ford"});
    head = append_student (head, (Student) {"YOLO", "NIN", "Tesla"});
    head = append_student (head, (Student) {"BOD", "Starset", "Chevy"});
    print_students (head);
    head = remove_student (head, "YOLO");
    printf ("removing YOLO\n");
    print_students (head);
}