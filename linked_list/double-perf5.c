/* Doubly Linked List implementation */
#include<stdio.h>
#include<stdlib.h>

typedef struct node  {
    int data;
    struct node* next;
    struct node* prev;
}Node, *NodePtr;


//Creates a new Node and returns pointer to it. 
NodePtr GetNewNode(int x) {
     NodePtr newNode = (NodePtr)malloc(sizeof(Node));
    newNode->data = x;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

//Inserts a Node at head of doubly linked list
NodePtr InsertAtHead(NodePtr head, int x) {
    NodePtr newNode = GetNewNode(x);
    if(head == NULL) {
        head = newNode;
        return head;
    }
    head->prev = newNode;
    newNode->next = head; 
    head = newNode;
    return head;
}

//Inserts a Node at tail of Doubly linked list
NodePtr InsertAtTail(NodePtr head, int x) {
    NodePtr temp = head;
    NodePtr newNode = GetNewNode(x);
    if(head == NULL) {
        head = newNode;
        return head;
    }
    while(temp->next != NULL) temp = temp->next; // Go To last Node
    temp->next = newNode;
    newNode->prev = temp;
    return head;
}

//Get size of list
int Size(NodePtr head) {
    NodePtr temp = head;
    int count = 1;
    if (temp) {
        while (temp->next) {
            temp->next = temp->next->next;
            count++;
        }
        return count;
    } else {
    return 0;
    } 
}

NodePtr DelNode (NodePtr head, int pos) {
    NodePtr tmp=head;
    if (tmp) {
        for (int i=0; i < pos && tmp->next; {i++; tmp = tmp->next} )
    }
}

//Prints all the elements in linked list in forward traversal order
void Print(NodePtr head) {
    NodePtr temp = head;
    printf("Forward: ");
    while(temp != NULL) {
        printf("%d ",temp->data);
        temp = temp->next;
    }
    printf("\n");
}

//Prints all elements in linked list in reverse traversal order. 
void ReversePrint(NodePtr head) {
    NodePtr temp = head;
    if(temp == NULL) return; // empty list, exit
    // Going to last Node
    while(temp->next != NULL) {
        temp = temp->next;
    }
    // Traversing backward using prev pointer
    printf("Reverse: ");
    while(temp != NULL) {
        printf("%d ",temp->data);
        temp = temp->prev;
    }
    printf("\n");
}

int main() {
    NodePtr head; 
    /*Driver code to test the implementation*/
    head = NULL; // empty list. set head as NULL. 
    printf ("Size is %d\n", Size(head));
    
    // Calling an Insert and printing list both in forward as well as reverse direction. 
    head = InsertAtTail(head, 2); Print(head); ReversePrint(head);
    printf ("Size is %d\n", Size(head));
    head = InsertAtTail(head, 4); Print(head); ReversePrint(head);
    head = InsertAtHead(head, 6); Print(head); ReversePrint(head);
    head = InsertAtTail(head, 8); Print(head); ReversePrint(head);
    
    printf ("Size is %d\n", Size(head));
}