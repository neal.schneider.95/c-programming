#include <stdio.h>
#include <math.h>
#include "shared.h"


// Calculate the perimeter of a rectangle
int main()
{
    double width = get_double("Enter the width: ", 1, 100);
    double length = get_double("Enter the length: ", 1, 100);

  /* Calculate Area of the square */
  double perimeter = 2 * (length + width);
  printf("perimeter of the rectangle : %0.4f\n", perimeter);
}