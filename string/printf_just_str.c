#include <stdio.h>

void main() {
    char word[20] =  "Sahara";
    printf("Default         |%s|\n", word);
    printf("Right Justified |%12s|\n", word);
    printf("Left Justified  |%-12s|\n", word);
}