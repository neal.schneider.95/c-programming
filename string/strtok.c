#include <stdio.h>
#include <string.h>

void main() {
    char address[] = "Washington DC,20059";
    char delim[] = " ,";               // Delimiters--splits with ' ' & ','
    char *token;                       // Temp string for tokens

    token = strtok(address, delim);    // Set up strtok
    while (token) {                    // Quit if NULL
        printf("%s\n", token);         // Print token
        token = strtok(NULL, delim);   // Get next token
    }
    printf("Address after tokenization: %s\n", address);
}