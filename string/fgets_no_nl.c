#include <stdio.h>
#include <string.h>

void main() {
    char name[10] = {};
    printf("Your name, please: ");
    fgets(name, 10, stdin);
    if (name[strlen(name) - 1] == '\n') {   // is the last char a newline?
        name[strlen(name) - 1] = 0;         // if so, replace with a null char.
    }
    printf("Welcome to class, %s!\n", name);
}