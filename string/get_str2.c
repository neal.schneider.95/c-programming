#include <stdio.h>

void main() {
    char name[10] = {};
    printf("Your name, please: ");
    scanf("%10s", name);
    //      └┴── String Width
    printf("Welcome, %s!\n", name);
}