#include <stdio.h>

void main() {
    char name[10] = {};
    printf("Your name, please: ");
    scanf("%10s", name);
    //      └┴── Maximum string width
    printf("Welcome to class, %s!\n", name);
}