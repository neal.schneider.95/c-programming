#include <stdio.h>
void main () 
{
    char names[5][8] = {
        {'L', 'e', 'o'}, 
        {'C', 'h', 'r', 'i', 's', 0}, 
        {'E', 'l', 'l', 'e', 'n', '\0'}, 
        {'T', 'o', 'm', '\0'}, 
        {'J', 'o', 'e', '\0'}
    };
    
    for (int name = 0; name < 5; name++)
    {
        for (int c = 0; c < 8; c++) 
        {
            printf("%c", names[name][c]);
        }
        printf("\n");
    }
}