#include <stdio.h>
#include <string.h>

void main() {

    char s1[] = "blue";
    char *s2 = "blue";
    char s3[] = {'b', 'l'};
    char s4[1024] = "yo!";
    char s5[1024] = {'y', 'o' , '!'};


    printf("Sizeof s1: %lu, sw %lu\n", sizeof(s1), sizeof(s1[0]));
    printf("Sizeof s2: %lu\n", sizeof(s2));
    printf("address of s1: %p, \naddress of s2: %p\n %p", s1, s2, s1);
    printf("%s\n", s1);     /*     displays "blue" */
    printf("S3 = %s\n", s3);
    // scanf("%s", s2);
    printf("%s\n", s1);     /*     displays "al"   */
    s2 = NULL;
    printf("len NULL %lu", strlen(s2));

    for (int i = 0; i<sizeof(s4); i++) {
        printf("%c", s4[i])
    }
    printf("\n");
    for (int i = 0; i<sizeof(s4); i++) {
        printf("%c", s5[i]);
    }
    printf("\n");
}