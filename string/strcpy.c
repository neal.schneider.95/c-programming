#include <stdio.h>
#include <string.h>

void main() {
    char copied_str[200] = "Blah, blah";
    char original_str[] = "The answer is 42!";
    printf ("%s\n", copied_str);
    strcpy(copied_str, original_str);
    printf ("%s\n", copied_str);
    // prints:
}