#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){
    char s1[100] = {0};                 // string 1
    char s2[100] = {0};                 // string 2
    char cmp_result[20] = {0};          // string result for printing
    int cmp_val = 0;                    // for result of strncmp
    int char_count = 0;                 // n for strncmp

    printf("Compare two Strings.\n");      // Info for user
    printf("Use how many characters?: ");
    scanf("%5d", char_count);              // Safely get input

    printf("String 1:");
    scanf("%100s", s1)                     // get string with scanf

    printf("String 2:");
    // fgets(s2, 100, stdin);              // get string 2
    // s2[strnlen(s2, 100) - 1] = 0;       // overwrite the ending \n char
    scanf("%100s", s2)                     // get string with scanf


    // strncmp compares char_count characters of the strings
    // essentially, for each character starting at 0, it
    // subtracts s2[i] - s1[i] until it either gets to char_count
    // or the result of the subtraction is not zero.
    // If any character diff is not zero it returns that result.
    // If it hits the end of the string or char_count without finding
    // a difference, it returns 0.
    cmp_val = strncmp(s1, s2, char_count);

    // set cmp_result based on outcome of comparison
    if (cmp_val < 0) {
        strcpy(cmp_result, "greater than"); // copy text to result string
    } else if (cmp_val > 0) {
        strcpy(cmp_result, "less than");
    } else {
        strcpy(cmp_result, "equal to");
    }

    printf("\nComparing %d chars,\n%s is %s %s (cmp = %d)\n",
        char_count, s1, cmp_result, s2, cmp_val);
}