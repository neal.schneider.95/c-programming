#include <stdio.h>
#include <string.h>     // where strstr and strlen are defined 

int main() {
    char *searchStr;    // Pointer to use to get each found term
                        // After it's assigned, it *is* a string
    char phrase[1024] = {0};
    char needle[100] = {0};
    printf("Enter a phrase to search: ");
    fgets(phrase, 1024, stdin);                 // get the phrase
    phrase[strlen(phrase)-1] = 0;               // replace \n with null char

    printf("What am I looking for? ");
    fgets(needle, 100, stdin);                  // get the search string
    needle[strlen(needle)-1] = 0;

    searchStr = strstr(phrase, needle);
    // loop until the search string comes back empty (NULL)
    while (searchStr) {

        // print the current string, starting where needle was found, at searchStr
        printf("%s\n", searchStr);

        searchStr = strstr(searchStr+1, needle);
    }  // end while
}  // end main

/*
Output:
$ gcc lab6substring.c
$ ./a.out
Enter a phrase to search: now good men... let's look for me...  Don't bother searching for meteorites!
What's the serach phrase? me
men... let's look for me...  Don't bother searching for meteorites!
me...  Don't bother searching for meteorites!
meteorites!
*/