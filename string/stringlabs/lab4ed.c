#include <stdio.h>
#include <ctype.h>                  // for tolower
#include <string.h>                 // for strlen

int main () {
    char input[101];                // input buffer
    int pos;
    int len;

    printf("Does it end in ed?\nIf so, I'll echo.\n: ");
    while (1) {             
        fgets(input, 100, stdin);   // get string from user
        len = strlen(input);        // convert to int
        if (len == 1) break;        // quit if empty ("\n")
        
        // since we used fgets, input[len] is \0 and input[len-1] is \n
        // so we will be looking for ed at len-2 and len-3
        
        // && "shortcuts" a bit.  If the first operand is false, it doesn't evaluate the second
        // so if input is NULL, it doesn't go any further, 
        // then if the string is too short (<3) it stops and won't attempt to get a negative index
        if (    input &&
                len > 3 && 
                tolower(input[len-3]) == 'e' && 
                tolower(input[len-2]) == 'd') {
            printf("%s: ", input);              // echo string
        } // end if
    } // end while
    printf("Thanks for playing.\n");
    return 0;
} // end main