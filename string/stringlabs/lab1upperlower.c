#include <stdio.h>
#include <string.h>
#include <ctype.h>                       // Neeeded for toupper/lower

void main(){
    char i;                              // counter
    char s[100] ={0};                    // input string;
    char upper[100], lower[100];         // converted strings
    printf("Enter string to convert: ");
    // scanf("%100s", s);                // Get user input with scanf
    fgets(s, 100, stdin);                // Safely get user input
    if (s[strlen(s) - 1] == '\n') {      // Is the last char a newline?
        s[strlen(s) - 1] = 0;            // If so, replace with a null char.
    }
    
    // loop until we hit the null terminator or end of buffer
    // For logic operations, a null char evaluates to false
    // and all other characters are true
    for (i = 0; s[i] && i<100; i++) {
        upper[i] = toupper(s[i]);
        lower[i] = tolower(s[i]);    
    }
    
    fputs("Upper Case: ", stdout);
    puts(upper);
    fputs("Lower Case: ", stdout); 
    puts(lower);
}
