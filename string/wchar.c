#include <stdio.h>
#include <wchar.h>
void main () {
    printf("wchar size: %lu\n", sizeof (wchar_t));
    printf("WCHAR_MIN: %d, %x\n", WCHAR_MIN, WCHAR_MIN);
    printf("WCHAR_MAX: %d, %x\n", WCHAR_MAX, WCHAR_MAX);
    printf("WEOF: %d, %x\n", WEOF, WEOF);

    wchar_t ws[50] = L"123ABCabc!@#";
    wprintf(L"ws = %s\n", ws);
}