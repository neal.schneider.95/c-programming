#define __STDC_WANT_LIB_EXT1__ 1
#include <stdio.h>

void main() {


    #ifdef __STDC_LIB_EXT1__
    printf("%ld - %lx\n", __STDC_LIB_EXT1__, __STDC_LIB_EXT1__);
    #else
    printf("__STDC_LIB_EXT1__ undefined.\n");
    #endif

    char name[10] = {};
    printf("Your name, please: ");
    // scanf_s("%s", name, 10);
    printf("Welcome to class, %s!\n", name);
}