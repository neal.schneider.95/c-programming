#include <stdio.h>

void main() {
    char *first_names[] = {"Adam", "Bob", "Clair"};
    char *last_names[] = {"Jackson", "Tomlin", "Jingleheimer"};
    float grades[] = {99.1111, 89.8989, 91.99999};

    printf("%-8s %-10s %5s\n", "First", "Last", "Grade");
    for (int i = 0; i < 3; i++) {
        printf("%+8s %-10s %2.2f\n", first_names[i], last_names[i], grades[i]);
    }
}