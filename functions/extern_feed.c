// feed.c
#include <stdio.h>

void main()
{
    extern void call_me(); // 'extern' brings these into scope here
    extern int animals;
    call_me();
    animals = 2;
    printf("%d\n", animals);
}