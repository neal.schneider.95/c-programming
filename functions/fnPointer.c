#include <stdio.h>

void foo (int i)
{
  printf ("foo %d!\n", i);
}

void bar (int i)
{
  printf ("%d bar!\n", i);
}
void message (void (*func)(int), int times);

void main (int want_foo) 
{
  void (*pf)(int) = &bar; /* The & is optional. */
  if (want_foo)
    pf = foo;
  message (pf, 5);
}

void message (void (*func)(int), int k)
{
  int j;
  for (j=0; j<k; ++j)
    func (j);  /* (*func) (j); would be equivalent. */
}