// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Labs/Demonstration/function-prototypes.html

// Demo: "Newline Records"

// int remove_newline(char * buffer);
// We are going to create a function that takes a pointer (don't get too caught up on the use of a poitner) 
// to a buffer, containing a nul-terminated string. We are then going to replace all newline characters 
// with spaces and return the number of newline characters changed.

// Return Value : Number of newline characters changed

// Parameters : Pointer to a null-terminated string

// Purpose : Replace all newline characters with spaces

// Requirments :

// Ensure buffer is not NULL
// Return ERR_NULL_POINTER if buffer is NULL
// Return ERR_NONE_FOUND if no newlines are found

// Neal Schneider
#include <stdio.h>
#define ERR_NULL_POINTER -1
#define ERR_NONE_FOUND 0
#define MAX_BUFF_SIZE 640

int remove_newline(char *buffer) {
    if (buffer == NULL) {
        return ERR_NULL_POINTER;
    }
    int nlCount = 0;
    int i = 0;
    while (buffer[i] && i < MAX_BUFF_SIZE) {
        if (buffer[i] == '\n') {
            buffer[i] = ' ';
            nlCount += 1;
        }
        i+=1;
    }
    return nlCount;
}

int main (){
    int count = 0;
    char string1[] = "\n\n\n\n\n\n\n\n\n\nNot\n much\n here...\n\n\n\n\n\n\n\n";
    char string2[] =    "The quick brown fox jumed over the lazy dog";
    char string3[] = "a\nab\nabc\nabcd\abcde\n";
    char string4[] = "";
    char string5[] = "O say can you see, by the dawn's early light,\n\
What so proudly we hailed at the twilight's last gleaming,\n\
Whose broad stripes and bright stars through the perilous fight,\n\
O'er the ramparts we watched, were so gallantly streaming?\n\
And the rocket's red glare, the bombs bursting in air,\n\
Gave proof through the night that our flag was still there;\n\
O say does that star-spangled banner yet wave\n\
O'er the land of the free and the home of the brave?"; 

    printf ("String 1 is :\n %s\n", string1);
    count = remove_newline(string1);
    printf ("With %i subs, String 1 is now:\n %s\n\n", count, string1);
    
    printf ("String 2 is :\n %s\n", string2);
    count = remove_newline(string2);
    printf ("With %i subs, String 2 is now:\n %s\n\n", count, string2);
    
    printf ("String 3 is :\n %s\n", string3);
    count = remove_newline(string1);
    printf ("With %i subs, String 3 is now:\n %s\n\n", count, string3);
    
    printf ("String 4 is :\n %s\n", string4);
    count = remove_newline(string1);
    printf ("With %i subs, String 4 is now:\n %s\n\n", count, string4);
    
    printf ("String 5 is :\n %s\n", string5);
    count = remove_newline(string5);
    printf ("With %i subs, String 5 is now:\n %s\n\n", count, string5);
    
    printf("remove_newline(NULL) is: %d\n", remove_newline(NULL));
}