// int fibonacci_number(int num);
// Purpose
// Define fibonacci_number() as a recursive function that returns a Fibonacci number of position "num"
// Allow the user to choose how many numbers are calculated
// Parameters
// Fibonacci Sequence number position to calculate
// Return Value
// sequenceNumber
// -1 if sequenceNumber is unrealistic
// The Fibonacci Sequence:
// Starts with 0 and 1 then each subsequent number is calculated by adding the two previous sequence numbers

// F(n) = F(n-1) + F(n-2)

#include <stdio.h>
// global variable to count iterations
int iterations = 0;

int fib (int num) {
    iterations ++;
    if (num <= 1) {
        return num;
    } else {
        return fib(num-1) + fib(num-2);
    }
}

int main () {
    // int sequence_num;
    // printf ("Fibonacci sequence: ");
    // scanf ("%d", &sequence_num);
    // printf ("The Fibonacci number for %i is %llu", sequence_num, fib(sequence_num));
    for (int i = 0; i <40; i++){
        iterations = 0;
        printf ("%d -> %d -- ", i, fib(i));
        printf("Done in %d iterations\n", iterations);
    }
}