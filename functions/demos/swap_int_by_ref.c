#include <stdio.h>

void swap_ints( int * a, int * b) {
    int temp = *a;
    *a = *b;
    *b = temp;
    return;
}

void main () {
    int x = 100;
    int y = 200; 
    printf("x is %d (%p), y is %d (%p).\n", x, &x ,y, &y);
    swap_ints (&x, &y);
    printf("x is %d (%p), y is %d (%p).\n", x, &x ,y, &y);
}