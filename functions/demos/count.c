#include <stdio.h>

void count_down(int num) {
    if (num > 0){
        printf("going down... %d\n", num);
        count_down(num -1);
        printf("on the way up... %d\n", num);
    }
    else
    {
        printf("Blast Off!");
    }
}

int main(void)
{
    int countNum = 0;
    int i = 0;
    printf("Countdown from ");
    scanf("%d", &countNum);

    count_down(countNum);

    return 0;
}
