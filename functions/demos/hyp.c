#include <stdio.h>
#include <math.h>

double hypotenuse(double a, double b) {
    double c = sqrt( a*a + b*b);
    return c;
}

int main () {
    printf("%f %f %f\n", 3.0, 4.0, hypotenuse(3.0, 4.0));
    printf("%f %f %f\n", 5.0, 12.0, hypotenuse(5.0, 12.0));
    printf("%f %f %f\n", 8.0, 15.0, hypotenuse(8.0, 15.0));
}