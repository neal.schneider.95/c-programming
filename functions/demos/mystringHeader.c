// MyStringHeader.c
#include "mystringHeader.h"

extern int print_the_count(char *inputString, int strLen)
{
    char tempChar = 0;
    int table[26] = {0};
    int count = 0;
    if (!inputString) {
        return ERR_NULL_POINTER;
    } 
    else if (!strLen) {
        return ERR_INVALID_LENGTH;
    }
    for (int i = 0; i < strLen; i++) {
        if (inputString[i] >= 65 && inputString[i] <= 122) {
            tempChar = inputString[i];
            tempChar = toupper(tempChar);
            table[tempChar - 65] += 1;
            count++;
        }
    }
    // Print the table 
    for (int i = 0; i < 26; i++) {
        printf ("%c: %d\n", i + 65, table[i]);
    }
    return count;
}
//////////////////////////
// reverse_it definition:
//  Return Value
//      0 on success
//      -1 forwardString is NULL
//      -2 if strLen is zero or less
//  Parameters - A non-NULL terminated string and the length of that string
//  Purpose - Print a non-null terminated string backwards and then print a newline
//     * Write a program that reads a string from user input, call reverse_it(), and then call print_the_count()

int reverse_it(char *forwardString, int strLen)
{
    char tempChar;
    if (!forwardString) {
        return ERR_NULL_POINTER;
    }
    if (strLen <= 0 || forwardString[0] =='\0') {
        return ERR_INVALID_LENGTH;
    }
    for (int i=0; i< strLen/2; i++) {
        tempChar = forwardString[i];
        // printf("%c", tempChar);
        // printf("%c", c2);
        forwardString[i] = forwardString[strLen-i];
        forwardString[strLen-i] = tempChar;
    }
    for (int i=0;i<strLen; i++) {
        printf("%c", forwardString[i]);
    }
    printf("\n");
    return SUCCESS;
}

int main () 
{
    char string[1024];
    char str[] = {"AAAAAaaaaaBBBcccc"};
    char str0[] = {"AAAAAaaaaaBBBcccc"};
    char *str2 = {"AAAAAaaaaaBBBcccc"};
    char *str3 = {"AAAAAaaaaaBBBcccc"};
    // int counter = print_the_count(str, 15);
    // printf("\nTotal Count: %d", counter);

    
    reverse_it(str2, 17);
}