#include <stdio.h>
#include <string.h>

char scope[] = {"Global\n"};
int main(void)
{
    int i = 0;    //iterating var
    printf("%s", scope);
 
    //this array is local to main()
    
    char scope[] = {"main\n"};
    printf("%s", scope);
    for (i = 0; i < 3; i++)
    {
        char scope[] = "Xmen";
        //this array is local to the for loop
        strcpy(scope, "for\n");
        printf("%s", scope);
    }
    printf("%s", scope);

}