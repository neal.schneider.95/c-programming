#include <stdio.h>

int incrementer (int i) {
    static int calls = 0;
    calls++;
    printf( "%d calls\n", calls);
    return i++;
}

int main () {
    printf("In Main\n");
    for (int i=0; i< 400; i += 20){
        incrementer (i);
        printf ("i = %d, ", i, incrementer(i));
    }
}

// extern int ext_var;
// int main (){
//     {
//         static int ext_var;
//         for (int i=0; i<10; i++){
//             printf("%d\n", i);
//             ext_var = i;
//         }
//     }
//     // extern int ext_var;
//     printf ("%d", ext_var);
// }