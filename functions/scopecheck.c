#include <stdio.h>

char scope[] = {"Global\n"};
int main(void)
{
    {
        static int i = 0;    //iterating the var
        printf("%s", scope);
    
        //this array is local to main()
        
        char scope[] = {"main\n"};
        printf("%s", scope);
        for (int i = 0; i < 3; i++)
        {
            //this array is local to the for loop
            char scope[] = {"for\n"};
            printf("%s", scope);
        }
        i = 99;
        
    }
    int i;
    printf ("%d", i);
}