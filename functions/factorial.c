#include <stdio.h>

int fact( long long n)
{
    //base case
    if(0 == n )
    {
        return 1;
    }
    //recursive call
    else
    {
        return n * fact(n - 1);
    }
}

int main () {
    long long temp; 
    for (int i = 0; i<20 ; i++) {
        temp  = fact (i);
        printf ("%3llu: %10llu \n", i, temp);
    }
}