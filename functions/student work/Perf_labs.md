# Student Performance Labs

## (Rounding Numbers) 
Function floor may be used to round a number to a specific decimal place. The statement:
```c
y = floor( x * 10 + .5 ) / 10;
```

rounds x to the tenths position (the first position to the right of the decimal point). The statement

```c
y = floor( x * 100 + .5 ) / 100;
```

rounds x to the hundredths position (the second position to the right of the decimal point). Write a program that defines four functions to round a number x in various ways
```
roundToInteger( number )

roundToTenths( number )

roundToHundreths( number )

roundToThousandths( number )
```
For each value read, your program should print the original value, the number rounded to the nearest integer, the number rounded to the nearest tenth, the number rounded to the nearest hundredth, and the number rounded to the nearest thousandth.


## For each of the following sets of integers, write a single statement that will print a number at random from the set.
```
2, 4, 6, 8, 10.

3, 5, 7, 9, 11.

6, 10, 14, 18, 22.
```
 ## (Hypotenuse Calculations)
 Define a function called hypotenuse that calculates the length of the hypotenuse of a right triangle when the other two sides are given. The function should take two arguments of type double and return the hypotenuse as a double. Test your program with the side values specified:
 |**Triangle**|**Side 1**|**Side 2**|
 |-|-|-|
 |1|3.0|4.0| 
 |2|5.0|12.0|
 |3|8.0|15.0|
 
 ## (Even or Odd) 
 Write a program that inputs a series of integers and passes them one at a time to function even, which uses the remainder operator to determine whether an integer is even. The function should take an integer argument and return 1 if the integer is even and 0 otherwise.
 
##  (Project: Drawing Shapes with Characters)
 Use techniques similar to those developed in Demo labs to produce a program that graphs a wide range of shapes.
 
 ## (Time in Seconds) 
 Write a function that takes the time as three integer arguments (for hours, minutes, and seconds) and returns the number of seconds since the last time the clock “struck 12.” Use this function to calculate the amount of time in seconds between two times, both of which are within one 12-hour cycle of the clock.
 
##  (Temperature Conversions)
 Implement the following integer functions:

Function celsius returns the Celsius equivalent of a Fahrenheit temperature.

Function fahrenheit returns the Fahrenheit equivalent of a Celsius temperature.

Use these functions to write a program that prints charts showing the Fahrenheit equivalents of all Celsius temperatures from 0 to 100 degrees, and the Celsius equivalents of all Fahrenheit temperatures from 32 to 212 degrees. Print the outputs in a tabular format that minimizes the number of lines of output while remaining readable.
 
 
 
 
 
 
 
 
 
 
 
