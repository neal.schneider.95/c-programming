/*
Name: Angel Angeles
Project: Assignment - Control Flow 2 - 4  
Date: 28 SEP 2021
*/

#include <stdio.h>

void diamond(int size);

int main(void) {
    int size = 0;
    printf("Enter diamond size: ");
    scanf("%i", &size);
    diamond(size);
    return 0;
}

void diamond(int size) {
    if (size < 3) {
        size = 3;
    } else if (size % 2 == 0) {
        size++;
    }

    for (int x = 0; x < size/2 + 1; x++) {
        for (int xy = size/2 - x; xy > 0; xy--) {
            printf(" ");
        }
        for (int xz = (x * 2) + 1; xz > 0; xz--) {
            printf("*");
        }
        printf("\n");
    }

    for (int a = 0; a < size/2; a++) {
        for (int ab = -1; ab < a; ab++) {
            printf(" ");
        }
        for (int ac = size - 2 - (2 * a); ac > 0; ac -= 1) {
            printf("*");
        }
        printf("\n");
    }
}
