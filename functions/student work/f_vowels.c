/*
TSgt Clemente Fernandez
29 September 2021
Lab 5:
    Write a program to check for a vowel in a string( implement a switch statement)
*/
#include <stdio.h>
#include <string.h>

int check_it(char *string, int len);

int main(void)
{
    // var
    char strUserInput[255];
    // Why did the below not work?
    // char *strUserInput; 
    // user prompt
    printf("What do you wanna check? ");
    scanf("%s", strUserInput);
    int len = strlen(strUserInput); // length of string
    
    check_it(strUserInput, len);
}

int check_it(char *string, int len)
{
    // starting at index pos 0, until the length, incrementing by one
    for (int i = 0; i < len; i++)
    {
        switch (string[i])
        {
        // catches a and A
        case 'a':
        case 'A':
            printf("Found vowel %c\n", string[i]);
            break;
        // catches e and E
        case 'e':
        case 'E':
            printf("Found vowel %c\n", string[i]);
            break;
        // catches i and I
        case 'i':
        case 'I':
            printf("Found vowel %c\n", string[i]);
            break;
        // cathces o and O
        case 'o':
        case 'O':
            printf("Found vowel %c\n", string[i]);
            break;
        // catches u and U
        case 'u':
        case 'U':
            printf("Found vowel %c\n", string[i]);
            break;
        default:
            break;
        }
    }
}
