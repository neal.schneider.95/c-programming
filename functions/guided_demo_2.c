int add(int,int);  /* function prototype */
void main()
{
   int x=1,y=2,z;
   z=add(x,y);      //     FUNCTION call
   printf("z=%d",z);
}

/* FUNCTION DEFINITION */
int add(int a, int b)
{
   return(a+b);
}