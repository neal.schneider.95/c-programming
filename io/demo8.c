// demo8.c
# include <stdio.h>
# include <stdlib.h>
void main()
{
  FILE *fp;
  char c;
  fp=fopen("lines.txt","w");
  if(fp==NULL)
  return;
  else
  {
    while( (c=getchar())!='*')
    fputc(c,fp);
  }
  fclose(fp);
}
