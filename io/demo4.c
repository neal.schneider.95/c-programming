// demo4.c
#include <stdio.h>
#include <stdlib.h>

void main()
{
  FILE *fp;
  char c=' ';
  fp=fopen("data.txt","a+");
  if(fp==NULL)
  {
    printf("Can not open file");
    exit(1);
  }
  printf("Write data & to stop press '.' :");
  while(c!='.')
  {
    c=getchar();
    fputc(c,fp);
  }
  printf("\n Contents read :");
  rewind(fp);
  while(!feof(fp))
  printf("%c",getc(fp));
}