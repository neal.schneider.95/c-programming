#include <stdio.h>
#include <stdlib.h>

void main()
{
  FILE *f;
  char c;
  f=fopen("list.txt","r");
  if(f==NULL)
  {
    printf("\nCannot open file");
    exit(1);
  }
  while((c=getc(f))!=EOF)
  printf("%c",c);
  fclose(f);
}