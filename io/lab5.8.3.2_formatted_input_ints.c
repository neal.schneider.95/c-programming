// Part 1
// Formatted Input (strings)
// Read a first, middle and last name as input into separate char arrays
// Specify a field-width to protect against buffer overflow
// Ensure the field-width leaves room for a nul-terminator
// Specify a ("\t") as a delimitating character
// Print the full name, separating each string with a tab and newline ("\t\n")
// Part 2
// Formatted Input (numbers)
// Read two integers from one line
// Format the input so that the integers are separated by an asterisk (*) x*y (e.g., 2*3, 11*14)
// Reprint the two integers and result as if the answer were being read by a human

// ////////////// EXAMPLE #1 ///////////////
// OUTPUT:  Enter two integers, separated by a *, to be multiplied…
// INPUT:   2*3
// OUTPUT:  The result of 2 multiplied by 3 is 6.  
// ////////////// EXAMPLE #2 ///////////////
// OUTPUT:  Enter two integers, separated by a *, to be multiplied…
// INPUT:   11*14
// OUTPUT:  The result of 11 multiplied by 14 is 154.

#include <stdio.h>
#define BUFF_SIZE 100

int main (){
int a;
int b;

    printf("Enter Two numbers to multiply.\n: ");
    scanf("%20i%*c%20i", &a, &b);

    printf("The result of %d multiplied by %d is %d.\n", a, b, a*b);
}
