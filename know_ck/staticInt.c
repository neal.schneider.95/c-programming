#include <stdio.h>
int f() {
    static int num;
    ++num;
    printf("%d", num);
}
int main() {
    f();
    f();
    f();
}