#include <stdio.h>
#include <stdint.h>
 
void main() {
    int16_t x = 42;
    int16_t y = x >> 1;
    int16_t z = x / 2;
    printf("%d >> 1 = %d; %d / 2 = %d\n", x, y, x, z);
 
    int16_t i = 256;
    int16_t j = i >> 4;
    int16_t k = i / 16;
    printf("%d >> 4 = %d; %d / 16 = %d\n", i, j, i, k);
 
    i = -i;
    j = i >> 4;
    k = i / 16;
    printf("%d >> 4 = %d; %d / 16 = %d\n", i, j, i, k);
}