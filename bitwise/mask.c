// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/performance_labs/Perf_labs.html
// 1. Write a program to shift the entered number by three bits left and display the result.
// 2. Write a program to shift the entered number by five bits right and display the result
// 3. Write a program to mask the most significant digit of the entered number. Use AND operator.
// 4. Write a program to enter two numbers and find the smallest out of them. Use conditional operator.

#include <stdio.h>
void printBinary(unsigned n)
{
    if (n > 1)
        printBinary(n >> 1);
    printf("%d", n & 1);
}

void main (){
    int num, mask;
    printf ("Enter a number to mask: ");
    scanf("%x", &num);
    printf ("Enter mask: ");
    scanf("%x", &mask);
    printf ("Mask %x of %x is %x\n", mask, num, num & mask);
    printf ("Mask %d of %d is %d\n:", mask, num, num & mask);
    printBinary(num);
    printf("\n");
    printBinary(mask);
    printf("\n");
    printBinary(num & mask);
    printf("\n");
}