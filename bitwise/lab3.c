#include <stdio.h>
void printBinary(unsigned int n)
{
    if (n > 1){
        printBinary(n >> 1);
    printf("%d", n & 1);
    };
}

void main () {
    unsigned int myint = 0x00FF00FF, bitToFlip, mask, result;
    printf("Enter bit to flip (0-%lu)\n",(sizeof(unsigned int)*8)-1);
    printf("%x <- myint\n",myint);
    scanf("%d", &bitToFlip);
    mask = 1<<bitToFlip;
    result = mask ^ myint;
    printf("%X\n", myint);
    printf("%X\n", result);
    printf("%d\n", myint);
    printf("%d\n", result);
    printBinary(myint);
    printf("\n");
    printBinary(result);
    printf("\n");
}