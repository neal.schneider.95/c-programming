#include <stdio.h>
#include <stdint.h>

void main() {
    int32_t a;
    printf("Input a decimal integer between 0 and %d: ", INT16_MAX);
    scanf("%d", &a);

    int32_t b;
    printf("Input a decimal integer between 0 and 15: ");
    scanf("%d", &b);

    int32_t c;
    
    printf("\n--- LEFT SHIFT ---\n");
    c = a << b;
    printf("%d << %d = %d\n", a, b, c);
    printf("%08x << %d = %08x\n", a, b, c);

    printf("\n--- RIGHT SHIFT ---\n");
    c = a >> b;
    printf("%d >> %d = %d\n", a, b, c);
    printf("%08x >> %d = %08x\n", a, b, c);

    a = -a;
    c = a >> b;
    printf("%d >> %d = %d\n", a, b, c);
    printf("%08x >> %d = %08x\n", a, b, c);
}
