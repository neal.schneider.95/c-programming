#include <stdio.h>
#include <stdint.h>
 
void main() {
    int16_t a = 1776;  // bin: 0000 0110 1111 0000    hex: 06f0
    int16_t b = 42;    // bin: 0000 0000 0010 1010    hex: 002a
    int16_t c = a | b; // bin: 0000 0110 1111 1010    hex: 06fa
    printf("%04hx | %04hx = %04hx\n", a, b, c);

    //added
    int result = -0x40 << 1;
    printf( "%x \n", result);

    unsigned short d = 0xFFFF;
    printf ("%x >> 3 = %x \n", d, d >> 3);
}