#include <stdio.h>
void printBinary(unsigned n)
{
    if (n > 1)
        printBinary(n >> 1);
    printf("%d", n & 1);
}

void main () {
    for (int i = 0; i < 256; i ++) {
        printf ("%d, %x, ", i, i);
        printBinary(i);
        printf(("\n"));
    }
}