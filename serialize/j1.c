#include "json.h"
#include <stdio.h>

void main () {
    struct JsonNode *jp, *new_node;
    const char s1[] = "{\"name\": \"Thing\", \"age\": 22, \"gender\": \"F\"}";
    printf("s1: %s\n", s1);

    struct JsonNode *j1 = json_decode(s1);
    char* s2 = json_encode(j1);
    printf("s2: %s\n", s2);

    jp = json_find_member(j1, "age");
    printf("age: %s\n", json_encode(jp));

    struct JsonNode *j2 = json_mkstring("Abby");
    printf("j2: %s\n", json_encode(j2));

    json_append_member(j1, "Nickname", j2);
    printf("%s\n", json_encode(j1));
}
