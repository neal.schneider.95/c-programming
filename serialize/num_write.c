#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

#define SIZE (50)

int main() {
    int nums[SIZE];
    int nums2[SIZE];
    for (int i = 0; i < SIZE; i++) {
        nums[i] = i * i;
    }
    for (int i = 0; i < SIZE; i++) {
        nums2[i] = htonl(nums[i]);
    }
    
    // Open a binary file for writing
    FILE *file = fopen("nums.dat", "wb");

    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    // Serialize and write the data to the file
    size_t num_items_written = fwrite(nums2, sizeof(int) * SIZE, 1, file);

    if (num_items_written != 1) {
        perror("Error writing to file");
        fclose(file);
        return 1;
    }

    // Close the file
    fclose(file);

    printf("Serialization complete!\n");

    return 0;
}
