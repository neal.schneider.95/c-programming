#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* TWIN_PRINT_OUT_TO_FILE = "{\"name\": \"%s\",\"age\": %d, \"gender\": \"%c\"}\n";
// const char* TWIN_PRINT_IN_TO_STRUCT = "{\"name\": \"%[^\"]\",\"age\": %d,\"gender\": \"%[^\",]\"}\n";
const char* TWIN_PRINT_IN_TO_STRUCT = "{\"name\": \"%[^\"]\",\"age\": %d,\"gender\": \"%[^ ,\"]\"}\n";

typedef struct Twin {
    char name[20];
    int age;
    char gender;
} Twin;

int main(int argc, char* argv[]) {
    // Initialize Twin variable
    Twin t1 = {
        .name = "Thing",
        .age = 22,
        .gender = 'F'
    };

    // Struct to hold data written from file
    Twin t2 = {};

    // Save the t1 struct to a file
    FILE* open_file = fopen("twin_json.json", "w+");
    // Check if file opened
    if (open_file == NULL) {
        printf("Could not open file. Exiting\n");
        return 1;
    }

    // Write structure to the file
    fprintf(open_file, TWIN_PRINT_OUT_TO_FILE, t1.name, t1.age, t1.gender);
    // Move the cursor back to the beginning of the file after writing
    fseek(open_file, 0, SEEK_SET);
    // Deserialize data from file and write to another struct
    // const char* TWIN_PRINT_IN_TO_STRUCT = "{\"name\": \"%[^\"]\",\"age\": %d,\"gender\": \"%[^ ,\"]\"}\n";
    int res = fscanf(open_file, TWIN_PRINT_IN_TO_STRUCT, t2.name, &t2.age, &t2.gender);
    fclose(open_file);
    // Print the struct
    printf("Result: %d", res);
    printf("\nT2 name: %s\nT2 age: %d\nT2 gender: %c\n", t2.name, t2.age, t2.gender);
    return 0;
}
