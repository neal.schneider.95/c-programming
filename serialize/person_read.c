#include <stdio.h>
#include <string.h>

// Define a struct to represent a person
struct Person {
    int age;
    char name[50];
    float height;
};

int main() {
    // Create an instance of the struct to store the read data
    struct Person person;

    // Open the binary file for reading
    FILE *file = fopen("person.dat", "rb");

    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    // Read the serialized data from the file into the struct
    size_t num_items_read = fread(&person, sizeof(struct Person), 1, file);

    if (num_items_read != 1) {
        perror("Error reading from file");
        fclose(file);
        return 1;
    }

    // Close the file
    fclose(file);

    // Display the deserialized data
    printf("Name: %s\n", person.name);
    printf("Age: %d\n", person.age);
    printf("Height: %.2f\n", person.height);

    return 0;
}
