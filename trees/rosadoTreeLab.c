// Tree traversal in C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node
{
    char item[256];
    struct node *left;
    struct node *right;
};

//Inorder traversal
void inorderTraversal(struct node *root)
{
    // Your code here
    if (root != NULL)
    {
        printf("%s\n", root->item);
        inorderTraversal(root->left);
        inorderTraversal(root->right);
    }
}

// preorderTraversal traversal
void preorderTraversal(struct node *root)
{
    // Your code here
    if (root != NULL)
    {
        preorderTraversal(root->left);
        printf("%s\n", root->item);
        preorderTraversal(root->right);
    }
}

// postorderTraversal traversal
void postorderTraversal(struct node *root)
{
    // Your code here
    // Your code here
    if (root != NULL)
    {
        postorderTraversal(root->left);
        postorderTraversal(root->right);
        printf("%s\n", root->item);
    }
}

// Create a new Node
struct node *createNode(char value[])
{

    struct node *newNode = malloc(sizeof(struct node));
    strcpy(newNode->item, value);
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

// Insert on the left of the node
struct node *insertLeft(struct node *root, char value[])
{
    root->left = createNode(value);
    return root->left;
}

// Insert on the right of the node
struct node *insertRight(struct node *root, char value[])
{
    root->right = createNode(value);
    return root->right;
}

struct node * insert(struct node *root, char value[])
{
    if (root == NULL)
        return createNode(value);

    int compare = strcmp(root->item, value);

    if (compare > 0)
    {
        // root->left = insertLeft(root->left, value);
        root->left = insertLeft(root, value);
    }
    else if (compare < 0)
    {
        // root->right = insertRight(root->right, value);
        root->right = insertRight(root, value);
    }

    return root;
}

int main()
{
    // struct node *root = createNode(1);
    //  insertLeft(root, 12);
    //  insertRight(root, 9);

    // insertLeft(root->left, 5);
    // insertRight(root->left, 6);

    //printf("Inorder traversal \n");
    // inorderTraversal(root);

    //printf("\nPreorder traversal \n");
    // preorderTraversal(root);

    //printf("\nPostorder traversal \n");
    // postorderTraversal(root);

    //open the file on read mode
    FILE *myFile_ptr = fopen("storms2.txt", "r");    

    //variables for tracking
    int counter = 0;
    char huricaneName[20] = {0};
    struct node *root;

    if (myFile_ptr != NULL)
    {
        while (fscanf(myFile_ptr, "%s%*[^\n]", huricaneName) != EOF)
        {
            //get the first word of the current line in the storms2.txt file
            // fscanf(myFile_ptr, "%s%*[^\n]", huricaneName);
            printf("%s", huricaneName);
            if (huricaneName != "EOF")
            {
                //only when adding the first item as root 
                if (counter == 0)
                {
                    root = createNode(huricaneName);
                    //increase by one so additional root is created
                    counter++;
                }
                else
                {
                    //insert additional items to the tree node
                    insert(root, huricaneName);
                }
            }
            else
            {
                //break out of while whe reach end of file
                break;
            }
            
        }
    }
    //close the file
    fclose(myFile_ptr);

    printf("Inorder traversal \n");
    inorderTraversal(root);

    printf("\nPreorder traversal \n");
    preorderTraversal(root);

    printf("\nPostorder traversal \n");
    postorderTraversal(root);
}
