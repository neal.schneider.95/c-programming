#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    char value;
    struct node *left, *right;
}Node, *Node_ptr;



struct node *new_node(char value)
{
    Node_ptr tmp = (Node_ptr)calloc(1,sizeof(struct node));
    tmp->value = value;
    tmp->left = tmp->right = NULL;
    return tmp;
}

void print(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        print(root_node->left);
        printf("%c \n", root_node->value);
        print(root_node->right);
    }
}

Node_ptr insert_node(Node_ptr node, char value) // inserting nodes!
{
    if (node == NULL) return new_node(value);
    if (value < node->value)
    {
        node->left = insert_node(node->left, value);
    }
    else if (value > node->value)
    {
        node->right = insert_node(node->right, value);
    }
    return node;
}
// Pretty-print Tree (-ish)
void printPadding( char c, int n){
    for (int i = 0; i<n; i++) putchar(c);
}
void ppTree (struct node *root, int level) {
    int i; 
    if (root) {
        ppTree(root->right, level+1);
        printPadding('\t', level);
        printf("%c\n", root->value);
        ppTree(root->left, level+1);
    } else {
        printPadding('\t', level);
        puts("*");
    }
} 

int main()
{

printf("Implementation of a Binary Tree in C.\n");

struct node *root_node = NULL;
root_node = insert_node(root_node, 36);
insert_node(root_node, 'm');
insert_node(root_node, 'e');
insert_node(root_node, 'r');
insert_node(root_node, 'o');
insert_node(root_node, 'c');
insert_node(root_node, 'a');

print(root_node);

ppTree(root_node,0);

return 0;
}