#include<stdio.h>
#include<stdlib.h>

typedef struct node
{
    char value;
    struct node *left, *right;
}Node, *Node_ptr;



struct node *new_node(char value)
{
    Node_ptr tmp = (Node_ptr)calloc(1,sizeof(struct node));
    tmp->value = value;
    tmp->left = tmp->right = NULL;
    return tmp;
}

void print(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
    {
        print(root_node->left);
        print(root_node->right);
        printf("%c \n", root_node->value);
    }
}

void postOrder(struct node *root) {
    if (root){
        postOrder(root->left);
        postOrder(root->right);
        printf("%c", root->value);
    }
}
Node_ptr insert_node(Node_ptr node, char value) // inserting nodes!
{
    if (node == NULL) return new_node(value);
    if (value < node->value)
    {
        node->left = insert_node(node->left, value);
    }
    else if (value > node->value)
    {
        node->right = insert_node(node->right, value);
    }
    return node;
}
// Pretty-print Tree (-ish)
void printPadding( char c, int n){
    for (int i = 0; i<n; i++) putchar(c);
}
void ppTree (struct node *root, int level) {
    int i; 
    if (root) {
        ppTree(root->right, level+1);
        printPadding('\t', level);
        printf("%c\n", root->value);
        ppTree(root->left, level+1);
    } else {
        printPadding('\t', level);
        puts("*");
    }
} 

int main()
{

printf("Implementation of a Binary Tree in C.\n");

struct node *root_node = NULL;
root_node = insert_node(root_node, 'H');
insert_node(root_node, 'C');
insert_node(root_node, 'Q');
insert_node(root_node, 'B');
insert_node(root_node, 'D');
insert_node(root_node, 'A');
insert_node(root_node, 'P');
insert_node(root_node, 'Z');
insert_node(root_node, 'X');

// insert_node(root_node->left->left, 'B');
// insert_node(root_node->left, 'C');
// insert_node(root_node->left->left->left, 'A');
// insert_node(root_node->left->right, 'D');
// insert_node(root_node->right, 'Q');
// insert_node(root_node->right->left, 'P');
// insert_node(root_node->right->right, 'Z');
// insert_node(root_node->right->right->left, 'X');

print(root_node);

ppTree(root_node,0);

postOrder(root_node);
printf("\n");

return 0;
}