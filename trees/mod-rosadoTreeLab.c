// Tree traversal in C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node
{
    char item[256];
    struct node *left;
    struct node *right;
};

//Inorder traversal
void inorderTraversal(struct node *root)
{
    // Your code here
    if (root != NULL)
    {
        printf("%s\n", root->item);
        inorderTraversal(root->left);
        inorderTraversal(root->right);
    }
}

// preorderTraversal traversal
void preorderTraversal(struct node *root)
{
    // Your code here
    if (root != NULL)
    {
        preorderTraversal(root->left);
        printf("%s\n", root->item);
        preorderTraversal(root->right);
    }
}

// postorderTraversal traversal
void postorderTraversal(struct node *root)
{
    // Your code here
    // Your code here
    if (root != NULL)
    {
        postorderTraversal(root->left);
        postorderTraversal(root->right);
        printf("%s\n", root->item);
    }
}

// Create a new Node
struct node *createNode(char value[])
{

    struct node *newNode = malloc(sizeof(struct node));
    strcpy(newNode->item, value);
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

// Insert on the left of the node
struct node *insertLeft(struct node *root, char value[])
{
    root->left = createNode(value);
    return root->left;
}

// Insert on the right of the node
struct node *insertRight(struct node *root, char value[])
{
    root->right = createNode(value);
    return root->right;
}

struct node * insert(struct node *root, char value[])
{
    if (root == NULL)
        return createNode(value);

    int compare = strcmp(root->item, value);

    if (compare > 0)
    {
        root->left = insert(root->left, value);
    }
    else if (compare < 0)
    {
        root->right = insert(root->right, value);
    }

    return root;
}


// Pretty-print Tree (-ish)
void printPadding( char c, int n){
    for (int i = 0; i<n; i++) putchar(c);
}
void ppTree (struct node *root, int level) {
    if (root) {
        ppTree(root->right, level+1);
        printPadding('\t', level);
        printf("%10s\n", root->item);
        ppTree(root->left, level+1);
    } else {
        printPadding('\t', level);
        puts("\t*");
    }
} 

int main()
{
    // struct node *root = createNode(1);
    //  insertLeft(root, 12);
    //  insertRight(root, 9);

    // insertLeft(root->left, 5);
    // insertRight(root->left, 6);

    //printf("Inorder traversal \n");
    // inorderTraversal(root);

    //printf("\nPreorder traversal \n");
    // preorderTraversal(root);

    //printf("\nPostorder traversal \n");
    // postorderTraversal(root);

    //open the file on read mode
    FILE *myFile_ptr = fopen("storms2.txt", "r");    

    //variables for tracking
    int counter = 0;
    char huricaneName[20] = {0};
    int category;
    int year;

    struct node *root;

    if (myFile_ptr != NULL)
    {
        while (fscanf(myFile_ptr, "%s %d %d ", huricaneName, &year, &category) != EOF)
        {
            printf("%s", huricaneName);
            //get the first word of the current line in the storms2.txt file
            //only when adding the first item as root 
            if (counter == 0)
            {
                root = createNode(huricaneName);
                //increase by one so additional root is created
                counter++;
            }
            else
            {
                //insert additional items to the tree node
                insert(root, huricaneName);
            }

        }
    }

    //close the file
    fclose(myFile_ptr);
    ppTree(root,0);

    printf("Inorder traversal \n");
    inorderTraversal(root);

    printf("\nPreorder traversal \n");
    preorderTraversal(root);

    printf("\nPostorder traversal \n");
    postorderTraversal(root);
}
