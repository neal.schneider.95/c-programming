#include<stdio.h>
#include<stdlib.h>
struct node
{
    int value;
    struct node *left, *right;
};

struct node *new_node(int value)
{
    struct node *tmp = (struct node *)malloc(sizeof(struct node));
    tmp->value = value;
    tmp->left = tmp->right = NULL;
    return tmp;
}

void print(struct node *root_node) // displaying the nodes!
{
    if (root_node != NULL)
{
    print(root_node->left);
    printf("%d \n", root_node->value);
    print(root_node->right);
}
}

struct node* insert_node(struct node* node, int value) // inserting nodes!
{

    if (node == NULL) return new_node(value);
    if (value < node->value)
    {
        node->left = insert_node(node->left, value);
    }
    else if (value > node->value)
    {
        node->right = insert_node(node->right, value);
    }
    return node;
}



// Pretty-print Tree (-ish)
void printPadding( char c, int n){
    for (int i = 0; i<n; i++) putchar(c);
}
void ppTree (struct node *root, int level) {
    int i; 
    if (root) {
        ppTree(root->right, level+1);
        printPadding('\t', level);
        printf("%d\n", root->value);
        ppTree(root->left, level+1);
    } else {
        printPadding('\t', level);
        puts("*");
    }
} 


int main()
{

printf("Implementation of a Binary Tree in C.\n");

struct node *root_node = NULL;
root_node = insert_node(root_node, 10);
insert_node(root_node, 10);
insert_node(root_node, 30);
insert_node(root_node, 25);
insert_node(root_node, 36);
insert_node(root_node, 56);
insert_node(root_node, 78);

print(root_node);

ppTree(root_node,0);

return 0;
}