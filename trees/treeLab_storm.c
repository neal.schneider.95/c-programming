#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define BUFF_SIZE 20

typedef struct node
{
    int category;
    int year;
    char name[BUFF_SIZE];
    struct node *left, *right;
} Node, *Node_ptr;

struct node *new_node(char* name, int year, int cat)
{
    Node_ptr tmp = (Node_ptr)calloc(1,sizeof(Node));
    tmp->category = cat;
    tmp->year = year;
    strcpy(tmp->name, name);
    tmp->left = tmp->right = NULL;
    return tmp;
}

void print(struct node *root) // displaying the nodes
{
    if (root != NULL)
{
    print(root->left);
    printf("%15s %4u %5u\n", root->name, root->category, root->year);
    print(root->right);
}
}

Node_ptr insert_node(Node_ptr curr_node, char* name, int year, int cat) // inserting nodes!
{
    if (curr_node == NULL) return new_node(name, year, cat);
    int compare = strncmp(curr_node->name, name, BUFF_SIZE);
    if (compare > 0)
    {
        curr_node->left = insert_node(curr_node->left, name, year, cat);
    }
    else if (compare < 0)
    {
        curr_node->right = insert_node(curr_node->right, name, year, cat);
    }
    return curr_node;
}
// Pretty-print Tree (-ish)
void printPadding( char c, int n){
    for (int i = 0; i<n; i++) putchar(c);
}
void ppTree (struct node *root, int level) {
    if (root) {
        ppTree(root->right, level+1);
        printPadding('\t', level);
        printf("%10s\n", root->name);
        ppTree(root->left, level+1);
    } else {
        printPadding('\t', level);
        puts("\t*");
    }
} 

int main()
{
    int category;
    int year;
    char name[BUFF_SIZE];
    struct node *root_node = NULL;
    int read_result = 0;

    FILE *f = fopen("storms2.txt", "r");
    if (!f) {
        printf("FILE ERR\n");
        return -1;
    }
    read_result = fscanf(f, "%20s %d %d", name, &year, &category);
    printf("%s %i %i\n", name, year, category); // debugging print
    root_node = new_node(name, year, category); 
        while (read_result != EOF) // Continue reading until EOF Found
        {
             // read until newline char or CHAR_BUFFER is reached
            insert_node(root_node, name, year, category);
            read_result = fscanf(f, "%20s %d %d", name, &year, &category);
            printf("%s %i %i\n", name, year, category); // debugging print
        }  

    printf("Strongest Hurricanes between 1950 and 2002\n");
    printf("%15s  %4s%5s\n","Name", "Cat", "Year");

    print(root_node);

    printf("\n\n\n Pretty print of tree:\n");

    ppTree(root_node,0);

    return 0;
}