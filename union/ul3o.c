#include <string.h>
#include <stdio.h>

void main() {
    union {         // Anoymous union
        float b;
        int count;
        char str[10];
    } u = { 999.9 }; // 'u' defined/initialized here
    
    u.b = 3.14;
    printf("Float: %e\n  -> int: %i\n  -> str: %10s\n", u.b, u.count, u.str);

    u.count = 144;
    printf("Int: %d\n  -> flt: %e\n  -> str: %10s\n", u.count, u.b, u.str);

    strcpy(u.str, "Howdy!");
    printf("Str: %10s\n  -> flt: %e\n  -> int: %d\n", u.str, u.b, u.count);
}