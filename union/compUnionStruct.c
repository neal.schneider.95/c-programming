#include <stdio.h>
#include <stdint.h>

union uni {     // Union's name established here as 'uni'
    uint32_t i; // Member 'i' is a 32 bit (4 byte) int
    float f;    // Member 'f' is a 32 bit (4 byte) float
    char c[4];     // Member 'c' is a 1 byte char
};
union uni u; // = {1,2,3};    // Variable 'u' declared as an 'union uni'

struct str {
    uint32_t i;
    float f;
    char c;
};  
struct str s;   // Variable 's' declared as a 'struct str'

void main() {
    printf("size of u:   %2ld, address: %p\n", sizeof (union uni), &u);
    printf("size of u.i: %2ld, address: %p\n", sizeof u.i, &u.i);
    printf("size of u.f: %2ld, address: %p\n", sizeof u.f, &u.f);
    printf("size of u.c: %2ld, address: %p\n\n", sizeof u.c, &u.c);

    printf("size of s:   %2ld, address: %p\n", sizeof (struct str), &s);
    printf("size of s.i: %2ld, address: %p\n", sizeof s.i, &s.i);
    printf("size of s.f: %2ld, address: %p\n", sizeof s.f, &s.f);
    printf("size of s.c: %2ld, address: %p\n\n", sizeof s.c, &s.c);
}

    // u.f = 1887.0;
    // printf ("float -> int %d \n", u.i);
    // u.i = 1;
    // printf ("int -> float %e \n", u.f);

    // char *cp = &u.c;
    // *cp = *(cp+1) = *(cp+2) = *(cp+3) = 0x0F;
    // printf ("0xFFFF FFFF -> float %e \n", u.f);

    // *cp = *(cp+1) = *(cp+2) = *(cp+3) = 0x00;
    // printf ("0x0000 0000 -> float %e \n", u.f);

    // *cp = *(cp+1) = 0;
    // *(cp+2) = 0x80;
    // *(cp+3) = 0xFF;
    // printf ("0x7F80 0000 -> float %e \n", u.f);
