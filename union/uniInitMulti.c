#include <stdio.h>
#include <stdint.h>

void main () {
    union uni {
        uint32_t i;
        float f;
        char c;
    } my_union, u2 = { 42 };  // Also declares 'my_union' and 'u2' as 'union uni' variables
    // 'u2' is initialized to 42, but 'my_union' is not

    union uni w = { 42 };             // Should only be initialized with an unsigned int.
    union uni u = {"you"};            // <-- This will cause a compile warning. (Wrong type)
    union uni v = { 1, 3.14, "you" }; // <-- Another warning: Too many items.  Can only be one item.
}