#include <stdio.h>

// Define a union that can store either an integer, a float, or a string
union Value {
    int i;
    float f;
    char str[20];
};

// Define a struct that represents a variable, which can have a name and a value of any type
struct Variable {
    char name[20];
    enum { INT, FLOAT, STRING } type;
    union Value value;
};

int main() {
    // Create some variables of different types
    struct Variable var1 = {"x", INT, { .i = 42 }};
    struct Variable var2 = {"y", FLOAT, { .f = 3.14 }};
    struct Variable var3 = {"z", STRING, { .str = "hello" }};

    // Print the values of the variables
    printf("%s = ", var1.name);
    if (var1.type == INT) {
        printf("%d\n", var1.value.i);
    } else if (var1.type == FLOAT) {
        printf("%f\n", var1.value.f);
    } else if (var1.type == STRING) {
        printf("%s\n", var1.value.str);
    }

    printf("%s = ", var2.name);
    if (var2.type == INT) {
        printf("%d\n", var2.value.i);
    } else if (var2.type == FLOAT) {
        printf("%f\n", var2.value.f);
    } else if (var2.type == STRING) {
        printf("%s\n", var2.value.str);
    }

    printf("%s = ", var3.name);
    if (var3.type == INT) {
        printf("%d\n", var3.value.i);
    } else if (var3.type == FLOAT) {
        printf("%f\n", var3.value.f);
    } else if (var3.type == STRING) {
        printf("%s\n", var3.value.str);
    }

    return 0;
}