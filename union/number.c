#include <stdio.h>

union number {
  int integer;
  float floating_point;
};

int main() {
  union number num;

  num.integer = 42;
  printf("Integer value: %d\n", num.integer);

  num.floating_point = 3.14;
  printf("Floating point value: %f\n", num.floating_point);

  return 0;
}