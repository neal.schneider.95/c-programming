#include <stdint.h>
#include <stdio.h>
void main() {
    struct u {
        float f; 
        long long L;
        long double D;
        char s[16];
    };
    struct u u;
    printf("size of u: %3ld, address: %p\n", sizeof (u), &u);
    printf("size of f: %3ld, address: %p\n", sizeof (u.f), &u.f);
    printf("size of D: %3ld, address: %p\n", sizeof (u.D), &u.D);
    printf("size of s: %3ld, address: %p\n", sizeof (u.s), &u.s);
    printf("size of L: %3ld, address: %p\n", sizeof (u.L), &u.L);

    typedef union address {
        uint32_t bin;
        uint8_t byte[4];
    } Address;

    Address a;
    printf("IPv4 address ");
    printf("%hd.", a.byte[3] = 192);
    printf("%hd.", a.byte[2] = 168);
    printf("%hd.", a.byte[1] = 0);
    printf("%hd ", a.byte[0] = 1);
    printf("= 0x%8X in hex\n", a.bin);
}