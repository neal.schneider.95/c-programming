#include <stdio.h>

// Define a union that can store either an integer, a float, or a string
typedef union value {
    int i;
    float f;
    char str[20];
} Value;

// Define a struct that represents a variable using Value
typedef struct vari {
    int  type;          // 1: int, 2: float, 3:str
    Value value;
} Vari;

void main() {
    int getVari(Vari *q) {
        int ret;
        printf("Enter a type (1:int, 2:float, 3:20 char string): ");
        scanf ("%1d", &q->type);
        if (q->type < 1 || q->type > 3) {
            return 1;
        }
        if (q->type == 1) {
            printf("Enter an int: ");
            ret = scanf ("%i", &q->value.i);
        } else if (q->type == 2) {
            printf("Enter a float: ");
            ret = scanf ("%f", &q->value.f);
        } else {
            printf("Enter a string: ");
            ret = scanf ("%20s", q->value.str);
        }
        if (ret == 1 ) {
            return 0;
        } else {
            char junk[1024];
            scanf("%1024s", junk);
            return 2;
        }
    }

    void printVari(Vari q) {
        if (q.type == 1) {
            printf("int: %d\n", q.value.i);
        } else if (q.type == 2) {
            printf("flt: %f\n", q.value.f);
        } else {
            printf("str: %s\n", q.value.str);
        }
    }

    // Create some variables of different types
    Vari v[4];
    int ret, i = 0;

    // Get the values from the user
    while (i < 4) {
        ret = getVari(v+i);
        if (ret) {
            printf("Error.  Try again.\n");
        } else {
            i++;
        }
    }

    // Print the Values
    for (int i = 0; i < 4; i++) {
        printf("%1d - ", i+1);
        printVari(v[i]);
    }
}