#include <stdio.h>
#include <stdint.h>

void main() {
    union ipv4 {
        uint8_t octet[4];
        uint32_t address;
    };
    union ipv4 a;

    a.octet[3] = 192;
    a.octet[2] = 168;
    a.octet[1] = 0;
    a.octet[0] = 1;
    printf("%d.%d.%d.%d is %X\n",
        a.octet[3], a.octet[2], a.octet[1], a.octet[0],
        a.address);
}