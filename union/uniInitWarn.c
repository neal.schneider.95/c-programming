#include <stdio.h>
#include <stdint.h>

void main () {
    union uni {
        uint32_t i;
        float f;
        char c[8];
    };

    union uni w = { 42 };             // Should only be initialized with an unsigned int.
    union uni u = {"you"};            // <-- This will cause a compile warning. (Wrong type)
    union uni v = { 1, 3.14, "you" }; // <-- Another warning: Too many items.  Can only be one item.
}