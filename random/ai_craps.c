#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int roll_dice(void);

int main(void) {
    int wins = 0;
    int losses = 0;
    int round_lengths[20] = { 0 };
    int i;

    srand(time(NULL));

    for (i = 0; i < 1000000; i++) {
        int round_length = 0;
        int point = roll_dice();
        round_length++;

        if (point == 7 || point == 11) {
            wins++;
        } else if (point == 2 || point == 3 || point == 12) {
            losses++;
        } else {
            int roll;
            do {
                roll = roll_dice();
                round_length++;
            } while (roll != point && roll != 7);

            if (roll == point) {
                wins++;
            } else {
                losses++;
            }
        }

        if (round_length < 20) {
            round_lengths[round_length]++;
        }
    }

    printf("Percentage of wins: %f\n", (double) wins / (wins + losses));
    printf("Round length distribution:\n");
    for (i = 1; i < 20; i++) {
        printf("%d: %d\n", i, round_lengths[i]);
    }

    return 0;
}

int roll_dice(void) {
    return rand() % 6 + rand() % 6 + 2;
}
