
//////////////ASSERT EXAMPLE 2///////////////////

#include <assert.h>     //defines assert()
#include <stdio.h>      //defines string-related functions

int main(void)
{
    int numerator, denominator;
    float result;
    puts("Enter two numbers to divide (ex. 5 / 3)");
    scanf("%d / %d", &numerator, &denominator);

    //assert that the denominator is not 0
    assert(denominator);

    result = (float)numerator / denominator;
    //print the string
    printf("Result: \t%.2f", result);
    return 0;
}