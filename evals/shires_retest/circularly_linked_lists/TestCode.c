#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct numNode *buildCList(int *nums, int size)
{
    //check for nums null
    if(nums == NULL){
        return NULL;
    }

    //check for valid size
    else if(size < 1){
        return NULL;
    }

    
    struct numNode *head = NULL;
    struct numNode *tail = NULL;
    //struct numNode *tail = NULL;
    //struct numNode *newnode = (struct numNode*)malloc(sizeof(struct numNode));
    struct numNode *test = malloc(sizeof(struct numNode));

    if(!test){
            return NULL;
        }
    
    for (int i = 0; i < size; i++){
        test->num = nums[i];
        test->next = NULL;
        
        if(!head){
            // printf("head is being created \n");
            head = test;
            //tail = head;
            head->next = head;
        }
        else{
            // printf("head exists \n");
            struct numNode *oldHead = NULL;
            oldHead = head;
            head = test;
            head->next = oldHead;

            // printf("new head = %d \n", head->num);
            //printf("new tail.next = %d \n", head->num);
            // printf("new head.next = %d \n", head->next->num);
        }
        
    }

    

    // struct numNode *curr = head;
    // for(int i = 0; i < size; i++){tail.next
    //     // printf("%d \n", curr->num);
    //     curr = curr->next;
    // }

    // while(curr->next != head->next){
    //     printf("%d \n", curr->num);
    //     curr = curr->next;
    // }
    //returns pointer to the head variable
    return head;
}
//empty the list and free the pointers
int emptyList(struct numNode *head)
{
    
}

void printList(struct numNode *head)
{
	int count = 0;
	if (!head)
	{
		printf("Empty List\n");
		return;
	}
	struct numNode *link = head;
	do
	{
		printf("%d -> ", link->num);
		link = link->next;
		count += 1;
		// sleep(.05);
	} while (link != head);
	printf("[end (%d)]\n", count);
}

//testing my own stuff
int main(){
    int nums2[] = { 90,80,70,60,50 };

    struct numNode *res = buildCList(nums2, sizeof(nums2) / sizeof(*nums2));
    //EXPECT_FALSE(NULL == res); //res should not be NULL
    struct numNode *temp = res;
    for (int i = 0; i< 5; i++){
        printList(buildCList(nums2, i));
    }
}
