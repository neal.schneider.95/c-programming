#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
//adds a node to the top of the stack takes a pointer to top and data for the new node
int push(struct numNode **top, int data)
{
    //checking that top is not null
    if(!top){
        return 1;
    }
    //allocating memeory for the new node
    struct numNode *newnode = malloc(sizeof(struct numNode));
    
    //checking that newnode is not null
    if(!newnode){
        return 1;
    }

    //giving new node value
    newnode->num = data;
    newnode->next = NULL;

    //checks if there is a top node 
    if(!*top){
        //creates a top if there is not one
        *top = newnode;
    }
    else{
        //if there is a top node we need to add the new node to the top and connect them
        newnode->next = *top;
        *top = newnode;
    }
    return 0;
}

int pop(struct numNode **top)
{
    //check for top null or no top
    if(!top || !*top){
        return 0;
    }
    //temp node to store the popped top
    struct numNode *temp;
    temp = *top;
    //saving the data to be returned
    int data_ret = (*top)->num;
    //moving top to the next node 
    *top = (*top)->next;
    //free the old top
    free(temp);

    return data_ret;
}
//empties the entire stack
void emptyStack(struct numNode **top)
{
//is the stack empty
    if(top == NULL || *top == NULL){
        printf("Stack is Empty\n");
        return;
    }
    else{
        //checking for only one node 
        if((*top)->next == NULL){
            *top = NULL;
        }
        else{
            //while temp is not null continue to pop off
            while(*top){
                struct numNode *temp = NULL;
                temp = *top;
                //move to the next node 
                *top = (*top)->next;
                free(temp);
                
            }
        }


    }
}
//creates a stack using even indices 
struct numNode *createStack(int actions[], int numActions)
{
    //checking for no actions or invalid actions
    if(numActions <= 0){
        return NULL;
    }
    //create node to return
    struct numNode *top = NULL;
    // numActions is not the length of the actions array it is 1/2
    for (int i = 0; i < numActions*2; i += 2)
    {
        //find what action since i will cover the even indexes 
        switch (actions[i])
        {
        case 1:
            pop(&top);
            break;
        case 2:
            push(&top, actions[i + 1]);
            break;
        
        case 3:
            emptyStack(&top);
            break;
        // only three operations permited
        default:
            return NULL;
            break;
        }
    }

    
    //return pointer to top node
    return top;

}
