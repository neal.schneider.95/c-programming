# C Programming: C Stack

## KSAT List

This question is intended to evaluate the following topics:

- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0078: Push and pop a Stack.
- S0062: Create and destroy a Stack.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

## Tasks

This task will implement a simple stack using a linked list. A numNode struct is defined in the `TestCode.h` file to facilitate the creation of the stack.

### Task 1

Implement a `push` function that adds an item to the stack and returns a status code.

**Parameters:**

1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack.
2. `data`: an integer representing the new data to be added to the stack.

**Return:**  an integer containing `0` if successful, or `1` if `top` is null or memory is unavailable.

- update the `top` pointer so it points to the new top item in the stack.

### Task 2

Implement a `pop` function that removes the first item in the stack and returns the data stored in that item.

**Parameters:**

1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack.

**Return:** an integer containing `0` if `top` was null; otherwise, it is the data within the removed node

- update the `top` pointer so that it points to the next item in the stack.
- if there are no remaining items, set `top` to NULL.

### Task 3

Implement an `emptyStack` function that removes all items in the stack.

**Parameters:**

1. `top`: a pointer to a `struct numNode` pointer representing the top of the stack.

**Return:** void

- Remove all nodes in the stack, freeing the memory used by the nodes.
- Ensure the `top` pointer is updated to NULL after the function is done.
- Ensure the `top` parameter that is passed in is not null; if so, return.

### Task 4

Implement the function `createStack` that creates a stack based on the data sent to it and returns the top of the stack when finished.

**Parameters:**

1. `actions`: an int array such that:
   - values in the even indices (0,2,4 etc.) indicate which type of action to do with the stack:
     1. pop the item off the top of the stack
     2. push the next value in the array onto the top of the stack
     3. empty the stack
   - The odd indices will only apply for push actions. If the action received is a push (2), then the following index contains the value to push onto the stack. The odd indices have no effect on pop (1) or empty stack (3).
   - For example, if the function receives: [2, 7, 2, 2, 1, 0, 3, 0], then it would push 7 onto the stack, then push 2 onto the stack, then pop the stack, then empty the stack.
   - Assume that the `actions` array will always have an even number of elements.
2. `numActions`: an int representing the total number of actions in the `actions` array

**Return:**

- a `struct numNode` pointer that represents the top of the stack
- `NULL` if an invalid action number appears in an even index of `actions` or if no data is provided

## Building and Testing Your Solution

The unit tests for your solution are located in `testcases.cpp`.  In order to run the tests, execute the following commands from a terminal in the `c_stack` directory:

1. `cmake .` - you only need to run this once
2. `make` - run this to compile your code and build the executable
3. `make test`

Repeat steps 2 and 3 above every time you make a change.

### Optional

Instead of `make test` you can run the executable file directly with the following command:

1. `./TestCode`
