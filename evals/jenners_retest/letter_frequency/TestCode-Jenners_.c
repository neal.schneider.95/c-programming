/*
SSgt Robert Jenners
9/7/2022
C-Programming Test: Letter Frequency
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Refer to README.md for the problem instructions

/**
 * @brief will determine the total occurrences of each letter of the alphabet in a string and returns a status code
 * 
 * @param sentence 
 * @param frequencyTable 
 * @return int 
 */
int letterFrequency(const char *sentence, int *frequencyTable)
{   

    //This doesn't pass, and isn't a good way to do this, but I couldn't think of another way in time

    
    //string length variable
    int string_length = 0;
    //used to store each character individually
    char c;

    //getting length of sentence
    string_length = strlen(sentence);

    //looping through every letter in the passed in string
    for(int i = 0; i< string_length; i++)
    {
        //storing the individual characters into c
        c = sentence[i];
        //getting int value to compare against ascii value with
        int a = (int)c;

        //making sure the value of a is a letter
        if ((a >= 65 && a <= 90) || (a >= 97 && a <= 122))
        {
            //checking the value of a, and adding to that specific index in the frequencyTable
            switch (a)
            {
            case 65:
                frequencyTable[0]++;
                break;
            case 97:
                frequencyTable[0]++;
                break;
            case 66:
                frequencyTable[1]++;
                break;
            case 98:
                frequencyTable[1]++;
                break;
            case 67:
                frequencyTable[2]++;
                break;
            case 99:
                frequencyTable[2]++;
                break;
            case 68:
                frequencyTable[3]++;
                break;
            case 100:
                frequencyTable[3]++;
                break;
            case 69:
                frequencyTable[4]++;
                break;
            case 101:
                frequencyTable[4]++;
                break;
            case 70:
                frequencyTable[5]++;
                break;
            case 102:
                frequencyTable[5]++;
                break;
            case 71:
                frequencyTable[6]++;
                break;
            case 103:
                frequencyTable[6]++;
                break;
            case 72:
                frequencyTable[7]++;
                break;
            case 104:
                frequencyTable[7]++;
                break;
            case 73:
                frequencyTable[8]++;
                break;
            case 105:
                frequencyTable[8]++;
                break;
            case 74:
                frequencyTable[9]++;
                break;
            case 106:
                frequencyTable[9]++;
                break;
            case 75:
                frequencyTable[10]++;
                break;
            case 107:
                frequencyTable[10]++;
                break;
            case 76:
                frequencyTable[11]++;
                break;
            case 108:
                frequencyTable[11]++;
                break;
            case 77:
                frequencyTable[12]++;
                break;
            case 109:
                frequencyTable[12]++;
                break;
            case 78:
                frequencyTable[13]++;
                break;
            case 110:
                frequencyTable[13]++;
                break;
            case 79:
                frequencyTable[14]++;
                break;
            case 111:
                frequencyTable[14]++;
                break;
            case 80 || 112:
                frequencyTable[15]++;
                break;
            case 80:
                frequencyTable[15]++;
                break;
            case 113:
                frequencyTable[16]++;
                break;
            case 82:
                frequencyTable[17]++;
                break;
            case 114:
                frequencyTable[17]++;
                break;
            case 83:
                frequencyTable[18]++;
                break;
            case 115:
                frequencyTable[18]++;
                break;
            case 84:
                frequencyTable[19]++;
                break;
            case 116:
                frequencyTable[19]++;
                break;
            case 85:
                frequencyTable[20]++;
                break;
            case 117:
                frequencyTable[20]++;
                break;
            case 86:
                frequencyTable[21]++;
                break;
            case 118:
                frequencyTable[21]++;
                break;
            case 87:
                frequencyTable[22]++;
                break;
            case 119:
                frequencyTable[22]++;
                break;
            case 88:
                frequencyTable[23]++;
                break;
            case 120:
                frequencyTable[23]++;
                break;
            case 89:
                frequencyTable[24]++;
                break;
            case 121:
                frequencyTable[24]++;
                break;
            case 90:
                frequencyTable[25]++;
                break;
            case 122:
                frequencyTable[25]++;
                break;
            
            default:
                break;
            }
        }
    }


    
}
