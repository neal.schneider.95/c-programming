/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/

#include <stdio.h>
#include <math.h>
int buyGroceries(int stuff[], int size)
{
	float itemCost[] = {0, 3.5, 2.25, 1.99, 4.15};
    float total = 0;
	float linecost = 0;
    // Check to ensure size is even and at least 2
    if ((size % 2 != 0) || (size < 2)) {
        return 0;
    }
    for (int i = 0; i < size; i += 2){
        // Ensure item is in the list and has at least one item
        if ((stuff[i] < 1) || (stuff[i] > 4) || (stuff[i+1] < 1)) {
            return 0;
        }
        if (stuff[i+1] < 5.0) {
            // Less than 5 items, so add full value
            linecost = itemCost[stuff[i]] * stuff[i+1];
			total += linecost;
        } else {
            // 5 or more items, so add discounted value
            linecost += itemCost[stuff[i]] * stuff[i+1] * 0.95;
			total += linecost;
        }
		// printf("i:%d  #:%f  t:%f\n", i, linecost, total);
		printf("i:%d  #:%d item: %d (%f) t:%f\n", i, stuff[i], itemCost[stuff[i]], linecost, total);
    }
    return (int) (round(total));
}

void printstuff(int stuff[], int size){
	printf("size: %d --> (", size);
	for (int i = 0; i <size; i++) {
		printf("%d, ", stuff[i]);
	}
	printf(")\n");
}

// void main () {
// 	int stuff1[] = {1,1,0,6};
// 	printstuff(stuff1, 4);
// 	printf("e1: %d\n", buyGroceries(stuff1, 4));

// 	int stuff[] = { 1, 3, 2, 5, 4, 4 };
// 	printstuff(stuff, 6);
// 	printf("n1: %d\n", buyGroceries(stuff, 6));

// 	int stuff2[] = { 1, 5, 2, 5, 4, 5, 3, 5 };
// 	printstuff(stuff2, 8);
// 	printf("n1: %d\n", buyGroceries(stuff2, 8));
// }