# KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0058: Create and destroy a Queue.
- S0071: Enqueue and dequeue a Queue.
- S0053: Implement a function that returns a memory reference.
- S0048: Implement a function that receives input parameters.
- S0090: Allocate memory on the heap (malloc).
- S0097: Create and use pointers.
- S0091: Unallocating memory from the heap (free).
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0156: Utilize a struct composite data type.
- S0160: Utilize the standard library.

# Tasks
This task will implement a simple queue using a linked list. A `numNode` struct is defined in the `TestCode.h` file to 
facilitate the creation of the queue. The information below explains the necessary functions to implement.

## enqueue
The `enqueue` function adds an item to the queue and returns 0 when it is successful or 1 otherwise. This function 
takes in two parameters:

1. `end`: a pointer to a `struct numNode` pointer representing the end of the queue. Before this function returns, 
   the `end` pointer is updated so it points to the new last item in the queue.

2. `data`: an integer representing the new data to be added to the queue.

The `enqueue` function should ensure the `end` parameter is not null; if so, it should return 1. Additionally, the 
function should return 1 if memory is unable to be allocated. The expected return type is an integer.

## dequeue
The `dequeue` function removes the first item in the queue and returns the data stored in that item. This function 
takes in one parameter:

1. `front`: a pointer to a `struct numNode` pointer representing the front of the queue. Before this function returns, 
   the `front` pointer is updated so it points to the next item in the queue. If there are no remaining items, it is 
   set to NULL.

The `dequeue` function should ensure the `front` parameter is not null; if so, it should return 0. The expected return 
type is an integer.

## emptyQueue
The `emptyQueue` function removes all items in the queue and returns void. This function takes one parameter:

1. `front`: a pointer to a `struct numNode` pointer representing the front of the queue. Before this function returns, 
   the `front` pointer is updated to NULL.

The `emptyQueue` function should ensure the `front` parameter is not null; if so, it should return. The expected return 
type is void.

## makeQueue
Write the function `makeQueue` that receives two parameters:

1. `actions`: an int array - this array contains values of 1, 2, or 3 in the even indices 0,2,4 etc that indicate 
   which type of action to do with the queue:

   1. dequeue (remove an item from the queue)
   2. enqueue (add an item to the queue)
   3. empty queue

   - The odd indices will only apply for enqueue actions. If the action received is action enqueue (2), then the next 
     index contains the value to add to the queue. The odd indices have no effect on dequeue (1) or empty queue (3)
        
   - So if the function receives:  [2, 7, 2, 2, 1, 0, 3, 0], then you would enqueue 7 to the queue, then enqueue 2 to 
     the queue, then remove an item from the queue, then empty the queue.

2. `numActions`: the total number of actions supplied in the array. In the above example this would be 4.

The function should iterate the actions array and determine what actions should occur on the queue. Depending on the 
action, the appropriate function should be called (i.e. `enqueue`, `dequeue`, or `emptyQueue`). If any action provided is 
not 1, 2, or 3, then return NULL. Assume that the array will always have an even number of elements. The function will 
return a `struct numNode` pointer that represents the front of the queue.
