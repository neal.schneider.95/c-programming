#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
// shires retest

int enqueue(struct numNode **end, int data)
{
    struct numNode *newNode = malloc(sizeof(struct numNode));
    
    //check if end is null
    if(!end){
         return 1;
    }
    //if mem unavailable return 1
    if(!newNode){
        return 1;
    }
    // printf("creating node");
    //give new node value
    newNode->num = data;
    newNode->next = NULL;

    //if the nothing is in queue just make the node
    if(*end == NULL){
        *end = newNode;
        
    }
    //point it to the current end 
    //newNode->next = *end;
    else{
        //old end has to point to newnode
        (*end)->next = newNode;
        

        //make new node the last in line
        *end = newNode; 

        if(!*end){
            return 1;
        }
    }

    //return 0 if successful
    return 0;
}
int dequeue(struct numNode **front)
{
    //check that front exists
    if(!front){
        return 0;
    }

    if(!*front){
        return 0;
    }
    //save the value of the node we are dequeueing 
    int returnValue = (*front)->num;

    //dequeue front of the line and make the next node in line the front
    if((*front)->next != NULL){
        struct numNode *oldFront;
        oldFront = *front;
        *front = (*front)->next;
        oldFront = NULL;
    }
    //if only one in the line then null it out
    if((*front)->next == NULL){
        *front = NULL;
    }
    //return value of dequeued object
    return returnValue;
}

void emptyQueue(struct numNode **front)
{   //check for empty queue
    if(!front){
        return;
    }

    struct numNode *oldFront;
    //walk through and free the nodes 
    while((*front)->next != NULL){
        oldFront = *front;
        *front = (*front)->next;
        free(oldFront);
    }
    //make sure front isn't pointed to empty mem
    *front = NULL;
}

struct numNode *makeQueue(int actions[], int numActions)
{
   
    // int size = 0;
    // while(actions[size]){
    //     size++;
    // }

    // if(size % 2 != 0){
    //     return NULL;
    // }

    if(numActions <= 0){
        return NULL;
    }

    struct numNode *front = NULL;
    struct numNode *end = NULL;

    // numActions is not the length of the actions array it is 1/2
    for (int i = 0; i < numActions*2; i += 2)
    {
        //find what action since i will cover the even indexes 
        switch (actions[i])
        {
        case 1:
            dequeue(&front);
            break;
        case 2:
            if(!front){
                enqueue(&front, actions[i + 1]);
                end = front;
                // printf("**********8");
            }
            else{
                enqueue(&end, actions[i + 1]);
            }
            break;
        case 3:
            emptyQueue(&front);
            break;
        // only three operations permited
        default:
            return NULL;
            break;
        }
    }

    
    // this is returning null
    return front;
}


// int main (){
//     int actions[] = { 2, 4, 2, 5, 2, 10 };
//     struct numNode *head = makeQueue(actions, 3);
//     struct numNode *res = head;
//     if(res){
//         printf("%d", res->num);
//     }
//     else{
//         printf("something went wrong");
//     }
// }
