#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "TestCode.h"

/*
Task #1

Write the function processNames that receives a 10-element array of char *. The
array contains a list of peoples' first names. The processNames function should 
create a linked list using the nameNode struct defined in Testcode.h.
The processNames function should iterate the array and insert the names into nodes 
so they are in ascending (alphabetical) order in the linked list.

The processNames function should return a pointer to the head node of the link 
list once processed.

Task #2

Write the function freeMemory that receives a pointer to your linked-list's head
node so you can iterate over the link list and free all allocated memory.

Neal Schneider, IDF Instructor
1 Sep 22
Subject matter test verification 
*/

struct nameNode *processNames(const char ** names)
{
	struct nameNode *head = NULL, *curr, *prev, *temp;
	if (!names) return NULL;
	for (int i = 0; i < 10; i++) { // Check for NULL strings
		if (!names[i]) return NULL;  
	}
	for (int i = 0; i < 10; i++)
	{
		if (strlen(names[i]) < 1)
			continue;  // Skip empty strings
		// Find correct alphabetical location in list
		curr = prev = head;
		while (curr != NULL && strcmp(curr->name, names[i]) < 0) {
			prev = curr;
			curr = curr->next;
		}
		temp = (struct nameNode *)malloc(sizeof(struct nameNode));
		if (curr == head) {  // If at beginning of list, make it the head.
			head = temp;
		} else 		{
			prev->next = temp;  // else insert at current location
		}
		temp->name = (char*)malloc(11);
		strncpy(temp->name, names[i], 10); // copy name to the node
		temp->next = curr; // point new node's next to next in list
	}
	return head; 	// return the current/updated head
}

void freeMemory(struct nameNode *head)
{
	struct nameNode *temp = head;
	while (temp != NULL)
	{
		head = temp->next;
        free(temp->name);
		free(temp);
		temp = head;
	}
}