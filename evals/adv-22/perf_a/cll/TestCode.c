/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0057 - Create and destroy a Circularly Linked List.
	S0069 - Find an item in a Circularly Linked List.
	S0070 - Add and remove nodes from a Circularly Linked List.
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0108 - Utilize post and pre increment/decrement operators.
	S0048 - Implement a function that receives input parameters.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0053 - Implement a function that returns a memory reference.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0160 - Utilize the standard library.
	S0033 - Utilize assignment operators to update a variable.
	S0090 - Allocate memory on the heap (malloc).
	S0091 - Unallocating memory from the heap (free).
*/

/*

Task #1

Write the function buildCList that receives a pointer to an array of integers (nums)
and the size of the array (size).

The function should create a circular linked list using 
the numNode struct defined below.

The function should iterate the "nums" array and insert the numbers into nodes. 
Each new node will be inserted at the head of the circulary linked list.

if a number in "nums" is already in the circularly linked list, do not insert it into the
circularly linked list and continue interating the "nums" list

The function should return a reference to the head node of the link list once processed.

if the size passed is 0, the function should return NULL.

Task #2

Write the function emptyList that receives a pointer (head) to the head node of the
circularly linked list, and removes each node and frees all the memory. The function should return the number of nodes removed.

if "head" is NULL, the function should return zero (0).

*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"
#include <unistd.h>
// #define RUNLOCAL

struct numNode* ins_node (struct numNode *root, int value) {
	struct numNode *link;
	printf("ins: %d ", value);
	if (root) {
		link = root;
		// not empty so first check for duplicate
		do { 
			// if value is in list already, return without doing anything
			if (link->num == value) return root;
			link = link->next;
		} while (link != root);
		struct numNode *new_Node = (struct numNode*)malloc(sizeof(struct numNode));
		new_Node->next = root;
		new_Node->num  = value;
		// find last node to update its next ptr
		while (link->next != root) {
			link = link->next;
		}
		link->next = new_Node;
		return new_Node;
	} else {
		// empty list, so create initial node
		struct numNode *new_Node = (struct numNode*)malloc(sizeof(struct numNode));
		new_Node->next = new_Node;
		new_Node->num  = value;
			return new_Node;
	}
}

void printList(struct numNode *head)
{
	int count = 0;
	if (!head)
	{
		printf("Empty List\n");
		return;
	}
	struct numNode *link = head;
	do
	{
		printf("%d -> ", link->num);
		link = link->next;
		count += 1;
		sleep(.05);
	} while (link != head);
	// printf("%d ->[end]\n", link->num);
	printf("[end (%d)]\n", count);
}

struct numNode * buildCList(int *nums, int size)
{
    struct numNode *cList = NULL;
	if (!nums || size <=0) return NULL;
	for (int i = 0; i<size; i++) {
		cList = ins_node(cList, nums[i]);
	}
	printList(cList);
	printf("First element: %d\n",cList->num);
    return cList;
}

int emptyList(struct numNode *head)
{
	int count = 0;
	struct numNode *link = head, *tmp; 
	if (head) {  // leave count = 0 if emtpy list
		if (link->next == head) {  // only one item in list
			free (link);
			return 1;
		}
		while (link->next != head) {
			count += 1;
			tmp = link;
			link = link->next;
			free (tmp);
		}
		free (link);
		count++;
	}
	return count;
}

#ifdef RUNLOCAL
void main (){
	int nums[] = {80, 80, 70, 60, 50};
	int num2[] = {90, 80, 70, 60, 50};

	struct numNode *a = NULL;
	struct numNode *t = (struct numNode *)malloc(sizeof(struct numNode));
	printList(a);
	t->num = 1;
	t->next = t;
	printList(t);

	t->next = (struct numNode *)malloc(sizeof(struct numNode));
	t->next->num  = 2;
	t->next->next = t;
	printList(t);
	t = ins_node(t, 2);
	printList(t);
	t = ins_node(t, 1);
	printList(t);

	a = ins_node(a, 20);
	printList(a);
	a = buildCList(nums, 5);
	printList(a);

	a = ins_node(a, 21);
	printList(a);

	a = buildCList(NULL, 1);
	printList (a);
	a = buildCList(nums, 1);
	printList(a);
	a = buildCList(nums, 5);
	printList(a);

	a = buildCList(num2, 0);
	printList(a);
	a = buildCList(num2, 5);
	printList(a);
}
#endif