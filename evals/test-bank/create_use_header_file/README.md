# C Programming: Create and Use Header File
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0158: Create and use header files.
- S0159: Type cast a variable to a different data type.
- S0160: Utilize the standard library.

## Tasks
You need to create functionality to convert Fahrenheit temperatures to Celsius.

### Task 1
Write a header file called `celsius.h` with appropriate header guards that contains a function prototype called 
`getCelsius` (See Task 2 below).

### Task 2
Create a source file called `celsius.c` that has a `getCelsius` function. The `getCelcius` function converts the 
provided temperature to Celsius and returns the converted value.

**PARAMETERS:**
1. `f`: an integer that represents a temperature in Fahrenheit

**RETURN:** a double that contains the temperature in Celsius

### Converting Fahrenheit to Celsius
The formula for conversions is:

(f - 32) * .5556    where f is the provided Fahrenheit temp.

### Task 3
Implement a function `processTemps` that processes an array of temperatures for a given month by returning the 
difference between the highest and lowest temperature. Your given code may or may not need updated.

**PARAMETERS:**
1. `temps`: A two-dimensional array of integers
2. `rows`: An integer representing the number of rows in the array

**RETURN:** a double with the difference between the highest and lowest monthly temperatures, or `-1` as indicated below

- Include your `celsius.h` file so it can be used to covert each temperature in the array to Celsius.
- The array will consist of two columns representing the low temperature (index [0]) and high temperature (index [1]) 
  for a given day. 
- The number of rows will depend on the month and will have a valid range from 28 through 31. 
- Update the temps parameter to accommodate the number of columns.
- Change each value inside the array from Fahrenheit to Celsius. The value returned from your function must be 
  type cast to an int before updating the array.
- Find the lowest and highest Celsius temperature recorded for the provided month.
- If any row value is less than 28 or greater than 31, the function should do no processing and return -1. 
- If any low temperature for a given day is higher than the high temperature for that same day, the function should do 
  no further processing and return -1.

### Example
For example, if the high for the month was 47 and the low 20, the function will return 27.

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
