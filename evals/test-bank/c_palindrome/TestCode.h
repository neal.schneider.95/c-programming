#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    int palindrome(const char *);
#ifdef __cplusplus
}
#endif