#include "TestCode.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#define MIN_FILE_SIZE 150

// Refer to README.md for the problem instructions

int searchFile(const char *fname, const char *str) 
{
    FILE *f;
    int str_count = 0;      // String count to return
    int file_len = 0;       // file length
    int str_len = 0;        // str length
    char *data;             // buffer pointer 
    if (!fname || !fname[0]) return -2;
    if (!str || !str[0]) return -3;
    if (!(f = fopen(fname, "r"))) {
        printf("Could not open file %s\n", str);
        return -3;
    }
    fseek(f, 0L, SEEK_END);
    file_len = ftell(f); 
    if (file_len <= MIN_FILE_SIZE) {
        fclose(f);
        return -2;
    }
    str_len = strlen(str);
    rewind(f);
    mmap(data, file_len, PROT_READ, MAP_PRIVATE, f, 0);
    if (str_len > file_len) return 0;
    for (int i = 0; i < file_len-str_len; i++) {
        if (!strncmp(data+i, str, str_len)) {
            str_count++;
        }
    }
    return str_count;
}
