# C Programming: Files
## KSAT List
This question is intended to evaluate the following topics:
- A0179: Implement file management operations.
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0036: Declare and implement a char * array (string).
- S0033: Utilize assignment operators to update a variable.
- S0042: Open and close an existing file.
- S0043: Read, parse, write (append, insert, modify) file data.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

## Tasks
Implement a function `findStats` that reads and reports voting results from four states 
(Texas, California, New York, and Florida), and returns a status code.

**PARAMETERS:**
1. `state`: an integer representing a state
2. `stats`: a two element integer array that will hold a county number and turnout percentage of the county with the
   highest turnout

**RETURN:** an integer of: `ERROR_SUCCESS` (0) on success; `ERROR_FILE_INVALID` (1006) if the file fails to open; or
`ERROR_INVALID_DATA` (13) if any voting percentage in the file is < 0 or > 100, or if `state` is not 1 - 4

- Voting results are stored in an ascii file with the following information: County number and voter turnout percentage.
- Each line represents this data in a comma delimited format and each value is an integer.
- The files are located in the same directory as your source code. 
- The `state` parameter should have one of the following values:
  1. Texas (indicates to use `texas.txt` file)
  2. California (indicates to use `california.txt` file)
  3. New York (indicates to use `newyork.txt` file)
  4. Florida (indicates to use `florida.txt` file)
- Each element in the `stats` array is initialized to zero
- The function should find the county with the highest voting percentage turnout. 
- Place the county number in the first element of the `stats` array, voting percentage in the second element of the 
  array
- Assume all files, once opened, contain valid format.
- Assume no duplicate high values in a file.

### Example
The following is an example of one line entry in the file:

```text
487,54
```

- 487 is the county number
- 54 is the turnout percentage

## Note
For this problem to work, the current directory should remain the folder with the text files (i.e. the root of the 
question's directory).

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
