#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    char* makeSentence(const char *str);
#ifdef __cplusplus
}
#endif