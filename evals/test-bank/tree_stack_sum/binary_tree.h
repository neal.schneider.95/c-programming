#pragma once

#include "stack.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef struct _Node {
    struct _Node *left;
    struct _Node *right;
    int key;
} node, *p_node;

typedef struct _Tree {
    p_node root;
    unsigned int size;
    p_stack stack;
} tree, *p_tree;

int create_tree(p_tree *pp_tree);
int destroy_tree(p_tree tree_ptr);

int tree_attach_root(p_tree tree_ptr, p_node new_root);

/*
 * For both tree_attach_left and tree_attach_right, the following param docs apply
 * 
 * @param tree_ptr Pointer to the tree which is being modified
 * @param attach_to Pointer to the node, which is in the tree, that the new node shall be attached to the right or left to
 * @param left_node (or right_node) the new node that shall be attached to the tree
 */
int tree_attach_left(p_tree tree_ptr, p_node attach_to, p_node left_node);
int tree_attach_right(p_tree tree_ptr, p_node attach_to, p_node right_node);

p_node create_node(int value);
int destroy_node(p_node target);

#ifdef __cplusplus
}
#endif

#define ATTACH_NODE_SUCCESS 1
#define CREATE_TREE_SUCCESS 1
#define DESTROY_NODE_SUCCESS 1
#define DESTROY_TREE_SUCCESS 1

#define ATTACH_NODE_ERROR (-1)
#define CREATE_TREE_ERROR (-2)
#define DESTROY_NODE_ERROR (-3)
#define DESTROY_TREE_ERROR (-4)
