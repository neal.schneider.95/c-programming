// #define RUNLOCAL 1
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int binToInt (const char *s){
    int a = 0;          // Accumulator
    for (int i = 0; i < strlen(s); i++){
        switch (s[i]) {
            case '0':
                a *= 2;
                break;
            case '1':
                a = a*2 + 1;
                break;
            default: 
                return -2;
        }   // end switch
    }       // end for
    // printf ("%s = %d = %x (%ld chars)\n", s, a, a, strlen(s));
    return a;
}

int bitwiseOps(const char *first, const char *second)
{
    int r;                              // result to return
    if (!first || !second) return -1;   // check for NULLS
    if (strlen(first) !=16 || strlen(second) != 16) return -1; // check for len!=16
    int a = binToInt(first);
    int b = binToInt(second);
    if (a ==-2 || b == -2) return -2;   // check for non-bin #s
    if (a % 2 == 0 && b % 2 == 0)       // Condition 1: evens (and)
        r =  a & b;
    else if (a % 2 == 1 && b % 2 ==1)   // condition 2: odds (or)
        r = a | b;
    else if (a>255 && b>255)            // condition 3: >255 (xor)
        r = a ^ b;
    else r = a + b;                     // everything else (add)
    // printf("%x xx %x = %x (%d)\n", a, b, r, r);
    return r;
}

#ifdef RUNLOCAL
int main () {
    binToInt("0000000000001110");
    binToInt("0000000011101111");
}
#endif