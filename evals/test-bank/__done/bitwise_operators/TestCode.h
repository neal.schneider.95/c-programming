#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    int bitwiseOps(const char *first, const char *second);

#ifdef __cplusplus
}
#endif
