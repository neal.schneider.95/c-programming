#pragma once

#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
    int arrayManipulate(int *, unsigned int);
#ifdef __cplusplus
}
#endif