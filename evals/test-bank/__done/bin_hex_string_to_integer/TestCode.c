#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions


int bin_hex_StrToInt32(const char *s, int mode)
{
    if (!s || (mode < 1 && mode >2)) return ERROR_INVALID_PARAMETER;    // parameter check
    if (strlen(s) <1 ) return ERROR_INVALID_PARAMETER;

    int a = 0;                      // accumulator
    int b = (mode == 1) ? 2: 16;    // character value (base) (bin = 2, hex = 16)
    int c;                          // character value
    for (int i=0;  i<strlen(s); i++) {
        c = tolower(s[i]);
        if (mode == 1) {
            c -= '0';
            if (c<0 || c>1) return ERROR_INVALID_PARAMETER;     // character check for bin
        } else {
            if (c >= '0' && c <='9') {
                c -= '0';
            }  else if (c >= 'a' && c <= 'f') {
                c = c - 'a' + 10;
            }  else return ERROR_INVALID_PARAMETER;             // character check for hex
        }
        a *= b;     // multiply the accumulator by the base
        a += c;     // add the current character value
    }
    return a;
}
