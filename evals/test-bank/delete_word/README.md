# C Programming: Delete Word
## KSAT List
This question is intended to evaluate the following topics:
- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0036: Declare and implement a char * array (string).
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0097: Create and use pointers.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

## Tasks
Implement a function `deleteWord` that deletes (in place) the first occurrence of `word` in the sentence and returns a 
status code when finished.

**PARAMETERS:**
1. `sentence`: a character pointer to a string with the sentence containing the word to delete
2. `word`: a character pointer to the string containing the word to delete

**RETURN:** an integer containing the status code for the function, as explained below

- The `word` should be deleted if it is found regardless of the other surrounding characters
- If the task is successful return ERROR_SUCCESS (0)
- If the sentence or word is an empty string the function should return ERROR_SUCCESS (0)
- If the provided parameters are bad return ERROR_INVALID_PARAMETER (87)
- If the provided 'word' is not part of `sentence` return ERROR_NOT_FOUND (1168)

## Building and Testing
To build and test your code, follow the [compile instructions](https://gitlab.com/90cos/cyv/cyber-capability-developer-ccd/ccd-master-question-file/-/blob/master/performance/exam_files/compile-instructions.md).

Be sure to check your compiler warnings and errors. Warnings often let you know when something you're doing may not be
what you intend.
