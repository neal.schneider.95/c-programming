#pragma once

#ifdef __cplusplus
extern "C" {
#endif

    int fileDump(const char *fname, const char *words[], int wordLen);

#ifdef __cplusplus
}
#endif