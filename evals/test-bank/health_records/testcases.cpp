#include <gmock/gmock.h>
#include "TestCode.h"

/*
01 09 1965
12 08 1955
07 31 1972
11 10 1973
06 20 1941*/

TEST(CalculateAgeInYearsTests, ZeroInputCases)
{
    HealthProfile expected = {0};
    HealthProfile result = {0};
    calculateAgeInYears(result);
    ASSERT_EQ(expected.age, result.age);
}

TEST(CalculateAgeInYearsTests, InvalidDayCases)
{
    HealthProfile expected = {0};
    HealthProfile result = {0};
    result.dateOfBirth.day = -55;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
    result.dateOfBirth.day = 32;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
    result.dateOfBirth.day = 56;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
}

TEST(CalculateAgeInYearsTests, InvalidMonthCases)
{
    HealthProfile expected = {0};
    HealthProfile result = {0};
    result.dateOfBirth.month = -3;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
    result.dateOfBirth.month = 13;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
    result.dateOfBirth.month = 45;
    calculateAgeInYears(result);
    EXPECT_EQ(expected.age, result.age);
}

TEST(CalculateAgeInYearsTests, NormalInputCase)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int currentMonth = tm.tm_mon + 1;
    int currentDay = tm.tm_mday;
    int currentYear = tm.tm_year + 1900;

    int testMonth = 11, testDay = 8, testYear = 1975;

    HealthProfile expected = {0};
    expected.age = (testMonth <= currentMonth && testDay <= currentDay) ? currentYear - testYear : currentYear - testYear - 1;

    HealthProfile result = {0};
    result.dateOfBirth.year = testYear;
    result.dateOfBirth.month = testMonth;
    result.dateOfBirth.day = testDay;
    ASSERT_EQ(expected.age, calculateAgeInYears(result));
}

TEST(CalculateBMITests, NullInputCases)
{
    double expected = 0.0;
    HealthProfile result = {0};
    ASSERT_DOUBLE_EQ(expected, calculateBMI(result));
}

TEST(CalculateBMITests, NormalInputCases)
{
    double expected = 28.0;
    HealthProfile result = {0};
    result.weight = 195;
    result.height = 70;
    ASSERT_DOUBLE_EQ(expected, calculateBMI(result));
}

TEST(CalculateMaxHeartRateTests, zeroInputCase)
{
    int expected = 0;
    ASSERT_EQ(expected, calculateMaxHeartRate(0));
}

TEST(CalculateMaxHeartRateTests, NormalInputCase)
{
    int expected1 = 155;
    int expected2 = 186;
    int expected3 = 140;
    EXPECT_EQ(expected1, calculateMaxHeartRate(65));
    EXPECT_EQ(expected2, calculateMaxHeartRate(34));
    EXPECT_EQ(expected3, calculateMaxHeartRate(80));
}

TEST(CalculateMaxTargetHeartRate, zeroInputCase)
{
    int expected = 0;
    ASSERT_EQ(expected, calculateMaxTargetHeartRate(0));
}

TEST(CalculateMaxTargetHeartRate, NormalInputCase)
{
    double expected1 = 131.75;
    double expected2 = 158.1;
    double expected3 = 119;
    EXPECT_EQ(expected1, calculateMaxTargetHeartRate(155));
    EXPECT_EQ(expected2, calculateMaxTargetHeartRate(186));
    EXPECT_EQ(expected3, calculateMaxTargetHeartRate(140));
}

TEST(CalculateMinTargetHeartRate, zeroInputCase)
{
    int expected = 0;
    ASSERT_EQ(expected, calculateMinTargetHeartRate(0));
}

TEST(CalculateMinTargetHeartRate, NormalInputCase)
{
    double expected1 = 77.5;
    double expected2 = 93;
    double expected3 = 70;
    EXPECT_EQ(expected1, calculateMinTargetHeartRate(155));
    EXPECT_EQ(expected2, calculateMinTargetHeartRate(186));
    EXPECT_EQ(expected3, calculateMinTargetHeartRate(140));
}

TEST(ProcessHealthProfilesTest, NormalCases)
{
    HealthProfile *healthDatabase = processHealthProfiles();
    ASSERT_TRUE(healthDatabase != NULL);

    EXPECT_EQ(0, strcmp("John", healthDatabase[0].firstName));
    EXPECT_EQ(0, strcmp("Smith", healthDatabase[0].lastName));
    EXPECT_EQ('M', healthDatabase[0].gender);
    EXPECT_EQ(1965, healthDatabase[0].dateOfBirth.year);
    EXPECT_EQ(1, healthDatabase[0].dateOfBirth.month);
    EXPECT_EQ(9, healthDatabase[0].dateOfBirth.day);
    EXPECT_EQ(20.3, healthDatabase[0].BMI);

    EXPECT_EQ(0, strcmp("Franklin", healthDatabase[1].firstName));
    EXPECT_EQ(0, strcmp("Wong", healthDatabase[1].lastName));
    EXPECT_EQ('M', healthDatabase[1].gender);
    EXPECT_EQ(1955, healthDatabase[1].dateOfBirth.year);
    EXPECT_EQ(12, healthDatabase[1].dateOfBirth.month);
    EXPECT_EQ(8, healthDatabase[1].dateOfBirth.day);
    EXPECT_EQ(33.4, healthDatabase[1].BMI);

    EXPECT_EQ(0, strcmp("Joyce", healthDatabase[2].firstName));
    EXPECT_EQ(0, strcmp("English", healthDatabase[2].lastName));
    EXPECT_EQ('F', healthDatabase[2].gender);
    EXPECT_EQ(1972, healthDatabase[2].dateOfBirth.year);
    EXPECT_EQ(7, healthDatabase[2].dateOfBirth.month);
    EXPECT_EQ(31, healthDatabase[2].dateOfBirth.day);
    EXPECT_EQ(24.3, healthDatabase[2].BMI);

    EXPECT_EQ(0, strcmp("James", healthDatabase[3].firstName));
    EXPECT_EQ(0, strcmp("Borg", healthDatabase[3].lastName));
    EXPECT_EQ('M', healthDatabase[3].gender);
    EXPECT_EQ(1973, healthDatabase[3].dateOfBirth.year);
    EXPECT_EQ(11, healthDatabase[3].dateOfBirth.month);
    EXPECT_EQ(10, healthDatabase[3].dateOfBirth.day);
    EXPECT_EQ(53.7, healthDatabase[3].BMI);

    EXPECT_EQ(0, strcmp("Jennifer", healthDatabase[4].firstName));
    EXPECT_EQ(0, strcmp("Wallance", healthDatabase[4].lastName));
    EXPECT_EQ('F', healthDatabase[4].gender);
    EXPECT_EQ(1941, healthDatabase[4].dateOfBirth.year);
    EXPECT_EQ(6, healthDatabase[4].dateOfBirth.month);
    EXPECT_EQ(20, healthDatabase[4].dateOfBirth.day);
    EXPECT_EQ(33.4, healthDatabase[4].BMI);

    for (int i = 0; i < 5; i++)
    {
        int maxHeartRate = 220 - healthDatabase[i].age;
        double maxTR = round(0.85 * maxHeartRate*100)/100;
        double minTR = round(0.50 * maxHeartRate*100)/100;

        EXPECT_EQ(maxHeartRate, healthDatabase[i].maxHeartRate);
        EXPECT_EQ(maxTR, healthDatabase[i].maxTargetRate);
        EXPECT_EQ(minTR, healthDatabase[i].minTargetRate);
    }
    
    free(healthDatabase);
}
