#include <stdio.h>
#include <math.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
int invalidGrade (int grade){
    return ((grade < 0) || (grade > 100));
}

int computeScore(int final, int midTerm, int project[], int quiz[])
{
    // Check the bounds of final and midTerm
    if (invalidGrade(final) || invalidGrade(midTerm)) return -1;
    
    // Caculate project score sum.  First locate lowest score to discard
    int minIndex = 0;
    int projectSum = 0;
    int quizSum = 0;
    for (char i = 1; i<6; i++) {
        if (invalidGrade(project[i])) return -1;
        if (project[i] < project[minIndex]) {
            minIndex = i;
        }
    }
    for (char i = 0; i<6; i++) {
        if (i == minIndex) continue;
        projectSum += project[i];
    }
    for (char i = 0; i<4; i++) {
        if (invalidGrade(project[i])) return -1;
        quizSum += quiz[i];
    }
    return round(final * 0.3 +
                 midTerm * 0.25 +
                 quizSum * 0.05 +
                 projectSum * 0.05);
}

void main () {
    int final[] = { 105, 89, 89 };
    int mid[] = { 89, -5, 90 };

    int proj[][6] = { { 88, 78, 98, 92, 70, 92 },
                     { 89, 89, 89, 89, 0, 89 },
                     { 89, 89, 89, 89, 101, 89 } };

    int quiz[][4] = { {80, 85, 95, -1 },
                    { 89, 89, 89, 89 },
                    { 89, 89, 89, 89 } };

    for (int i = 0; i<3; i++){
        printf ("%d: %d\n", i, computeScore(final[i], mid[i], proj[i], quiz[i]));
    }

    // VALID TEST CASES
    int final2[] = { 95, 89, 57 };
    int mid2[] = {89, 89, 82};

    int proj2[][6] = { { 88, 78, 98, 92, 70, 92 },
                     { 89, 89, 89, 89, 89, 89 },
                     { 89, 89, 89, 89, 100, 89 } };

    int quiz2[][4] = { {80, 85, 95, 97 },
                    { 89, 89, 89, 89 },
                    { 74, 69, 80, 81 } };

    for (int i = 0; i<3; i++){
        printf ("%d: %d\n", i, computeScore(final2[i], mid2[i], proj2[i], quiz2[i]));
    }
}