#include <gmock/gmock.h>
#include "TestCode.h"


TEST(TestInvalid, computeScore_invalid_scores)
{
    int final[] = { 105, 89, 89, 89 };
    int mid[] = { 89, -5, 89, 90 };

    int proj[][6] = { { 88, 78, 98, 92, 70, 92 },
                     { 89, 89, 89, 89, 89, 89 },
                     { 89, 89, 89, 89, 89, 89 },
                     { 89, 89, 89, 89, 101, 89 } };

    int quiz[][4] = { {80, 85, 95, -1 },
                    { 89, 89, 89, 89 },
                    { 89, 101, 89, 89 },
                    { 89, 89, 89, 89 } };

	for ( int i = 0; i <4; i++) 
	{
		ASSERT_EQ(0, computeScore(final[i], mid[i], proj[i], quiz[i]));
	}
}

TEST(TestValid1, computeScore_valid1)
{
	int p1[6] = {69, 24, 76, 68, 100, 94};
	int q1[4] = {90, 90, 70, 70};
	ASSERT_EQ(85, computeScore(90, 85, p1, q1));

	int p2[6] = {89, 89, 89, 89, 89, 89};
	int q2[4] = {89, 89, 89, 89};
	ASSERT_EQ(89, computeScore(89, 89, p2, q2));

	int p3[6] = {89, 89, 7, 89, 89, 89};
	int q3[4] = {89, 89, 89, 89};
	ASSERT_EQ(89, computeScore(89, 89, p3, q3));
}