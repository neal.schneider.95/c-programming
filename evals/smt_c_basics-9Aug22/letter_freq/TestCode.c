/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0036 - Declare and implement a char * array (string).
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0110 - Implement error handling.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0160 - Utilize the standard library.
	S0033 - Utilize assignment operators to update a variable.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
// #define RUNLOCAL

/*
 * The function letterFrequency takes as input a one-line strings
 * and determines the total occurrences of each letter of the alphabet in the line.
 *
 * Case sensitivity is not an issue i.e. "A" and "a" are the considered the same for this question
 * If a non-alpha character is encountered it should be ignored, adding nothing to any count.
 *
 * @param sentence			The input string to process
 * @param frequencyTable	An already allocated buffer in which to place the output
 *
 * Expected Return Values:
 *		- The task is successful: 1
 *		- Bad input is provided: 0
 */

int letterFrequency(const char* sentence, int* frequencyTable)
{
	// check for null pointer and empty string
	// safe because of || short-cutting
	if (!sentence || sentence[0] == 0)
		return 0;

	// zeorize the table
	for (int i = 0; i < 26; i++)
		frequencyTable[i] = 0;

	// loop through the string
	for (int i = 0; sentence[i] != 0; i++)
	{
		// get char and convert to upper char
		char c = toupper(sentence[i]);
		// printf("%c:%d, ", c, c-'A');  // debugging check
		if (c >= 'A' && c <= 'Z')
		{ // check for valid chars; skip if not alpha
			frequencyTable[c - 'A'] += 1;
		}
	}
	return 1;
}

#ifdef RUNLOCAL
void main()
{
	int ft[26] = {0};
	const char *stringlist[] = {
		" all lower case!",
		"ALL UPPER CASE ",
		"aaa",
		"THIS IS A SENTENCE IN UPPER CASE. this is another sentence in lower case!"};

	for (int sen = 0; sen < 4; sen++)
	{
		letterFrequency(stringlist[sen], ft);
		printf("%s: \n", stringlist[sen]);
		for (int i = 0; i < 26; i++)
		{
			printf("%d, ", ft[i]);
		}
		printf("\n");
	}
}
#endif