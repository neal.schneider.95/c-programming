/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0097 - Create and use pointers.
	S0098 - Implement a function pointer to call another function.
	S0099 - Use pointer arithmetic to traverse an array.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0053 - Implement a function that returns a memory reference.
	S0048 - Implement a function that receives input parameters.
	S0079 - Validate expected input.
	S0160 - Utilize the standard library.
	S0033 - Utilize assignment operators to update a variable.
	S0090 - Allocate memory on the heap (malloc).
*/

#define _CRT_SECURE_NO_WARNINGS 1

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/*
Using the function pointers techniques to allow the computation of the circumference of a circle, the area of a circle or the volume of a sphere.

Use an array of function pointers in which each pointer represents a function that returns double and receives two parameters:
 - radius: a double parameter
 - message:  a reference to char*
    Note: message is a char * passed by reference. Within each function populate
          this message with one of the following appropriate strings:
             "The circumference of a circle"
             "The area of a circle"
             "The volume of a sphere"
 
 The function names are:
    circumference
    area
    volume

 Each function will return the computation rounded to 2 decimal points.
 

The functionChoice function will be called from the testcases to determine which
of the above functions will be called. This function receives three parameters:
    int choice (0 - circumference, 1 - area, 2 - volume)
    double radius (the radius of the shape)
    char ** message (an empty char* passed by reference to the function)

Notes:
   - circumference = 2*pi*radius
   - area = pi*radius^2
   - volume = 4.0/3.0 * pi * radius^ 3

*/

// I wonder why we do not use M_PI that's in math.h...
#define OBTUSE_TEST_PI 3.14

// defining a simple function to round to two digits; used in each function.
double rnd2(double val)
{
	return round(val * 100) / 100.0;
}

double circumference(double radius, char **message)
{
	// assign the message...  Assumes the caller doesn't attempt to change since it's a literal
	*message = "The circumference of a circle";
	// do the math as per notes
	return rnd2(2 * OBTUSE_TEST_PI * radius);
}

double area(double radius, char **message)
{
	// assign the message...  Assumes the caller doesn't attempt to change since it's a literal
	*message = "The area of a circle";
	// do the math as per notes
	return rnd2(OBTUSE_TEST_PI * radius * radius);
}

double volume(double radius, char **message)
{
	// assign the message...  Assumes the caller doesn't attempt to change since it's a literal
	*message = "The volume of a sphere";
	// do the math as per notes
	return rnd2(OBTUSE_TEST_PI * 4.0 / 3.0 * radius * radius * radius);
}

double functionChoice(int choice, double radius, char **message)
{
	// declare and assign function pointer array, matching specs of functions above
	double (*fn_ptr[3])(double, char **) = {circumference, area, volume};
	// Return value based on chosen function 
	return fn_ptr[choice](radius, message);

	// uh oh... did it first this way but missed the phrase "Use an array of function pointers"

	// // declare the fn pointer to return
	// double (*fn_ptr)(double, char **);
	// // assign fn_ptr to point to function of choice
	// switch (choice)
	// {
	// 	case 0:
	// 	{
	// 		fn_ptr = circumference;
	// 		break;
	// 	}
	// 	case 1:
	// 	{
	// 		fn_ptr = area;
	// 		break;
	// 	}
	// 	case 2:
	// 	{
	// 		fn_ptr = volume;
	// 		break;
	// 	}
	// 	default:
	// 	{
	// 		// Error handling of bad values for choice would go here.
	// 	}
	// }
	// return fn_ptr(radius, message);
}