#include <stdio.h>
#include <string.h>
#include "TestCode.h"
#include <stdlib.h>

typedef struct numNode Node;
// Refer to README.md for the problem instructions

int push(struct numNode **top, int data)
{
    Node* tmp;
    if (top == NULL) return 1;
    tmp = (Node*) malloc (sizeof(Node));
    if (tmp == NULL) return 1;
    tmp->num = data;
    tmp->next = NULL;
    if (*top == NULL) {
        *top = tmp;
    } else {
        tmp->next = *top;
        *top = tmp;
    }
    return 0;
}

int pop(Node **top)
{
    Node* tmpNodePtr;
    if (top == NULL) return 0;
    if (*top == NULL) return 0;
    tmpNodePtr = *top;
    int tmp = tmpNodePtr->num;
    *top = (*top)->next;
    free(tmpNodePtr);
    return tmp;
}

void emptyStack(Node **top)
{
    if (top == NULL) { return; }
    else {
        while (*top) {
            Node* tmp = *top;
            *top = tmp->next;
            free (tmp);
        }
    }
    return ;
}

struct numNode *createStack(int actions[], int numActions)
{
    Node *tmpStack = NULL;
    if (!actions) { return NULL; }
    for (int i = 0; i<numActions*2; i+=2) {
        switch (actions[i]) {
            case 1:
                // printf ("Popped %d\n", pop(tmpStack));
                pop(&tmpStack);
                continue;
            case 2: 
                push(&tmpStack, actions[i+1]);
                continue;
            case 3:
                emptyStack(&tmpStack);
                continue;
            default:        // something other than 1-3, so abort!
                emptyStack(&tmpStack);  
                return NULL;
        }  // End Switch
    }  // end for
    return tmpStack;
}
// Extra function for debugging 
void printStack(struct numNode **top) {
    if (!*top) { 
        printf("Emtpy Stack\n");
        return;
    }
    Node *probe = *top;
    printf("TOP -> ");
    while (probe) {
        printf("%d -> ", probe->num);
        probe = probe->next;
    }
    printf("END\n");
}

// int main (){
//     struct numNode *stack = NULL;
// //     printStack (&stack);
// //     push(&stack, 100);
// //     printStack (&stack);
// //     push(&stack, 200);
// //     printStack(&stack);
// //     printf ("Popped %d\n", pop(&stack));
// //     printStack(&stack);
// //     emptyStack(&stack);
// //     printStack(&stack);

//     int actions[] = { 2, 4, 2, 5, 2, 10 };
//     stack = createStack(actions, 3);
//     printStack(&stack);
//     int actions2[] = { 2, 4, 2, 5, 2, 10, 3, 0 };
//     stack = createStack(actions2, 4);
//     printStack(&stack);
// }