/*
Neal Schneider, IDF Instructor
2 Sep 22
Subject matter test verification
*/
#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"
// #define RUN_LOCAL 1

typedef struct numNode NumNode;

// Refer to README.md for the problem instructions

int printQueue(NumNode **front){
    NumNode *link = *front;     // make copy of the front
    int count = 0;
    while (link) {              // loop through the queue
        printf("%d->", link->num);
        link = link->next;
        count++;
    }
    printf("[end %d]\n", count);
}

int enqueue(struct numNode **end, int data)
{
    if (!end) return 1;             // check for NULL end; return err if so
    NumNode *newNode = (NumNode*) malloc(sizeof(NumNode));
    if (!newNode) return 1;         // malloc failed so return error
    if (*end) {                     // non-empty queue so add num to end
        (*end)->next = newNode;
    }
    newNode->next = NULL;           // clear the end's next ptr
    newNode->num = data;            // copy data to the new node
    *end = newNode;                 // update the end ptr
    return 0;                       // all's good so return success
}

int dequeue(struct numNode **front)
{
    if (!front || !(*front)) return 0;  // check for NULL front and empty queue
    NumNode *tmp = *front;              // make copy of front
    int data = (*front)->num;           // get data from node
    *front = (*front)->next;            // advance front to next node
    free(tmp);                          // free old node
    return data;                        // send data back
}

void emptyQueue(struct numNode **front)
{
    if (!front || !(*front)) return;    // check for NULL front and empty queue
    NumNode *link = *front, *tmp;       // get copy of front
    while (link) {                      // loop through queue
        tmp = link;                     // save current node to free
        link = link->next;              // advance link to next node
        free (tmp);                     // free node
    }
    *front = NULL;                      // reset node to NULL (empty queue)
    return;
}

struct numNode *makeQueue(int actions[], int numActions)
{
    // Check for null actions, and bad number of actions
    if (!actions || numActions < 1  ) return NULL;
    NumNode *front = NULL, *end = NULL;         // declare front and end of queue
    for (int i = 0; i < numActions*2; i+=2) {   // loop through actions
        switch (actions[i]) {                   // switch on current action
        case 1:
            dequeue(&front);
            break;
        case 2:
            if (!front) {                       // Empty Queue?
                enqueue(&front, actions[i+1]);  // so let enqueue create node
                end = front;                    // and assign end to to the new node
            } else {                            // non-empty, so add to end
                enqueue(&end, actions[i+1]);    
            }
            break;
        case 3:
            emptyQueue(&front);
            break;
        default:
            return NULL; // Action not valid (1,2,3)
            break;
        }
    }
    return front;
}

#ifdef RUN_LOCAL
int main (){
    int actions[] = {2, 4, 2, 5, 2, 10};
    struct numNode *head = makeQueue(actions, 3);
    struct numNode *res = head;
    printQueue(&head);
}
#endif