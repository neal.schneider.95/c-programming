/* 	Neal Schneider, IDF Instructor
	2 Sep 22
	Subject matter test verification
	C Advanced - Performance B - c_linked_list problem.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "TestCode.h"

/*
Task #1

Write the function processNames that receives a 10-element array of char *. The
array contains a list of peoples' first names. The processNames function should
create a linked list using the nameNode struct defined in Testcode.h.
The processNames function should iterate the array and insert the names into nodes
so they are in ascending (alphabetical) order in the linked list.

The processNames function should return a pointer to the head node of the link
list once processed.

Task #2

Write the function freeMemory that receives a pointer to your linked-list's head
node so you can iterate over the link list and free all allocated memory.
*/

#define NUM_NAMES 10
#define MAX_NAME_LEN 10
// #define RUN_LOCAL 1

typedef struct nameNode NameNode;

NameNode *processNames(const char ** names)
{
	NameNode *head = NULL, *curr, *prev, *temp;
	if (!names) return NULL;				// Check for NULL array
	for (int i = 0; i < NUM_NAMES; i++) { 	// Check for NULL strings before processing
		if (!names[i]) return NULL;  
	}

	for (int i = 0; i < NUM_NAMES; i++) {	 // loop through number of names
		if (strlen(names[i]) < 1) continue;  // Skip empty strings

		// Find correct alphabetical location in list... kinda like an insertionSort
		curr = prev = head;
		while (curr != NULL && strcmp(curr->name, names[i]) < 0) {
			prev = curr;
			curr = curr->next;
		}
		temp = (NameNode*)malloc(sizeof(NameNode));
		if (curr == head) {  	// if at beginning of list, make it the head.
			head = temp;
		} else {
			prev->next = temp;  // else insert at current location
		}
		temp->name = (char*)malloc(MAX_NAME_LEN + 1);   // allocate space for name
		strncpy(temp->name, names[i], MAX_NAME_LEN);    // copy name to the node
		temp->next = curr; 		// point new node's next to next in list
	}
	return head; 				// return the current/updated head
}

void freeMemory(NameNode *head)
{
	// temp pointer to keep the head's location after advancing the head so it can be freed
	NameNode *temp = head;	

	while (temp != NULL) {  		// loop through the linked list
		head = temp->next;			// advance head to next node
        free(temp->name);			// free the memory allocated to the name
		free(temp);					// free the node itself
		temp = head;				// advance temp to next node too
	}
}

int printList(NameNode *head) {
	NameNode *link = head;
	int count = 0;
	if (link) {
		while (link) {
			printf("%s->", link->name);
			link = link->next;
			count++;
		}
		printf ("[end (%d)]\n", count);
	} else {
		printf("[EmptyList]\n");
		return 0;
	}
}


#ifdef RUN_LOCAL
int main (){
	const char *names[] = {"Joe", "Ace", "Gene", "Paul", "Peter", "Hank", "Timmy", "Sarah", "Alice", "Abcedefghijklmnop"};
	NameNode *res = NULL;
	printList(res); 
	res = processNames(names);
	printList (res);
	freeMemory(res);
	res = NULL; 
}
#endif