#include <string.h>

int main() {
    char *a = "abc";
    char b[] = "abd";
    printf("a = %s\n", a);
    printf("b = %s\n", b);
    printf("strcmp (a, a) == %d\n", strcmp(a,a));
    printf("strcmp (a, b) == %d\n", strcmp(a,b));
    printf("strcmp (b, a) == %d\n", strcmp(b,a));

  return 0;
}