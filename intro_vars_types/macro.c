#include <stdio.h>

#define DEBUG_INT(x) \
printf(#x " is %d", x)

int main () {
    int getNum = 0;
    scanf("%d", &getNum);
    DEBUG_INT (getNum);
    // printf("getNum is %d", getNum);
    return 0;
}