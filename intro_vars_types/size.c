#include <stdio.h>

int main () {
    printf("Size of int    %d\n",sizeof(int));
    printf("Size of char   %d\n",sizeof(char));
    printf("Size of float  %d\n",sizeof(float));
    printf("Size of double %d\n",sizeof(double));
    // printf("Size of void   %d",sizeof(void));  // not valid to get sizeof(void)

}