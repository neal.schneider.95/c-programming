#pragma once

#include <stdio.h>

typedef struct numNode
{
    int num;
    struct numNode *next;
}numNode;


#ifdef __cplusplus
extern "C" {
#endif
    int push(struct numNode **top, int data);
    int pop(struct numNode **top);
    void emptyStack(struct numNode **top);
    struct numNode *createStack(int [], int);
#ifdef __cplusplus
}
#endif
