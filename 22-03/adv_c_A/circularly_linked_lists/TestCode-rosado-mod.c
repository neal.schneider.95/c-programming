#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions
int calls = 0;
struct numNode *head;

void prepend(int data)
{

    struct numNode *newNode = (struct numNode *)malloc(sizeof(struct numNode));
    struct numNode *temp;

    newNode->num = data;
    if (head == NULL)
    {
        head = newNode;
        newNode->next = head;
    }
    else
    {
        temp = head;
        while (temp->next != head)
        {
            temp = temp->next;
        }
        newNode->next = head;
        temp->next = newNode;
        head = newNode;
    }
}

struct numNode *buildCList(int *nums, int size)
{
    if (size <= 0 || !nums) // ns: added check for NULL nums
    {
        return NULL;
    }

    // looking for duplicates

    int duplicates[20] = {0};

    for (size_t i = 0; i < size; i++)
    {
        prepend(nums[i]);
    }

    return head;
}

int emptyList(struct numNode *head)
{
    struct numNode *tmp = head, *tmp2 = head;
    int counter = 0;
    if (!head) return 0;  //ns: check for NULL numNode

    while (tmp->next != head)
    {
        tmp2 = tmp;
        tmp = tmp->next;
        // tmp2 = NULL;  //ns: need to free the node, not NULL
        free(tmp2);
        counter++;
    }
    // head = NULL;  //ns: need to free the head, not NULL
    // free(head);
    free (tmp);
    counter++;

    return counter;
}

// void printList(struct numNode *head)
// {
//     int count = 0;
//     if (!head)
//     {
//         printf("Empty List\n");
//         return;
//     }
//     struct numNode *link = head;
//     do
//     {
//         printf("%d -> \n", link->num);
//         link = link->next;
//         count += 1;
//         // sleep(.05);
//     } while (link != head);
//     printf("[end (%d)]\n", count);
// }

// void main()
// {
//     int nums[] = {80, 80, 70, 60, 50};
//     int num2[] = {90, 80, 70, 60, 50};

//     struct numNode *a = NULL;
//     struct numNode *t = (struct numNode *)malloc(sizeof(struct numNode));

//     a = buildCList(NULL, 1);
//     printList(a);
//     a = buildCList(nums, 1);
//     printList(a);
//     printf("emptying list of 1/n");
//     emptyList(a);
//     printList(a);
//     a = buildCList(nums, 5);
//     printList(a);

//     a = buildCList(num2, 0);
//     printList(a);
//     a = buildCList(num2, 5);
//     printList(a);

//     int nums4[] = {5, 7, 8, 7, 9, 2, 34, 5, 54, 1};
//     int nums3[] = {5, 7, 8, 9, 2, 34, 54, 1};

    
//     a = buildCList(nums3, sizeof(nums3) / sizeof(*nums3));
//     printf("nums3\n");
//     printList(a);
//     printf("Emptying list\n");
//     emptyList(a);
//     printList(a);

//     a = buildCList(nums4, sizeof(nums4) / sizeof(*nums4));
//     printf("nums4\n");
//     printList(a);
//     printf("Emptying list\n");
//     emptyList(a);
//     printList(a);

//     printf("NULL Case:\n");
//     printList(buildCList(NULL,1));
//     emptyList(NULL);
// }
