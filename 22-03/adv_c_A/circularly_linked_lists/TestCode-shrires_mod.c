#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct numNode *buildCList(int *nums, int size)
{
    //check for nums null
    if(nums == NULL){
        return NULL;
    }

    //check for valid size
    else if(size < 1){
        return NULL;
    }

    int count = 0;
    //track head and tail
    struct numNode *head = NULL;  //ns added null assignment
    struct numNode *tail = NULL;  //ns added null assignment
    //created a new array to store used numbers 
    int mynums[size];

    while(count < size){
        //iterating through my array to see if we have 
        //used the number before
        int matched = 0;
        for(int i = 0; i < count; i++){
            if(nums[count]==mynums[i]){
                matched = 1;
            }
        }
        if (matched) {
            count++;
            continue;
        }
        //creating a new node pointer
        struct numNode *newNode= malloc(sizeof(struct numNode));
        //if head is null set new node equal to head 
        if (head == NULL){
            //sets data 
            newNode->num = nums[count];
            head= newNode;
            head->next = newNode; //ns added
            tail= newNode;
            //stores used number
            mynums[count] = nums[count];
        }
        else{
            //creates a node at the head of the list 
            newNode->num = nums[count];
            newNode->next = head;
            head = newNode;
            tail->next = head;
            //stores used number
            mynums[count] = nums[count];
        }
        count++;
    }
    
    //returns pointer to the head variable
    return head;
}
//empty the list and free the pointers
int emptyList(struct numNode *head)
{
    //track nodes removed
    int numremoved = 0;
    //return 0 if head is null
    if(head==NULL){
        return 0;
    }
    struct numNode *newNode;
    struct numNode *temp;
    newNode = head->next;
    //traverse the nodes and null them out 
    // while(newNode!=head){
    //     temp = newNode;
    //     temp=NULL;
    //     newNode = newNode->next;
    //     numremoved++;
    // }

    while(newNode!=head){
        temp = newNode;
        newNode = newNode->next;
        free(temp);
        numremoved++;
    }

    head = NULL;
    numremoved++;
    // free(temp);
    // free(newNode);
    free(head);
    return numremoved;
}

// //testing my own stuff
// int main(){
//     struct numNode *head;
//     struct numNode *probe;
//     int nums2[] = { 90,80,70,60,50};
//     head = buildCList(nums2, 6);
//     probe = head;
//     while(probe->next!=head){
//         printf("number = %d\n", probe->num);
//         probe = probe->next;
//     }
//     int x = 0;
//     x = emptyList(head);
//     printf("%d", x);
// }

// void printList(struct numNode *head)
// {
//     int count = 0;
//     if (!head)
//     {
//         printf("Empty List\n");
//         return;
//     }
//     struct numNode *link = head;
//     do
//     {
//         printf("%d -> ", link->num);
//         link = link->next;
//         count += 1;
//         // sleep(.05);
//     } while (link != head);
//     printf("[end (%d)]\n", count);
// }

// void main()
// {
//     int nums[] = {80, 80, 70, 60, 50};
//     int num2[] = {90, 80, 70, 60, 50};

//     struct numNode *a = NULL;
//     struct numNode *t = (struct numNode *)malloc(sizeof(struct numNode));

//     a = buildCList(NULL, 1);
//     printList(a);
//     a = buildCList(nums, 2);
//     printList(a);
//     printf("emptying list of 1/n");
//     emptyList(a);
//     // printList(a);
//     a = buildCList(nums, 5);
//     printList(a);

//     a = buildCList(num2, 0);
//     printList(a);
//     a = buildCList(num2, 5);
//     printList(a);

//     int nums4[] = {5, 7, 8, 7, 9, 2, 34, 5, 54, 1};
//     int nums3[] = {5, 7, 8, 9, 2, 34, 54, 1};

    
//     a = buildCList(nums3, sizeof(nums3) / sizeof(*nums3));
//     printf("nums3\n");
//     printList(a);
//     printf("Emptying list\n");
//     emptyList(a);
//     printList(a);

//     a = buildCList(nums4, sizeof(nums4) / sizeof(*nums4));
//     printf("nums4\n");
//     printList(a);
//     printf("Emptying list\n");
//     emptyList(a);
//     printList(a);

//     printf("NULL Case:\n");
//     printList(buildCList(NULL,1));
//     emptyList(NULL);
// }
