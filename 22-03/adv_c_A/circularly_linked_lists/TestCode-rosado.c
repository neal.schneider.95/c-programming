#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct numNode *head;

void prepend(int data)
{

    struct numNode *newNode = (struct numNode *)malloc(sizeof(struct numNode));
    struct numNode *temp;

    newNode->num = data;
    if (head == NULL)
    {
        head = newNode;
        newNode->next = head;
    }
    else
    {
        temp = head;
        while (temp->next != head)
        {
            temp = temp->next;
        }
        newNode->next = head;
        temp->next = newNode;
        head = newNode;
    }
}

struct numNode *buildCList(int *nums, int size)
{
    if (size <= 0)
    {
        return NULL;
    }

    // looking for duplicates

    int duplicates[20] = {0};



    for (size_t i = 0; i < size; i++)
    {
        prepend(nums[i]);
    }

    return head;
}

int emptyList(struct numNode *head)
{
    struct numNode *tmp = head;
    int counter = 0;

    while (tmp->next != head)
    {
        struct numNode *tmp2 = tmp;
        tmp = tmp->next;
        tmp2 = NULL;
        counter++;
        
        
    }
    head = NULL;
    free(head);
    counter++;

    return counter;
}
