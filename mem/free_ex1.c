#include <stdio.h>
#include <stdlib.h>

void main() {
    char *c_ptr = malloc(sizeof *c_ptr);   // Allocates memory for 1 char
    if (c_ptr == NULL) {                   // Check for failure
        fprintf(stderr, "malloc failed.\n");        
        exit(EXIT_FAILURE);
    }
    *c_ptr = 'Z';                          // Assign a value into that memory
    printf("-%c-\n", *c_ptr);              // Print confirms value assigned

    free(c_ptr);                           // Deallocate memory at c_ptr

    // but it doesn't stop the below because c_ptr is still an address
    printf("-%c-\n", *c_ptr);              // Behavior is undefined...
    *c_ptr = 65;                           // You CAN still put things there    
    printf("-%c-\n", *c_ptr);              // but you definitely SHOULD NOT!
}