#include <stdio.h>
#include <stdlib.h>

void main() {
    long *array_ptr = calloc(10, sizeof *array_ptr); // Allocate/zeroize 10 longs
    if (array_ptr == NULL) {                         // Check for failure
        fprintf( stderr, "calloc failed.\n" );      
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 10; i++) {                   // Confirm 0s
        printf("%ld ", *(array_ptr + i));
    }
    printf( "\n" );
    for (int i = 0; i < 10; i++) {                   // Assignment
        *(array_ptr + i) = i;
    }
    for (int i = 0; i < 10; i++) {                   // Confirm print
        printf("%ld ", *(array_ptr + i));
    }
    printf( "\n" );
}