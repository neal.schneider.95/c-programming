#include <stdio.h>
#include <stdlib.h>  // Needed for malloc, calloc, realloc, and free

int main () {
    int *int_ptr = NULL;
    
    int_ptr = (int*) malloc(sizeof(int_ptr));
    if (int_ptr == NULL)
    {
        printf("Something went horribly wrong.... Couldn't allocate memory!\n");
        return 1;
    }
    *int_ptr = 999;
    printf("value of new variable is : %d\n", *int_ptr);
    printf("It's address is : %p\n", int_ptr);

    free(int_ptr);
    int_ptr = NULL;

    printf("It's address is : %p\n", int_ptr);
}