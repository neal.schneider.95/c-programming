// TODO: Include any needed headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int haystack_needle(char *haystack, char *needle, char *buffer);
int main()
{
    char string[] = "This is my brand new sentence";
    char substring[] = " brand new";
    // TODO: Create a char buffer named *buffer using malloc \
    ensure it is large enough to hold string[] + a nul-terminator
    char *buffer = (char*)malloc(strlen(string)+1);

    // TODO: Ensure buffer was created
    if (!buffer) {
        printf("CROAK!\n");
        return 1;
    }

    int ret = haystack_needle(string, substring, buffer);
    printf("returned %d\n", ret);
    // TODO: Give back the memory from buffer
    // TODO: Print out the modified string
    printf ("str: %s\n", string);
    printf ("sub: %s\n", substring);
    printf ("buf: %s\n", buffer);

    free(buffer);
    return 0;

}

/*
Arguments:
-haystack --- pointer to string to search
-needle   --- pointer to substring to search inside of haystack
-buffer   --- pointer to a buffer

PURPOSE:
haystack_needle takes the arguments above and searches the haystack
for the needle. Once it finds the needle... it removes it from the haystack.

Return:
return 0 on success
return -1 on error .... only needed if userinput is enabled.
*/
int haystack_needle(char *haystack, char *needle, char *buffer)

{
    int needleLength = strlen(needle);
    /*
    TODO: Using strstr() find the needle in the haystack (substring in string)
    the pointer that is returned is pointing to the substring within the string 
    Set that pointer to a new char pointer called *mark_position
    */
    strcpy(buffer, haystack);

    char *marker = strstr(buffer, needle);
    if (!marker) {
        return -1;
    }
    /*
    TODO: Using strcpy(), store the needle into the buffer...
    you are not authorized to use the variable needle.
    */

    /*
    TODO: Using memmove, overwrite the haystack (string)... effectively removing the needle 
    HINT: start at the mark_position... that is after all where we need to start replacing.
    */
    memmove(marker, marker+needleLength, strlen(buffer)-(marker-buffer)-needleLength+1);
    return 0;
}
