#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char pangram[] =  "How vexingly quick daft zebras jump!";

int rec_fn(int val) {
    int loc = 0x1abcde00 + val;
    int *loc_ptr = &loc; 
    int *val_ptr = &val;
    int *heap_ptr;
    heap_ptr = malloc (sizeof(int));
    *heap_ptr = loc;
    char *heap_str;
    heap_str = (char*) malloc (strlen(pangram)+1);
    strcpy(heap_str, pangram);

    printf("%3d: %p\t%p\t%p\n", val, loc_ptr, val_ptr, heap_ptr);
    if (val > 0) {
        rec_fn(--val);
    } else {
        free(heap_ptr);
        free(heap_str);
        return 0;
    }
    free (heap_ptr);
    free (heap_str);
    return 1;
}

int main () {
    char *pangram_ptr;
    pangram_ptr = pangram;
    printf("depth: \t loc_ptr \t val_ptr \t heap_ptr \n");
    rec_fn(200);
    printf("done\n");
}