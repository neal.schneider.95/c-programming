#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // Needed for malloc, calloc, realloc, and free
#define BUFF_SIZE 256

int main () {
    char message[] = "Hello heap space...";
    char *str_ptr = NULL;
    
    str_ptr = (char*) calloc(BUFF_SIZE, sizeof(char));
    if (str_ptr == NULL)
    {
        printf("Something went horribly wrong.... Couldn't allocate memory!\n");
        return 1;
    }
    strcpy(str_ptr, message);
    printf("value of new variable is : %s\n", str_ptr);
    printf("It's address is : %p\n", str_ptr);

    free(str_ptr);
    str_ptr = NULL;

    printf("It's address is : %p\n", str_ptr);
}