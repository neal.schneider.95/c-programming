//TODO: Include needed headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void)
{
    //TODO: Create a string containing your first name
    char name[] = "MyPrecious";

    //TODO: Get the size of this string
    int name_size = strlen(name);

    //TODO: Declare a char pointer *str
    char *new_str;

    //TODO: Allocate a section of memory of type char of 40 bytes, and...
    //TODO: Assign the address of this allocated space to the pointer value
    new_str = (char*)malloc(40);

    //TODO: Copy your name into the allocated space using strcpy()
    strcpy (new_str, name);

    //TODO: Print out your name that is stored in the allocated memory space
    printf("My name is %s. \n", new_str);

    //TODO: Reallocate the memory space using the size of the string rather than 40 bytes
    new_str = realloc(new_str, name_size+1);

    //TODO: Print out the string again
    printf("My name is %s. \n", new_str);
}