// TODO: Include any needed headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int haystack_needle(char *haystack, char *needle, char *buffer);
int main()
{
    char string[] = "This is my brand new sentence";
    char substring[] = " brand new";
    // TODO: Create a char buffer named *buffer using malloc \
    ensure it is large enough to hold string[] + a nul-terminator
    char *buffer = NULL;

    buffer = (char*) malloc(30);

    if (buffer==NULL){
        printf("ERROR");
    }


    // TODO: Ensure buffer was created
    haystack_needle(string, substring, buffer);
     printf("buffer: %s\n", buffer);

    // TODO: Give back the memory from buffer
    free(buffer);
    // TODO: Print out the modified string

   

    return 0;

}

/*
Arguments:
-haystack --- pointer to string to search
-needle   --- pointer to substring to search inside of haystack
-buffer   --- pointer to a buffer

PURPOSE:
haystack_needle takes the arguments above and searches the haystack
for the needle. Once it finds the needle... it removes it from the haystack.

Return:
return 0 on success
return -1 on error .... only needed if userinput is enabled.
*/
int haystack_needle(char *haystack, char *needle, char *buffer)

{
    int needleLength = strlen(needle);
    char *mark_position = NULL;
    mark_position = (char*) malloc(needleLength);



    /*
    TODO: Using strstr() find the needle in the haystack (substring in string)
    the pointer that is returned is pointing to the substring within the string 
    Set that pointer to a new char pointer called *mark_position
    */
   mark_position = strstr(haystack, needle);

    /*
    TODO: Using strcpy(), store the needle into the buffer...
    you are not authorized to use the variable needle.
    */
   strcpy(buffer, mark_position);
   memmove(buffer, mark_position+ strlen(mark_position), needleLength);

    /*
    TODO: Using memmove, overwrite the haystack (string)... effectivly removing the needle 
    HINT: start at the mark_position... that is after all where we need to start replacing.
    */
   printf("%s\n", haystack);
   memmove(haystack, " ", needleLength);
   printf("%s\n", haystack);
   free(mark_position);

    return 0;
}
