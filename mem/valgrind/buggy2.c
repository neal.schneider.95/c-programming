#include <stdio.h>
#include <stdlib.h> 

long unsigned blockSize = 0;
char *buffer;

int main (){
    printf ("int is %lu bytes\n", (sizeof(int)));
    printf ("u   is %lu bytes\n", (sizeof(unsigned)));
    printf ("lu  is %lu bytes\n", (sizeof(long unsigned)));
    printf ("llu is %lu bytes\n", (sizeof(long long unsigned)));
    while (1) {
        blockSize ++;
        printf("allocating block %lu, (%lx bytes)... ", blockSize, 
            ((long unsigned)1)<<blockSize);
        buffer = (char*)malloc(((long unsigned)1)<<blockSize);
        if (!buffer) {
            printf("Croak!  Couldn't allocate!\n");
            break;
        }
        printf("at %p\n", &buffer);
    }
}