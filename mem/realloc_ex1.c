#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void main() {

int *i_ptr = malloc(sizeof *i_ptr);       // Allocate memory for 1 int
if (i_ptr == NULL) {                      // Check for failure
    fprintf(stderr, "malloc failed.\n");      
    exit(EXIT_FAILURE);
}
*i_ptr = 2001;                            // Assign value into that memory
printf("%d\n", *i_ptr);                   // Print confirms value assigned

int *new_i_ptr = realloc(i_ptr, 2 * sizeof *i_ptr);// Realocate for 2 ints
if (new_i_ptr == NULL) {                           // Check for failure
    fprintf( stderr, "realloc failed.\n");
    exit(EXIT_FAILURE);
}
*(new_i_ptr + 1) = 42;                    // Assign value to new memory
for (int i = 0; i < 2; i++){              // Confirmation print
    printf("%d\n", new_i_ptr[i]);
}
free(new_i_ptr);             // you do not have to free the original i_ptr
                  
}