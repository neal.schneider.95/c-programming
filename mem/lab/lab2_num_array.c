#include <stdio.h>
#include <stdlib.h>

int main () {
    int stop;
    int powers;
    u_int64_t *array;           // int is acceptable. used u_int64_t to get biggest possible nums, though
    int val;

    printf("Enter stop and number of powers? ");
    scanf("%d %d",  &stop, &powers);
    if (stop >= 1 && powers > 0) {                              //  Check range
        // Allocate  & zeroize array size:  size is inclusive of stop, so add one * number of powers
        array = calloc((stop + 1) * powers, sizeof(int));
        for (int row = 1; row <= stop; row++) {                 // Iterate rows up to stop
            for (int col = 0; col < powers; col++) {            // Iterate column up to powers
                val = row;                                      // Start row with row ** 1 (== row)
                for (int p = 0; p < col; p++) {
                    val = val * row;                            // Multiply by row to increment power
                }
                *(array + col + row * powers) = val;            // Populate Array with values with pointer math
            }
        }
        for (int p = 0; p < powers; p++) {                      // Print header 
            printf("n^%d    ", p + 1);
        }
        for (int row = 1; row <= stop; row++) {                 // Iterate over rows
            printf("\n%ld: ", *(array + row * powers));         // Print 1st column
            for (int col = 1; col < powers; col++) {            // iterate over powers
                printf("%6ld ", *(array + col + row * powers)); // print value from array
            }
        }
        printf("\n");
        free(array);                                            // <<--- Very important!  Free the array!!
    } else {
        // Print notice if out of range
        printf("Stop and powers must be greater than 0.\n");
    }
}