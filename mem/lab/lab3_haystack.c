#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Define function as required
char *haystack_needle(char *haystack, char *needle) {
    char *tmp = strstr(haystack, needle);                   // Use strstr to find the needle
    if (!tmp) {                                             // If no found,
        return NULL;                                        // Return NULL and don't modify
    }
    int skip = strlen(needle);                              // Number of chars to skip
    // Loop over remainder of string, overwriting needle
    for (int i = 0; i <= strlen(tmp) - skip; i++) {         // Use <= to include null-terminator
        tmp[i] = tmp[i + skip];
    }
    haystack = realloc(haystack, strlen(haystack) - skip);  // Update haystack and release unused mem
    return haystack;
}

int main () {
    char *phrase = malloc(101);
    char *needle = malloc(51);
    char *new_phrase;

    if (!phrase || !needle){                          // Check for failed malloc
        printf("Error allocating memory.\n");
    } else {

        printf("Enter a phrase: ");                             // Input phrase
        fgets(phrase, 101, stdin);
        if (phrase[strlen(phrase) - 1] == '\n') {
            phrase[strlen(phrase) - 1] = 0;
        }
        printf("Enter a substring to remove: ");                // Input needle
        fgets(needle, 51, stdin);
        if (needle[strlen(needle) - 1] == '\n') {
            needle[strlen(needle) - 1] = 0;
        }

        // Use fn to modify phrase
        new_phrase = haystack_needle(phrase, needle);

        if (new_phrase) {                                       // If no err...
            printf("Modified Phrase: %s\n", phrase);            // Print new phrase
            free(new_phrase);                                   // Dealloc phrase
        } else {                                                // If err...
            printf("\"%s\" was not found.\n", needle);          // Print notification
            free(phrase);                                       // Dealloc phrase
        }
        free(needle);                                           // Free unmodified needle
    }
}