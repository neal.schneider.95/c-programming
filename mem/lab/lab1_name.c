#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main () {
    char *name;
    
    name = malloc(21);                          // Allocate space for 20 chars + null
    if (name == NULL){                          // Check for failed malloc
        printf("Error allocating memory.\n");
    } else {
        int len;

        printf("What is your name? ");          // Get name
        fgets(name, 21, stdin);                 // Using fgets for safer input

        if (name[strlen(name) - 1] == '\n') {   // overwrite newline char if there
            name[strlen(name) - 1] = 0;
        }
        printf("Welcome, %s\n", name);          // Print string
        free(name);                             // <<--- Critical! Free memory!
    }
}