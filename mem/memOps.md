# Memory Management notes

MD book: https://39ios-idf.staging.90cos.cdl.af.mil/4_c_module/12_memory_management/index.html

Slides: https://39ios-idf.staging.90cos.cdl.af.mil/4_c_module/slides/12_memory_management.html#/

Geeks for geeks has a good article on the overall memory layout of a C program. https://www.geeksforgeeks.org/memory-layout-of-c-program/

# Valgrind: dynamic instrumentation toolset.  

It can automatically detect many memory management and threading bugs, and profile your programs in detail.