#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main() {

    int *array_ptr = malloc(10 * sizeof(*array_ptr));    // Allocate 10 ints
    if (array_ptr == NULL) {                            // Check for failure
        fprintf(stderr, "malloc failed.\n");      
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 10; i++) {                   // Assignment
        array_ptr[i] = i;
    }
    for (int i = 0; i < 10; i++) {                      // Confirmation print
        printf("%d ", array_ptr[i]);
    }
    printf("\n");
}