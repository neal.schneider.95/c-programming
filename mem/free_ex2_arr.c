#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main() {
    // Allocate/zeroize over a million long ints
    long *array_ptr = calloc(1 << 20, sizeof *array_ptr);
    if (array_ptr == NULL) {                         // Check for failure
        fprintf(stderr, "calloc failed.\n");        
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 1 << 20; i++) {
        array_ptr[i] = i;                            // Populate the array
    }
    for (int i = 10; i < 20; i++) {
        printf("%ld \n", array_ptr[1 << i]);         // Confirm assignment
    }
    free(array_ptr);  // That's it, you don't have to free each item,
                      // only the pointer returned by the allocation function
    void *x = array_ptr * 2;
       
}