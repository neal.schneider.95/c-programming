#include <stdio.h>

int MAXSIZE = 8;       
char stack[8];     
int top = -1;            

int isempty() {

   if(top == -1)
      return 1;
   else
      return 0;
}
   
int isfull() {

   if(top == MAXSIZE)
      return 1;
   else
      return 0;
}

char peek() {
   return stack[top];
}

char pop() {
   int data;
    
   if(!isempty()) {
      data = stack[top];
      top = top - 1;   
      return data;
   } else {
      printf("Could not retrieve data, Stack is empty.\n");
   }
}

int push(int data) {

   if(!isfull()) {
      top = top + 1;   
      stack[top] = data;
   } else {
      printf("Could not insert data, Stack is full.\n");
   }
}

int main() {
   char word[50];
   int i = 0;
   char c;
   int ispalindrome = 1;
   // get the sentence
   printf ("Palindrome check... Enter a word: ");
   scanf("%[^\n]s", word); 

   // parse and push on stack
   while (word[i] != 0) {
      push (word[i]);
      i++;
   }

   // print stack data 
   // while(!isempty()) {
   //    char c = pop();
   //    printf("%c",c);
   // }
   i = 0;
   while (word[i]!=0 && ispalindrome) {
      c = pop();
      if (word[i] != c ) {
         ispalindrome = 0;
      }
      i++;
      printf("%c",c);
   }
   printf("\n%s\n", ispalindrome ? "Is a Palindrome" : "Is not a palindrome");
}
