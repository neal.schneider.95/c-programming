#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 64

#define VICTORY()\
printf( "The world is at peace...");\
return(0);\

int main () {
    char s[BUFFER_SIZE];
    printf("All's good if you give me tacos...");
    scanf("%s", &s); 
    
    if (strcmp(s, "tacos") == 0) {
        VICTORY ();
    }
    else {
        printf("I didn't get tacos...");
    }
}
