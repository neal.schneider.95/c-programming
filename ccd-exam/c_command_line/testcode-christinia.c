#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


int format_file(char format, int hasfile, char *filename, int allowchar);
int fileDump(const char *fname, const char *words[], int wordLen);


int main(int argc, char *argv[])
{

    char format = ' ';
    char *file_name =  NULL;
    int allowchar = 0;

    if(argc > 5){
        //too many options
        printf("Too many options\n");
        return EXIT_FAILURE;
    }
 
    //loop through argv and look for options with '-'
    //when found look at second index for option


    for(int index = 0; index <= argc, index++;){
        printf("This is index: %d\n", index);
        printf("%s\n", argv[index]);


        if(argv[index][0]  != '-'){
        printf("Options are wrong\n");
        return EXIT_FAILURE;
        }


        switch(tolower(argv[index][1])){
            case 'p':
                format = 'p';
                break;
            case 'd':
               format = 'd';
               break;
            case 'n':
                format = 'n';
                break;
            case 'a':
                allowchar = 1;
                break;
            case 'f':
                file_name = malloc(strlen(argv[index + 1]));
                strcpy(file_name, argv[index + 1]);
                printf("filename: %s", file_name);
                index++;
                break;
            default:
                printf("%s is not supported\n.", argv[index]);
                return EXIT_FAILURE;
        }
    }


     if(file_name == NULL){
        file_name = malloc(strlen("output.txt"));
        strcpy(file_name, "output.txt" + 1);
     }
    // char format = "p";
    // int hasfile = 0;
    // char *file_name =  calloc(strlen("output.txt") + 1, sizeof *file_name);
    // int allowchar = 0;
    printf("format: %c\n", format);
    printf("filename :%s\n", file_name);
    printf("allowchar: %d\n", allowchar);




}




int fileDump(const char *fname, const char *words[], int wordLen) {


    if (!fname || !words) {
        return 0;
    }


    FILE *out = fopen(fname, "w");
    if (!out) {
        return 0;
    }
    for (int i = 0; i < wordLen; i++) {
        if (words[i]) {
            // int status = fputs(words[i], out);
            // if (status == EOF) {
            //     return 0;
            // }
            // status = fputc('\n', out);
            // if (status == EOF) {
            //     return 0;
            // }


            int status = fprintf(out,"%s\n", words[i]);
            if(status < 0){
                return 0;
            }
        }
    }
    fclose(out);
    return 1;
}
