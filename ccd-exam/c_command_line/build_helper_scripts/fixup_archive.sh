#!/bin/bash

#This script is used internally by the build process. It is not designed to be invoked by the examinee. It renames the
#main function so that we can call it in the test code.
#This script includes LOTS of error checking so if it fails in the pipeline or for an examinee we can see what's going
#on quickly. The other reason for the thorough error checking is that the examinee would get a very confusing message
#about two main symbols if we didn't detect that the symbol rename had failed. This could lead them to believe the
#problem was in their code rather than a failure in this script.

echo $(tput setaf 4)---Starting $0 script for library $1---
LIB_NAME=$1   #This is the name of the library where we will find the main() to be renamed
USER_OBJ_FILE=TestCode.c.o

# Test to see if library exists
if [ ! -f ${LIB_NAME} ]; then
    echo "$(tput setaf 1)${LIB_NAME} not found!"
    exit 1
fi

# Blow away any existing copy of the USER_OBJ_FILE. We want to get the one from out of the library file.
rm -f ${USER_OBJ_FILE}

#
# Step 1. Rename the symbol
#
# Approach:  extract the obj file from LIB_NAME, rename main to libmain use objcopy, and insert the modified obj file
#back into lib
echo "1. Rename main to libmain stage"

# Extract the .o file
ar x ${LIB_NAME}  #Extract .o file

# Test to see if extraction worked
RETVAL=$?
if [ ${RETVAL} -ne 0 ]; then
    echo "$(tput setaf 1)ar extraction failed with error code ${RETVAL}"
    exit 1
fi
if [ ! -f ${USER_OBJ_FILE} ]; then
    echo "$(tput setaf 1)${USER_OBJ_FILE} not found when extracting from library!"
    exit 1
fi

#Test to see if the symbol main exists (this is an important step to give examinees a usable error if they rename main,
#because the error message the linker would give if we let it notice the error is likely to confuse examinees)
if ! nm ${USER_OBJ_FILE} | grep main; then
    echo "$(tput setaf 1)Symbol main not found!" #Ordinarily gcc would give the error "undefined reference to `main'"
    exit 1
fi

#Rename the symbol
objcopy -v --redefine-sym main=libmain ${USER_OBJ_FILE}  #Rename symbol
RETVAL=$?
if [ ${RETVAL} -ne 0 ]; then
    echo "$(tput setaf 1)objcopy --redefine-sym failed with error code ${RETVAL}"
    exit 1
fi
nm ${USER_OBJ_FILE} | grep libmain #Verify renaming visually when connected to stdout -- this helps proctors see success
#Test to see if renamed symbol is in object file. Necessary since objcopy returns
#success even if the symbol rename failed (e.g., wrong symbol name).
if ! nm ${USER_OBJ_FILE} | grep libmain; then
    echo "$(tput setaf 1)Symbol rename failed!"
    exit 1
fi

#Replace the OBJ file in the archive with the updated file
ar r ${LIB_NAME} ${USER_OBJ_FILE} #replace existing .o file with new one
RETVAL=$?
if [ ${RETVAL} -ne 0 ]; then
    echo "$(tput setaf 1)ar replace failed with error code ${RETVAL}"
    exit 1
fi


#
# Step 2. Verify the renaming worked
#
# Approach:  extract the obj file from the library in a tempdir directory and check for the symbol
# We don't need quite as much error checking here.
echo "2. Verify renaming stage"
rm -rf tempdir  #A bit ugly, but this is in the build directory and it lets us leave around the results to examine
mkdir tempdir
cd tempdir
cp ../${LIB_NAME} .
ar x ${LIB_NAME} ${USER_OBJ_FILE}
RETVAL=$?
if [ ${RETVAL} -ne 0 ]; then
    echo "$(tput setaf 1)ar extract for verify failed with error code ${RETVAL}"
    exit 1
fi
#Unfortunately ar does not always return a failure code, so we need to manually check that this worked
if [ ! -f ${USER_OBJ_FILE} ]; then
    echo "$(tput setaf 1)${USER_OBJ_FILE} not found when extracting from library in verify stage!"
    exit 1
fi
nm ${USER_OBJ_FILE} | grep libmain
if ! nm ${USER_OBJ_FILE} | grep libmain; then
    echo "$(tput setaf 1)Renamed symbol not found in library!"
    exit 1
fi

# Wrap up
set +x
echo $(tput setaf 4)-------------------
echo -n $(tput sgr0)  #Reset terminal to original colors
