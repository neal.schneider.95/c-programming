# C Fundamentals: Magic Square

**Recommended Duration:** TODO: X hours   -or-   X.X hours   -or-   XX minutes (if less than 1 hour)

## Instructions

1. In your GitLab project for the "C Fundamentals Exam", create a branch and merge request named "magic_squares".  
   - Ensure that your merge request begins in a **Draft** status.
2. Checkout the "magic_squares" branch in your local repository.
3. Read the overview below, then complete the task(s) that follow.

## Overview - Magic Square

The Magic Square is a grid like the example shown below:

```text
4 9 2
3 5 7
8 1 6
```

A 3x3 Magic Square has the following properties:

1. The grid contains each of the numbers 1 through 9 exactly once.
2. The sum of each row, each column, and each diagonal all add up to the number 15.

## Task

In `TestCode.c`, implement the function `isMagicSquare` that determines whether a two-dimensional array contains a Magic Square, according to the following specification:

- **Prototype:**
  - `int isMagicSquare(int values[ROWS][COLS]);`
- **Parameters:**
  1. `values`: a two-dimensional array of int
- **Return:**  
  - `1` if the array meets all the requirements of a magic square
  - `0` otherwise

## Building and Testing

- In your terminal, run `./test_magic_square.sh` to build and run the tests on your solution.
- Be sure to check the terminal output for warnings and errors.  Errors will prevent the tests from running, but warnings may not.  Your solution should not have any warnings.

## Other Requirements

1. Your solution may only use C programming tool/techniques taught in this course; violating this requirement is an automatic failure of the exam.

## Submission Instructions

1. Use `git` to `commit` and `push` your solution to your branch in the GitLab project.
2. **Do not** `commit` or `push` any executable files or the contents of the `build` directory.
3. Add your instructor as a reviewer to the merge request for your solution.
4. Mark the merge request as **Ready**.
5. When you are complete with all the questions for this exam, notify your instructor that your solutions are ready for review.

## DIcE Rubric

TODO: assign points / percents

### Document - 10 % / points

#### Project Writeup - ? % / points  - We don't think this section is needed for performance exams.  Would make more sense in the Team Projects

- Does the writeup document challenges and successes encountered? - ? % / points
- Does the writeup document any lessons learned?  - ? % / points

#### Grammar/Spelling - ? % / points

- Is the project free of grammatical and spelling errors? - ? % / points
- Is non-code formatting consistent? - ? % / points

#### Code Formatting / Style Guide - ? % / points

- Is the code formatting consistent? - ? % / points
- Are appropriate names chosen to enable code readability? - ? % / points
- Are comments added where appropriate and aid understanding of the logic? - ? % / points
- Is any outside code cited appropriately? - ? % / points

### Implement - 20 % / points

#### Version Control - ? % / points

- Does the project / branch / merge request have the correct name(s)? - ? % / points
- Has the work been pushed properly? - ? % / points
- Were commits broken down into appropriate scopes? - ? % / points
- Are commit messages simple and informative? - ? % / points

#### Architecture - ? % / points

- Are all function calls in your solution required? (i.e. Did you avoid unnecessary work?) - ? % / points
- Are unused variables or functions avoided? - ? % / points

### Execute - 70 % / points

#### Safety - ? % / points

- Does the program avoid crashing, even on invalid input? - ? % / points

#### Builds - ? % / points

- Does the program run without any warnings or errors? - ? % / points
- Does the program pass all of the test cases? - ? % / points

#### Requirements - ? % / points

- Is all logic correct in regards to calculations and output? - ? % / points
- Are all other requirements met? - ? % / points
