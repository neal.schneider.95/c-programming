#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define NUM_USERS 20  // Number of users submitting print jobs
#define MAX_JOBS_PER_USER 3  // Maximum print jobs each user can submit

sem_t printQueueSemaphore;
sem_t printerResourceSemaphore;

void *user_function(void *user_id) {
    long userId = (long)user_id;

    for (int i = 0; i < MAX_JOBS_PER_USER; i++) {
        int sv;

        sem_getvalue(&printQueueSemaphore, &sv);
        // Simulate user preparing a print job
        printf("User %ld is preparing print job %d. (v:%d)\n", userId, i, sv);
        // Acquire the print queue semaphore to add the job
        sem_wait(&printQueueSemaphore);
        sem_getvalue(&printQueueSemaphore, &sv);

        // Add the print job to the queue
        printf("User %ld added print job %d to the queue. (v:%d)\n", userId, i, sv);

        // Release the print queue semaphore
        sem_post(&printQueueSemaphore);

        // Acquire the printer resource semaphore to print
        sem_wait(&printerResourceSemaphore);

        // Simulate printing
        printf("Printer is printing job %d for User %ld.\n", i, userId);
        sleep(1);  // Simulate printing time

        // Release the printer resource semaphore
        sem_post(&printerResourceSemaphore);

        // Simulate job completion
        printf("Job %d for User %ld is complete.\n", i, userId);
    }

    pthread_exit(NULL);
}

int main() {
    pthread_t users[NUM_USERS];
    int rc;

    // Initialize semaphores
    sem_init(&printQueueSemaphore, 0, 4);  // Semaphore for print queue control
    sem_init(&printerResourceSemaphore, 0, 4);  // Semaphore for printer resource control

    // Create user threads
    for (long i = 0; i < NUM_USERS; i++) {
        rc = pthread_create(&users[i], NULL, user_function, (void *)i);
        if (rc) {
            fprintf(stderr, "Error creating user thread %ld\n", i);
            return 1;
        }
    }

    // Join user threads
    for (long i = 0; i < NUM_USERS; i++) {
        pthread_join(users[i], NULL);
    }

    // Destroy semaphores
    sem_destroy(&printQueueSemaphore);
    sem_destroy(&printerResourceSemaphore);

    return 0;
}

