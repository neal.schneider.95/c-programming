#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define MEMORY_POOL_SIZE 10

// Define the memory pool as an array
char memory_pool[MEMORY_POOL_SIZE];

// Define a semaphore to control access to the memory pool
sem_t mutex;

// Define a semaphore to track available memory blocks
sem_t available_memory;

// Function to allocate memory
void *allocate_memory(int size) {
    sem_wait(&available_memory);  // Wait for available memory
    sem_wait(&mutex);            // Lock the memory pool

    void *ptr = NULL;
    for (int i = 0; i < MEMORY_POOL_SIZE; i++) {
        if (memory_pool[i] == 0) {
            memory_pool[i] = 1;  // Mark the block as allocated
            ptr = &memory_pool[i];
            break;
        }
    }

    sem_post(&mutex);  // Unlock the memory pool
    return ptr;
}

// Function to deallocate memory
void deallocate_memory(void *ptr) {
    sem_wait(&mutex);  // Lock the memory pool

    char *block_ptr = (char *)ptr;
    int index = block_ptr - memory_pool;
    if (index >= 0 && index < MEMORY_POOL_SIZE) {
        memory_pool[index] = 0;  // Mark the block as deallocated
        sem_post(&available_memory);  // Signal available memory
    }

    sem_post(&mutex);  // Unlock the memory pool
}

// Thread function
void *thread_function(void *arg) {
    int thread_id = *((int *)arg);

    // Allocate memory
    void *ptr = allocate_memory(1);
    if (ptr != NULL) {
        printf("Thread %d allocated memory at address %p\n", thread_id, ptr);

        // Simulate some work with the allocated memory
        // ...

        // Deallocate memory
        deallocate_memory(ptr);
        printf("Thread %d deallocated memory\n", thread_id);
    }

    return NULL;
}

int main() {
    // Initialize semaphores
    sem_init(&mutex, 0, 1);            // Initialize mutex to 1 (unlocked)
    sem_init(&available_memory, 0, MEMORY_POOL_SIZE);  // All memory blocks initially available

    // Create threads
    pthread_t threads[5];  // Create 5 threads
    int thread_ids[5];
    for (int i = 0; i < 5; i++) {
        thread_ids[i] = i;
        pthread_create(&threads[i], NULL, thread_function, &thread_ids[i]);
    }

    // Wait for threads to finish
    for (int i = 0; i < 5; i++) {
        pthread_join(threads[i], NULL);
    }

    // Clean up semaphores
    sem_destroy(&mutex);
    sem_destroy(&available_memory);

    return 0;
}
