/*
Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Preprocessor/performance_labs/Lab25.html
CONDITONAL COMPILATION
Using the preprocessor, do the following:
    Redefine EOF as 66 (without compiler warnings)
    if true is not defined:
        declare false as zero
        declare true as one
        declare SET_BOOLS as 1
    else
    declare SET_BOOLS as 0
inside of main, print SET_BOOLS and EOF.

Try this again, this time adding #include<stdbool.h> to the top of the file.

Neal Schneider
*/

// stdbool.h is added in second part of second part of the exercise.
// Notice how that the block after '#ifndef true' dims in after inserting, 
// That shows that true is defined in stdbool.

#include <stdbool.h>  
#include <stdio.h>

#ifdef EOF
    #undef EOF
#endif
#define EOF 66
#ifndef true
    #define true 0
    #define false 1
    #define SET_BOOLS 1
#else
    #define SET_BOOLS 0
#endif

void main (){
    printf ("EOF = %i\n", EOF);
    printf ("SET_BOOLS = %i", SET_BOOLS);
}
