
#include <stdio.h>
#include <stdlib.h>
#define FATAL {puts("Error");\
exit(EXIT_FAILURE);}
int main(void)
{
    int x = 0;
    printf( "EXIT_FAILURE = %d\n", EXIT_FAILURE);  // Only to show what EXIT_FAILURE is
    if (0 == x)
        FATAL;

    printf("%.5f", 10.0 / x);
    return 0;
}