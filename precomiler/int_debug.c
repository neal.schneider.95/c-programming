#include <stdio.h>
#define DEBUG_INT(x) printf(#x " is %d\n", x)

int main(void)
{
    int getNum = 0;
    scanf("%d", &getNum);
    DEBUG_INT(getNum); //-> printf("getNum" " is %d\n", getNum);
                       //   printf("getNum is %d\n", getNum);
    return 0;
}