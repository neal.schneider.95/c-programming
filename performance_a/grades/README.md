# C Programming: Grades

## KSAT List

This question is intended to evaluate the following topics:

- A0019: Integrate functionality between multiple software components.
- A0018: Analyze a problem to formulate a software solution.
- S0029: Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034: Declare and implement appropriate data types for program requirements.
- S0031: Utilize logical operators to formulate boolean expressions.
- S0035: Declare and/or implement of arrays and multi-dimensional arrays.
- S0032: Utilize relational operators to formulate boolean expressions.
- S0033: Utilize assignment operators to update a variable.
- S0007: Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052: Implement a function that returns a single value.
- S0048: Implement a function that receives input parameters.
- S0079: Validate expected input.
- S0081: Implement a looping construct.
- S0108: Utilize post and pre increment/decrement operators.
- S0082: Implement conditional control flow constructs.
- S0160: Utilize the standard library.

## Tasks

Implement a function `computeScore` that computes the weighted average of a student’s scores and returns the course grade.

### Parameters

1. `final`: An integer containing the score of the final
2. `midterm`: An integer containing the score of the midterm
3. `project`: An integer array of 6 elements containing the scores of the projects
4. `quiz`: An integer array of 4 elements containing the scores of the quizzes

### Return

An integer containing the final grade rounded to the nearest integer or `ERROR_INVALID_DATA` (13) if any score is < 0 or > 100

- The function should compute the floating point before rounding the score to the nearest integer.

## Scoring

The overall score is computed as follows.

- The student takes four (4) quizzes, six (6) projects, a Midterm exam, and a Final Exam.
- The student’s lowest project score is discarded; therefore, the scoring only includes five (5) projects.
- Each score is in a range from 0 to 100.
- The overall score is computed using the values below as such:
  - Final – 30%
  - Midterm – 25%
  - Quizzes – 5% each
  - Projects – 5% each

`overallScore = final * .30 + midterm * .25 + quiz1 * .05 + quiz2 * 05 + ... etc.`

## Building and Testing Your Solution

The unit tests for your solution are located in `testcases.cpp`.  In order to run the tests, execute the following commands from a terminal in the `grades` directory:

1. `cmake .` - you only need to run this once
2. `make` - run this to compile your code and build the executable
3. `make test`

Repeat steps 2 and 3 above every time you make a change.

### Optional

Instead of `make test` you can run the executable file directly with the following command:

1. `./TestCode`
