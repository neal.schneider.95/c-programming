/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0036 - Declare and implement a char * array (string).
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0110 - Implement error handling.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0160 - Utilize the standard library.
	S0033 - Utilize assignment operators to update a variable.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 * The function letterFrequency takes as input a one-line strings
 * and determines the total occurrences of each letter of the alphabet in the line.
 *
 * Case sensitivity is not an issue i.e. "A" and "a" are the considered the same for this question
 * If a non-alpha character is encountered it should be ignored, adding nothing to any count.
 *
 * @param sentence			The input string to process
 * @param frequencyTable	An already allocated buffer in which to place the output
 *
 * Expected Return Values:
 *		- The task is successful: 1
 *		- Bad input is provided: 0
 */

int jane_sum (int arr[], int size);

int letterFrequency(const char* sentence, int* frequencyTable)
{
	// if input parameters are invalid, return 0
	if (sentence == NULL || strlen(sentence) == 0) 
        return 0;
    else {
		// iterate over the string sentence
        for (int i = 0; i < strlen(sentence); i++) {
			// if char is a uppercase, subtract 65 to match frequencyTable
            if (sentence[i] >= 65 && sentence[i] <= 90)
				// increment the letter in that table
                frequencyTable[sentence[i]-65]++;

			// if char is a lowercase, subtract 97 to match frequencyTable
            else if (sentence[i] >= 97 && sentence[i] <= 122) 
				// increment the letter in that table
                frequencyTable[sentence[i]-97]++;
        }
		// return 1 if letters were successfully counted
        if (jane_sum(frequencyTable, 26) > 0)
            return 1;
    }
}

// helper function to add up all counts in the frequencyTable
int jane_sum (int arr[], int size) {
    int sum = 0;
    for (int i = 0; i < size; i++) 
        sum += arr[i];
    return sum;
}
