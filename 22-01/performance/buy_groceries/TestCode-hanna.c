/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/



#include <stdio.h>
#include <math.h>


int buyGroceries(int groceriesList[], int size)
{
double total = 0;
    double cost;
    // checks if list provided is null or equal to 0
    if(groceriesList == NULL || groceriesList == 0)
    // returns 0 if bad input provided
        return 0;
    // loops whle true
    while(*groceriesList){
        // if first item is 0 it returns 0
        // bad input
        if(groceriesList[0] == 0){
            return 0;
        }
        // checks if even item is 0 and size is greater than 0
        if((size) % 2 == 0 && size > 0){
            // loops through list based on every other item in the list
            for(int i = 0; i<size; i = i + 2){
                // checks if legal input which is between 1 and 4
                if((groceriesList[i] > 0 && groceriesList[i] <= 4)){
                    switch (groceriesList[i])
                    {
                        // eggs
                        case 1:
                            // if the number of eggs is 0
                            // bad input. return 0
                            if(groceriesList[i+1] == 0)
                                return 0;
                            // calc the cost
                            // 3.5 * amount
                            cost = 3.50 * groceriesList[i+1];
                            // calc discount if 5 or more
                            if(groceriesList[i+1] >= 5)
                                // subtract discount of 5% from cost
                                cost = cost - (cost * 0.05);
                            // update running total
                            total = total + cost;
                            break;
                        // milk
                        case 2:
                            // if the number of milk is 0
                            // bad input. return 0
                            if(groceriesList[i+1] == 0)
                                return 0;
                            // calc the cost
                            // 2.25 * amount
                            cost = 2.25 * groceriesList[i+1];
                            // calc discount if 5 or more
                            if(groceriesList[i+1] >= 5)
                                // subtract discount of 5% from cost
                                cost = cost - (cost * 0.05);
                            // update running total
                            total = total + cost;
                            break;
                        // bread
                        case 3:
                            // if the number of bread is 0
                            // bad input. return 0
                            if(groceriesList[i+1] == 0)
                                return 0;
                            // calc the cost
                            // 1.99 * amount
                            cost = 1.99 * groceriesList[i+1];
                            // calc discount if 5 or more
                            if(groceriesList[i+1] >= 5)
                                // subtract discount of 5% from cost
                                cost = cost - (cost * 0.05);
                            // update running total
                            total = total + cost;
                            break;
                        // sugar
                        case 4:
                            // if the number of bread is 0
                            // bad input. return 0
                            if(groceriesList[i+1] == 0)
                                return 0;
                            // calc the cost
                            // 4.15 * amount
                            cost = 4.15 * groceriesList[i+1];
                            // calc discount if 5 or more
                            if(groceriesList[i+1] >= 5)
                                // subtract discount of 5% from cost
                                cost = cost - (cost * 0.05);
                            // update running total
                            total = total + cost;
                            break;
                        default:
                            return 0;
                            break;
                    }
                }
                else
                    return 0;
                    
            }
        }
        // Round the total and return
        return round(total);
    }
    // Should reach here
    return 0;
}
