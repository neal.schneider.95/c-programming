/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/



#include <stdio.h>
#include <math.h>


int buyGroceries(int stuff[], int size)
{
    // check if stuff is == 0 or not an even number return 0 as per validation in instructions 
    if(size%2 != 0 || size == 0)
        return 0;

    // initializing all the varialbes 
    int i = 0; //  for groceries
    int q = 1; // for quanity 
    // since its a price and decimal precision is 7 used a float
    float groceries1 = 0;    // eggs
    float groceries2 = 0;    // milk
    float groceries3 = 0;    // bread
    float groceries4 = 0;    // sugar
    float total = 0;
    
    while(i < size && q < size) 
    {
        if(stuff[i] < 1 || stuff[i] > 4) // checking if stuff is the numbers 1-4 if not return 0 per instructions 
        { 
            return 0;
        }
        else if(stuff[q] <= 0) 
        {
            return 0;
        }
        else
        {
            // switch statement to go through the grocieries 
            switch (stuff[i])
            {
                case 1:             // eggs 
                    if(stuff[q]<5)
                    {
                        groceries1 = stuff[q]*3.50;
                        i+=2;
                        q+=2;
                    }
                    else
                    {
                        groceries1 = (stuff[q]*3.50) - (stuff[q]*3.50)*.05; // 5 or more for an item, a 5% discount is applied on those items
                        i+=2;
                        q+=2;
                    }
                    break;
                case 2:                 // milk
                    if(stuff[q]<5)
                    {
                        groceries2 = stuff[q]*2.25;
                        i+=2;
                        q+=2;
                    }
                    else
                    {
                        groceries2 = (stuff[q]*2.25) - (stuff[q]*2.25)*.05; //5 or more for an item, a 5% discount is applied on those items
                        i+=2;
                        q+=2;
                    }
                    break;              // bread
                case 3:
                    if(stuff[q]<5)
                    {
                        groceries3 = stuff[q]*1.99;
                        i+=2;
                        q+=2;
                    }
                    else
                    {
                        groceries3 = (stuff[q]*1.99) - (stuff[q]*1.99)*.05; // 5 or more for an item, a 5% discount is applied on those items
                        i+=2;
                        q+=2;
                    }
                    break;
                case 4:             // sugar
                    if(stuff[q]<5)
                    {
                        groceries4 = stuff[q]*4.15;
                        i+=2;
                        q+=2;
                    }
                    else
                    {
                        groceries4 = (stuff[q]*4.15) - (stuff[q]*4.15)*.05; // 5 or more for an item, a 5% discount is applied on those items
                        i+=2;
                        q+=2;
                    }
                    break;
            }
        total = groceries1 + groceries2 + groceries3 + groceries4; // getting total cost of items 
        } 
    }
    return round(total); // using round to rround the total price.. 
}
