/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/



#include <stdio.h>
#include <math.h>


int buyGroceries(int stuff[], int size)
{
//check to see if size is even or if 0
//even check done to ensure there is a proper pair(item,value)
	if(size%2 != 0 || size == 0)
        return 0;
	//delcare int looping variables
    int i = 0;
    int j = 1;
	//declare double representing items and total
	double item1, item2,item3, item4, total = 0;

    //loop while i and j are less than size
    while(i<size && j<size){
		//check to see if "item" value of array is bewteen 1 and 4 
        if(stuff[i]<1 || stuff[i]>4){
            //i+=2;
            return 0;
        }
		//check to see if number of items is a value bigger than 0
        else if(stuff[j]<=0){
           // j+=2;
            return 0;
        }
        else{
		//if so switch statment over items spots ie the 0th ,2nd indices
        switch (stuff[i])
        {
		//ie item1 
        case 1:
			// check if number of items purchased is greater than 5
            if(stuff[j]<5){
				//dont recieve discount so item1 is the value of stuff[j] * price of that item
				//this will also be the condition all the way down the switch
                item1 = stuff[j]*3.50;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            else{
                item1 = (stuff[j]*3.50) - (stuff[j]*3.50)*.05;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            break;
        case 2:
            if(stuff[j]<5){
                item2 = stuff[j]*2.25;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            else{
                item2 = (stuff[j]*2.25) - (stuff[j]*2.25)*.05;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            break;
        case 3:
            if(stuff[j]<5){
                item3 = stuff[j]*1.99;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            else{
                item3 = (stuff[j]*1.99) - (stuff[j]*1.99)*.05;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            break;
        case 4:
            if(stuff[j]<5){
                item4 = stuff[j]*4.15;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            else{
                item4 = (stuff[j]*4.15) - (stuff[j]*4.15)*.05;
				//increment both i and j by 2 to get to the next item,quantity pair
                i+=2;
                j+=2;
            }
            break;
        
        default:
            break;
        }
		//calculate total
        total = item1 + item2+ item3 + item4;

        }
        
    }
    //return rounded total to 2 decimal places
    return round(total);
}