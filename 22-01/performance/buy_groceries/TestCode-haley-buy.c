/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/



#include <stdio.h>
#include <math.h>

// Refer to README.md for the problem instructions

int buyGroceries(int stuff[], int size)
{
    //If the "size" variable is passed as 0, neg num, or odd number, it is invalid. Return 0
    if (size <= 0 || size%2 !=0)
    {
        return 0;
    } 

    //Check for 0 and negative quantities
    for (int e = 0; e < size; e++)
    {
        int intIndexItemVsQuantityCheck = e%2;
        if (stuff[e] <= 0 && intIndexItemVsQuantityCheck == 1)
        {
            return 0;
        }
        else if ((stuff[e] <= 0 || stuff [e] >= 5) && intIndexItemVsQuantityCheck == 0)
        {
            return 0;
        }
    }

    // declare a price chart to make life easier
    double arrPriceChart[4][3] = { {1, 3.5, 3.325} , {2, 2.25, 2.1375}, {3, 1.99, 1.8905}, {4, 4.15, 3.9425} };

    // declare variables to keep track of everything
    double dblCurrentItemPrice = 0;
    int intPrcChrtCounter = 0;
    double dblRunningTotal = 0;
    int intQuantityPurchased = 0;

    //vars for easy reading
    int intStuffValue = 0;
    int intPriceChartValue = 0;


    // loop through, incrementing by 2 to skip to every other item (the item index vs price index)
    for (int i = 0; i < size; i+=2)
    {
        // assign the quantity of the purchased item
        intQuantityPurchased = stuff[i+1];

        // loop through the price chart
        for (intPrcChrtCounter =0; intPrcChrtCounter < 8; intPrcChrtCounter++)
        {
            // get the actual item index number, assign it for easy reading
            intPriceChartValue = arrPriceChart[intPrcChrtCounter][0];

            // get the current "stuff" index value, reassign it for easy reading
            intStuffValue = stuff[i];

            //if the variables declared above match...
            if (intStuffValue == intPriceChartValue)
            {
                //we get the price. the price index will change based on quantity purchased, 
                // index 2 for a discount (5+ purchased), index 1 for full price 

                if (intQuantityPurchased >= 5)
                {
                    // discount price
                    dblCurrentItemPrice = arrPriceChart[intPrcChrtCounter][2];
                }
                else
                {
                    // full price
                    dblCurrentItemPrice = arrPriceChart[intPrcChrtCounter][1];
                }

                //add it to the running total and break out to save some loops
                dblRunningTotal = dblRunningTotal + (intQuantityPurchased * dblCurrentItemPrice);
                break;
            }
        }
    }

    // round the running total to be returned
    dblRunningTotal =  round(dblRunningTotal);

    return dblRunningTotal;
}