/*
This question is intended to evaluate the following topics:
	A0018 - Analyze a problem to formulate a software solution.
	A0019 - Integrate functionality between multiple software components.
	S0081 - Implement a looping construct.
	S0082 - Implement if and if/else constructs.
	S0083 - Implement a switch statement.
	S0034 - Declare and implement appropriate data types for program requirments.
	S0035 - Declare and implement of arrays and muli-dimensioanl arrays.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathmatical equations.
	S0108 - Utilize post and pre increment/decrement operators.
	S0051 - Implement a function that implements pass by reference input parameters.
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0079 - Validate expected input.
	S0033 - Utilize assignment operators to update a variable.
*/

/* Write a function buyGroceries that accepts and int array and an int representing the size
of the array. Every two (2) indicies in the array represents an item number and quantity to purchase groceries.
The function will iterate the array and determine the total cost of groceries as a floating point number
based on the item number, quantity of each item, and cost of each item. There are four valid items.

item #  item   Cost
1       eggs   3.50
2       milk   2.25
3       bread  1.99
4       sugar  4.15

For example, the function may receive an array with the following:   1 5 2 3 4 4

resulting in 5 eggs at $3.50, 3 milks at $2.25 each, and 4 sugars at $4.15 each.

Compute a grand total based on quantity and cost of each item. If there is a quantity
of 5 or more for an item, a 5% discount is applied on those items. So, for the above
example there would be a 5% discount applied to the eggs.

Once the total is computed, round the value to an integer and return it.

The size passed for the array should always be an even number > 0, if it is not, the function
should return 0.

If any item number is not a 1, 2, 3, or 4, the function should return 0.

If any quantity is 0 or less, the function should return 0.

If a valid size is passed (even number) assume array is of that size.

Assume there will be no duplicate entries for an item number.

*/



#include <stdio.h>
#include <math.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int buyGroceries(int listG[], int size)
{
    //accept int array aka list G for groceries
    //VALIDATION CHECKS
        //check is size is even, if not return 0
    if(size%2 != 0)
    {
        return 0;
    }    
        //check if size 0 or less
    if (size == 0)
    {
        return 0;
    }

        //check to make sure item number is 1 - 4
    // for(int x = 0; x<size; x+2)
    //     if(listG[x] <1 || listG[x] >4){
    //         return 0;
    //     }
        //check to make sure quant is not 0 or less
    // for(int y = 1; y<=size; y+2)
    //     if(listG[y] <= 0)
    //         return 0;   
        
        
    //
      //index of item
    int i = 0;
    //index of quantity
    int j = 1;
    //float item price totals and int for send
    float item1 = 0;
    float item2 = 0;
    float item3 = 0;
    float item4 = 0;
    float total = 0;
    //two new variables added for rounded int and temp for discount
    int send;
    float temp = 0;
        //adding validation for 1-4 and 0 into while loop for calcs
    //while item is less then size and quantity is less than or equal to size 
    while(i<size && j<=size){
            //validation check for items 1-4 only, return 0 if true
        if(listG[i]<1 || listG[i]>4){
            return 0;
        }
            //if the quantity is less than 0...return 0
        else if(listG[j]<=0){
            return 0;
        }
        //else validation good...calc the prices
        else{
// item #  item   Cost
// 1       eggs   3.50
// 2       milk   2.25
// 3       bread  1.99
// 4       sugar  4.15
        switch (listG[i])
        {
        case 1:
            //if less than five, get standard price
            if(listG[j]<5){
                //eggs 3.50
                item1 = listG[j]*3.50;
                i+=2;
                j+=2;
            }
            else{
                //else you get costco supersaver discount of 5 %
                temp =  (listG[j]*3.50)*.05;
                //use temp vari to store savings and subtract
                item1 = (listG[j]*3.50) - temp;
                //need to move two indicies since item and quant are pair
                i+=2;
                j+=2;
            }
            //break if case met
            break;
        case 2:
            //milk 2.25
            if(listG[j]<5){
                item2 = listG[j]*2.25;
                i+=2;
                j+=2;
            }
            else{
                temp =  (listG[j]*2.25)*.05;
                item2 = (listG[j]*2.25) - temp;
                i+=2;
                j+=2;
            }
            break;
        case 3:
            //bread is 1.99
            if(listG[j]<5){
                item3 = listG[j]*1.99;
                i+=2;
                j+=2;
            }
            else{
                temp = (listG[j]*1.99)*.05;
                item3 = (listG[j]*1.99) - temp ;
                i+=2;
                j+=2;
            }
            break;
        case 4:
            //sugar is 4.15...must be organic
            if(listG[j]<5){
                item4 = listG[j]*4.15;
                i+=2;
                j+=2;
            }
            else{
                temp = (listG[j]*4.15)*.05;
                item4 = (listG[j]*4.15) - temp;
                i+=2;
                j+=2;
            }
            break;
        }
        
    }
    //add up all items into total
    total = item1 + item2+ item3 + item4;
    //round total and send it
    send = round(total);
        
    }
    
    return send;
}
