#include <stdio.h>
int calls = 0;
struct data {
    double v0;
    double v1;
    double v2;
    double v3;
    double v4;
    double v5;
    double v6;
    double v7;
    double v8;
    double v9;
} Data;
void m(struct data input) 
{
    calls++;
    printf("hi, %d\n",calls);
    // hi, 43178 where it fails with m()
    // hi, 14391 with m(struct data ) data with 10 doubles
    m(input);
}

void main() 
{
    struct data d; // = {0.1, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
    m(d);
}