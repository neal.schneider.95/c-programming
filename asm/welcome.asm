; Assignment 1.asm
; 

global _start


_start:
	; print message
	mov eax, 0x4 		; load call (write)
	mov ebx, 0x1		; to STDOUT 
	mov ecx, message	; message
	mov edx, mlen 		; length
	int 0x80
	
    mov input_var, DWORD 0xC8  ; move 200 (C8 hex) to input_var, but then gets abandonded

	mov eax, 0x1 		; load exit call
	mov ebx, 0x5		; exit status
	int 0x80
	
section .data
	message: db "Welcome to Assembly programming.", 10, "Your grade will be randomly assigned", 10, "by the Intel 8086 processor!", 10
	mlen equ $-message

section .bss
    input_var: RESD
    sum: RESD
