; Assignment 2.1
; 1. Write a program that determines the amperage that would be pulled from a standard 120-volt wall outlet by a computer with a 1200-watt power supply running at maximum power. 

; Use the standard equation W=VA (Watts = Volts * Amperage).

global _start


_start:
	; print message
	mov ax, [amps]   ; load amps
	mov bx, [volts]	; load volts 
	mul bx 
    nop 
    nop
    nop
	
section .data
	amps: db 30
    volts: db 120