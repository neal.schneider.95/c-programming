# GDB commands

gdb -q ./filename

info functions    // get information on available functions

shell readelf -h <filename>  // get header information on file executable

break _start   // breaking point at entry point

set disassembly-flavor intel  // set assembly format to intel

run   // execute program in gdb

disassemble    // shows program assembly instructions

info variables   // show variable names with address location

x/xb <0xaddress> 
or
x/xb <&variable>      // first "x" is for examine and second 'x' is for "how many" bytes. 'b" is for bytes. You can also use 'w' and 'd' 


