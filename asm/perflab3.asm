; LAB #3

; Order of operations (Challenge Assignment)
; Create a program that calculates the following expression: answer = (A + B) – (C + D)
    ; • The answer must be stored in a variable of the correct data type given your data
    ; (A, B, C, D).
    ; • The values for your data (A, B, C, D) must be stored in registers (e.g., eax, ebx), not variables.
    ; • You must supply the initial values for the data (A, B, C, D).
    ; • Create a string that could accompany the answer in an output statement
    ; (e.g., "The answer is:"). You do not have to output the string.
    ; • Comment each line of codeto briefly describe each line’s meaning.


section .data
    a: qword  0x1000
    b: qword  0x0100
    c: qword  0x0010
    d: qword  0x0001

section .text
	global _start

_start:
; ; populate registers
; 	mov eax, 0x1000 ; a  ; load A
; 	mov ebx, 0x0100 ; b  ; load B
; 	mov ecx, 0x0010 ; c  ; load C
; 	mov edx, 0x0001 ; d  ; load D

; populate registers from variables
	mov eax, [a]  ; load A
	mov ebx, [b]  ; load B
	mov ecx, [c]  ; load C
	mov edx, [d]  ; load D

    add eax, ebx    ; Add B to A
    add ecx, edx    ; Add D to C
    sub eax, ecx    ; Subtract the sums


; Exit gracefully
	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;
