/*
// Ref: https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Variables/performance_labs/lab2.html

// Declare and initialize variables of data types:

//     int
//     float
//     double
//     char
// Type cast and print the following:

//     int -> float
//     int -> char
//     float -> double
//     double -> float
//     char -> int
//     33 -> char
// Note: Initialize int to a value between 32 and 126
// Neal Schneider
// 22 Sep 21
*/

#include <stdio.h>

int main(){
    int i = 32;
    float f = 33.3;
    double df = 34.444444444444444444444444444444;
    char c = 36;
    int intArray[10]; 

    printf ("%f\n", (float) i);
    printf ("%c\n", (char) i);
    printf ("%fl\n", (double) f);
    printf ("%f\n", (float) df);
    printf ("%f\n", (int) c);
    printf ("%c\n", 33);

    printf ("size of int array: %d", sizeof(intArray));
}