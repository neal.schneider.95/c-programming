#include <stdio.h>

int main()
{
    struct player
    {
        char name[32];
        int highscore;
        float hoursPlayed;      // Extra element
    };
    struct player xbox;

    printf("Enter the player's name: ");
    scanf("%s",xbox.name);
    printf("Enter their high score: ");
    scanf("%d",&xbox.highscore);
    printf("Enter their hours played: ");   // get hours played
    scanf("%f",&xbox.hoursPlayed);          // Read float
    

    printf("Player %s has a high score of %d and spent %.2f hours doing so.\n",
            xbox.name, xbox.highscore, xbox.hoursPlayed);
    return(0);
}