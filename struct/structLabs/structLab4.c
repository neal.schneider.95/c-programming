#include <stdio.h>
#include <string.h>

int main()
{
    typedef struct employee
    {
        char id[6];
        char name[20];
        float rate;
        float hours;
    } Employee;
    
    Employee list[10] = {};
    int i = 0;

    printf("Enter Contact list: \n(enter twice to exit)\n");
    while (1) {
        printf("Person %d First Name: ", i+1);
        fgets(list[i].name.first, 20, stdin);
        list[i].name.first[strlen(list[i].name.first)-1] = 0;
        if (list[i].name.first[0] == 0) break;

        printf("    Last Name: ");
        fgets(list[i].name.last, 25, stdin);
        list[i].name.last[strlen(list[i].name.last)-1] = 0;

        printf("        Phone: ");
        fgets(list[i].phone, 20, stdin);
        list[i].phone[strlen(list[i].phone)-1] = 0;

        i++;
    }
    int list_size = i;

    for (i = 0; i< list_size; i++) {
        printf("%3d %20s %25s %20s\n", i+1, 
            list[i].name.first,
            list[i].name.last,
            list[i].phone);
    }
    return(0);
}