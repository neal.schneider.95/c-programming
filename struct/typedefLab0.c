#include <stdio.h>

typedef long double real;
typedef short power;

void main (){
    real x, result;
    power y;
    printf("Enter a real number: ");
    scanf("%Lf", &x);
    printf("Enter a power (integer): ");
    scanf("%hi", &y);
    result = 1;
    for (power i = 0; i < y; i++) {
        result *= x;
    }
    printf("%Lf raised to the %d power is %Lf\n", x, y, result);
}