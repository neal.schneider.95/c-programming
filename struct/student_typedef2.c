
#include <stdio.h>

typedef struct student
{
    char name[16];
    char grade;
    int secretFavoritenessLevel;

} Student;

int main () {
    //Then we can do this...
    Student bob = {"Bob Ross", 100, 100};
    Student *student_ptr = &bob;

    struct student sally = {"Sally Moreau", 70, 105};

    Student * group1[4] = { &bob, &sally};

    for (int i=0; i<4; i++){
        printf ("%p -> %s\n", group1[i], group1[i]->name);
        printf ("  %p -> %d (grade)\n" , &group1[i]->grade, group1[i]->grade);
        printf ("  %p -> %d (sfl)\n" , &group1[i]->secretFavoritenessLevel, group1[i]->secretFavoritenessLevel);
    }
    printf ("%p/n", NULL);
}


