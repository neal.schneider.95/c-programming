#include <string.h>
#include <stdlib.h>

typedef struct student{
    char name [20];
    int grade;
    int secretFavoritenessLevel;
} student;

int main(){
    struct student alice = {"Alice Cooper", 79, 3};
    struct student bob = {"Bob Ross", 100, 100};
    struct student *groupOne[4] = {&alice, &bob};
    // ...
    groupOne[2] = (student*) malloc(sizeof(student));
    strcpy(groupOne[2]->name, "Ethan Allan");
    groupOne[2]->grade = 80;
    groupOne[2]->secretFavoritenessLevel = 90;

    groupOne[3] = &alice;
}