#include <string.h>  // needed for strcpy
#include <stdlib.h>  // needed for malloc

struct lotForSale
{
    int lot_number;
    float lot_cost;
    float square_footage;
    char address[100];
};

int main(void)
{
    //Declare a struct variable
    struct lotForSale *palmStreet = (struct lotForSale*) malloc(sizeof(struct lotForSale));

    //Define the first member
    palmStreet->lot_number = 8755;
    
    //Define the second member
    palmStreet->lot_cost = 25000;
    
    //Define the third member
    palmStreet->square_footage = 6534;

    //Define the last member
    strcpy (palmStreet->address, "101 Moetown Rd., Crestview, WA 97192");
}