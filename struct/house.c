typedef struct houseForSale {
    char mailing_address[1024];
    float cost_per_sq_ft;
    float square_footage;
    float total_cost;
} HouseForSale;

int main(void)
{
    struct houseForSale FF4HQ = { 
        "Baxter Building, 42nd Street, Madison Avenue, Manhattan", 107.15, 28000, 0
    };
    HouseForSale durdenManor = {
        "420 Paper St. Wilmington, DE 19886", 0.01, 4500, 0
    };

    HouseForSale mustSellHouses[4]; // Array of 4 structs
    mustSellHouses[0] = FF4HQ;
    mustSellHouses[1] = durdenManor;
}