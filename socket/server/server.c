#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
// #include <sys/socket.h>

#include <arpa/inet.h>

#define PORT 12345
#define BUFFER_SIZE 1024

int main() {
    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    // Create a socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Error creating socket");
        exit(EXIT_FAILURE);
    }

    // Set up the server address struct
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(PORT);

    // Bind the socket to the server address
    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
        perror("Error binding socket");
        close(server_socket);
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(server_socket, 5) == -1) {
        perror("Error listening for connections");
        close(server_socket);
        exit(EXIT_FAILURE);
    }

    printf("Server is listening on port %d...\n", PORT);

    while (1) {
        // Accept a client connection
        client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_len);
        if (client_socket == -1) {
            perror("Error accepting client connection");
            continue;
        }

        printf("Accepted connection from %s:%d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        // Receive filename, and maybe start of file

        char buffer[BUFFER_SIZE];;
        char filename[256] = {};
        ssize_t bytes_received;
        ssize_t total_bytes = 0;
        ssize_t offset = 0;

        bytes_received = recv(client_socket, buffer, sizeof(buffer), 0);
        if (bytes_received < 0) {
            perror("Error receiving filename");
            close(client_socket);
            continue;
        }
        strcpy(filename, buffer);
        printf("Filename: %s (len:%ld, rx: %ld)\n", buffer, strlen(buffer), bytes_received);
        if (bytes_received > strlen(buffer) + 1) {
            offset = bytes_received - strlen(buffer) - 1;
        }
        // Create and open the file for writing
        FILE *file = fopen(buffer, "wb");
        if (file == NULL) {
            perror("Error opening file for writing");
            close(client_socket);
            continue;
        }

        while (1) {
            if (offset) {
                fwrite(buffer + offset, 1, bytes_received - offset, file);
                total_bytes = bytes_received - offset;
                offset = 0;
                printf("Wrote %ld bytes (offset)\n", total_bytes);
                continue;
            }
            bytes_received = recv(client_socket, buffer, sizeof(buffer), 0);
            if (bytes_received < 0) {
                break;
            }
            printf("Writing %ld bytes\n", bytes_received);
            fwrite(buffer, 1, bytes_received, file);
            total_bytes += bytes_received;
            offset = 0;
            if (bytes_received <= 0) {
                break;
            }

        }

        fclose(file);
        printf("Received file '%s' from %s:%d (%ld bytes)\n", 
        filename, inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port), total_bytes);
        close(client_socket);
    }

    close(server_socket);

    return EXIT_SUCCESS;
}
