#include <stdio.h>

void main () {


double numberArray[3][4] = {
    { 0.0, 0.1, 0.2, 0.3},
    { 1.0, 1.1, 1.2, 1.3},
    { 2.0, 2.1, 2.2, 2.3}
};

    // Print the matrix
    for (int row = 0; row<3; row++)
    {
        for (int col=0; col<4; col++) 
        {
            printf("%5.1lf", numberArray[row][col]);
        }
        printf("\n");     // Completed printing the row, so start a new one
    }

}