#include <stdio.h>
#include <string.h>     // where strtok and strlen are defined 
#include <ctype.h>      // where tolower is defined

int main() {
    char *word;         // Pointer to use to get each word in turn
                        // After it's assigned, it *is* a string
    char phrase[1024] = {0};
        // "To form a pig Latin phrase from an English language phrase tokenize the phrase into words with function strtok";
    printf("Enter a string to convert to pig latin\n:");
    fgets(phrase, 1024, stdin);              // get string 1

    // fgets includes the linefeed from the input, so overwrite that with the null-terminator.
    phrase[strlen(phrase)-1] = 0;

    // grab the first word by passing the phrase to strtok and space delimeter.
    word = strtok(phrase, " ");

    // loop until word comes back empty (NULL)
    while (word) {

        // print each word, but modified.
        // word+1 is actually doing some "pointer math" that we will discuss in a future lesson.
        // This could also be done using the 'address of' operator
        // in that case, we would use &word[1] -- that means the address of the second char

        printf("%s%cay ", word+1, tolower(word[0]));

        // get the next word.  Passing NULL tells strtok to keep searching in the string already provided.
        word = strtok(NULL, " ");
    }
    printf("\n");
}