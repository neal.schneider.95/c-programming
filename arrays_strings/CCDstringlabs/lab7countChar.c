#include <stdio.h>
#include <string.h>     // where strstr and strlen are defined 

int main() {
    char *searchStr;    // Pointer to use to get each found char
                        // After it's assigned, it *is* a string
    char phrase[1024] = {0};
    char needle[10] = {0};
    int count = 0;
    printf("Enter a phrase to search: ");
    fgets(phrase, 1024, stdin);                 // get the phrase
    phrase[strlen(phrase)-1] = 0;

    printf("What's the search character? ");
    fgets(needle, 10, stdin);              // get the search character 

    searchStr = strchr(phrase, needle[0]);
    // loop until word comes back empty (NULL)
    while (searchStr) {
        count++;        // Found the search character, so increment the counter

        // advance the searchStr to the next occurrence using strchr
        searchStr = strchr(searchStr, needle[0]);
    }  // end while
    printf("I found '%c' %d times.\n", needle[0], count);
}  // end main

/*
Output:
$ gcc searchChar.c
$ ./a.out
Enter a phrase to search: now good men... let's look for me...  Don't bother searching for meteorites!
What's the serach character? .
I found '.' 6 times.
*/