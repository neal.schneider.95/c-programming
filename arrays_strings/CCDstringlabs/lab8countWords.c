#include <stdio.h>
#include <string.h>

int main() {
    char text[1024] = {0};
    char *word;
    int count = 0;

    // remind what's being asked and prompt user for text 
    printf("Enter a phrase to count words.\nPress enter twice to end input\n:");
    
    // read text in from console (up to 1024 characters)
    fgets(text, 1024, stdin);

    // loop until user enters nothing (two returns)
    while (strlen(text)>1) {

        // start the tokenizer by passing the text and the delimiters
        word = strtok(text, " ");

        // loop until the word comes back empty (NULL)
        while (word) {
            count++;                            // increment the counter
            word = strtok(NULL, " ");           // get the next word
        }
        printf("[%d] -> ", count);              // Show current count and prompt
        fgets(text, 1024, stdin);               // get next block of text
    }
    printf("I found %d words. \n", count);      // print out the final count
}