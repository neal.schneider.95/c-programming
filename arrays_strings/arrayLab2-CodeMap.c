// Substitution code map

#include <stdio.h>  // To provide printf()

void main ()
{
    int row, col;                           // Declaring the loop variables
    char matrix[26][26] = {0};

    // initialize the matrix
    for (row = 0; row<26; row++)        // Loop over the rows
    {
        for (col = 0; col<26; col++)    // Loop over the columns in each row
        {
            // Assign the value to each element
            // The modulus operator (%) rolls the value back to zero
            // when it exceeds 25 and becomes 'A' again.
            // When compiled, 'A' is converted into its ASCII value.
            matrix[row][col] = 'A' + ((row + col) % 26);
        }
    }

    // Print the matrix
    for (row = 0; row<26; row++)
    {
        for (col=0; col<26; col++) 
        {
            printf("%c", matrix[row][col]);
        }
        printf("\n");     // Completed printing the row, so start a new one
    }

}