// airline reservation system

#include <stdio.h>
#define NUM_FIRST_CLASS 5
#define TOTAL_SEATS 10

// A function to print the current seat availability
// seats: an array of current seat assignments (0 = available, 1 = taken)
void printSeats(char *seats){
    for (int i = 0; i<TOTAL_SEATS; i++) {
        if (seats[i])
            printf("X");
        else
            printf(".");
    }
    printf("\n");
}

void printTicket(int seat) {
    printf("+--------------------------+\n");
    if (seat < NUM_FIRST_CLASS) {
    printf("|    FIRST CLASS TICKET    |\n");
    } else {
    printf("|   ECONOMY CLASS TICKET   |\n");
    }
    printf("|      SEAT %2d             |\n", seat + 1);
    printf("+--------------------------+\n");
}

void main() {
    char seats[TOTAL_SEATS] = {0};      // Zeroized array for seats
    char ui = 0;                        // User input
    char foundSeat = 0;                 // Vacant Seat is found
    char quit = 0;
    char firstClassFull = 0;
    printf("Airline Reservation system:\n");
    printf("  Please type 1 for \"first class\"\n");
    printf("  Please type 2 for \"economy\"\n");

    while(1){
        foundSeat = 0;
        printf("(1,2,q)");
        ui = getc(stdin);
        if (ui == '1'){
            for(int i = 0; i<NUM_FIRST_CLASS; i++){
                if (seats[i] == 0) {
                    seats[i] = 1;
                    printTicket(i);
                    foundSeat = 1;
                    break;
                }
            } 
            if (foundSeat) continue;
            firstClassFull = 1;
            printf("No first class seats are available.\nWould you like to try economy class? (y/n) ");
            ui = getc(stdin);
            if (ui == 'Y' || ui == 'y') 
                ui = '2';
        }
        if (ui == '2') {
            foundSeat = 0;
            for (int i = NUM_FIRST_CLASS; i<TOTAL_SEATS; i++) {
                if (seats[i] == 0){
                    seats[i] = 1;
                    printTicket(i);
                    foundSeat = 1;
                    break;
                }
            }
            // printSeats(seats);
            if (firstClassFull && !foundSeat) {
                printf ("Next flight leaves in 3 hours.\n");
                ui = 'q';
            }
            else if (!firstClassFull && !foundSeat)
            {
                printf("No economy seats are available. Would you like to try first class? ");
                ui = getc(stdin);
                foundSeat = 0;
                if (ui == 'Y' || ui == 'y') {
                    for(int i = 0; i<NUM_FIRST_CLASS; i++) {
                        if (seats[i] == 0) {
                            seats[i] = 1;
                            printTicket(i);
                            foundSeat = 1;
                            break;
                        }
                    }
                    if(!foundSeat){
                        printf ("Next flight leaves in 3 hours.\n");
                        ui = 'q';
                    }
                }    
            }
        }
        // printSeats(seats);
        if (ui == 'q' || ui == 'Q') break;
    }
}