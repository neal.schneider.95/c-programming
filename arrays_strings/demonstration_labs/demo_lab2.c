#include <stdio.h>

int main(void)
{
	char myFavoriteWord [20] = {0};
	
	myFavoriteWord[0] = 70;
	myFavoriteWord[1] = 97;
	myFavoriteWord[2] = 110;
	myFavoriteWord[3] = 116;
	myFavoriteWord[4] = 97;
	myFavoriteWord[5] = 115;
	myFavoriteWord[6] = 116;
	myFavoriteWord[7] = 105;
	myFavoriteWord[8] = 99;
	
	printf("My favorite word is %s \n", myFavoriteWord);
	
	// getchar();
	
	for (char i=0; i<30; i++) {
		printf("%d %c %d\n", i, myFavoriteWord[i], myFavoriteWord[i]);
	}
	printf("\n");
	fflush(stdout);
	
	return 0;
}
