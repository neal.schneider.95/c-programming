#include <stdio.h>
#include <stdlib.h>                         // Library that has the random functions
#include <time.h>                           // time is used to seed random generator

void main() {
    int scores[13] = {0};                   // Allocate and zeroize array
    int die1, die2, score;

    srand(time(NULL));                      // seed the random number generator
    
    for (int i = 0; i < 100000; i++) {      // loop over the number of rolls to do
        die1 = rand() % 6 + 1;
        die2 = rand() % 6 + 1;
        score = die1 + die2;                // calculate the score
        scores[score] += 1;                 // increment that score in the array
        // Or we could do the above 4 lines in one line.  It's less readable, but equivalent:
        // scores[(rand() % 6) + (rand() % 6) + 2] += 1;    }

    printf("score\trolls\n");               // print the header
    for (int i = 2; i <= 12; i++) {         // loop over potential scores
        printf("%2d\t%4d\n", i, scores[i]); // print each score
    }
}
